<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
        <!-- sidebar-brand  -->
        <div class="sidebar-item sidebar-brand">
            <a href="https://www.apartamentoslapiramide.com" target="_blank">Apartamentos La Piramide</a>
        </div>
        <!-- sidebar-header  -->
        <div class="sidebar-item sidebar-header d-flex flex-nowrap">
            <div class="user-pic">
                <img style="width: 56px; height: 72px;" class="img-responsive img-rounded rounded" src="{{asset('sidebar/img/user.jpg')}}" alt="User picture">
            </div>
            <div class="user-info">
                <span class="user-name">{{ auth()->user()->name }}
                    <strong>{{ auth()->user()->surname }}</strong>
                </span>
                <span class="user-role">{{ auth()->user()->Rol }}</span>
                <span class="user-role">Author: Jerobel Marrero</span>
                <span class="user-status">
                    <i class="fa fa-circle"></i>
                    <span>Online</span>
                </span>
            </div>
        </div>
        <!-- sidebar-search  -->
        <div class="sidebar-item sidebar-search">
            <div>
                <div class="input-group">
                    <input type="text" class="form-control search-menu" placeholder="Search...">
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
       
        <!-- sidebar-menu  -->
        <div class=" sidebar-item sidebar-menu">
            <ul>
                <li class="header-menu">
                    <span>General</span>
                </li>

                <li>
                    <a href="/home">
                        <i class="fa fa-home"></i>
                        <span class="menu-text">Home</span>
                    </a>
                </li>

                <li>
                    <a href="/booking">
                        <i class="fa fa-book"></i>
                        <span class="menu-text">Booking</span>
                    </a>
                </li>

                <li>
                    <a href="/seeker">
                        <i class="fa fa-search"></i>
                        <span class="menu-text">Seeker</span>
                    </a>
                </li>


                <li>
                    <a href="/rooms">
                        <i class="fas fa-bed"></i>
                        <span class="menu-text">Rooms</span>
                        <span class="badge badge-pill badge-primary">Beta</span>
                    </a>
                </li>


                <li>
                    <a href="/today">
                        <i class="fa fa-calendar"></i>
                        <span class="menu-text">Today</span>
                    </a>
                </li>



                <li>
                    <a href="/travelagent">
                        <i class="fas fa-atlas"></i>
                        <span class="menu-text">Travel Agents</span>
                    </a>
                </li>

                <!-- <li>
                    <a href="*">
                        <i class="fas fa-address-book"></i>
                        <span class="menu-text">Provider</span>
                        <span class="badge badge-pill badge-danger">Disabled</span>
                    </a>
                </li> -->


                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="fa fa-window-restore"></i>
                        <span class="menu-text">Services</span>
                        <!-- <span class="badge badge-pill badge-danger">3</span> -->
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="/restaurant">Restaurant

                                </a>
                            </li>

                            <li>
                                <a href="/stadistics">Stadistics
                                </a>
                            </li>

                            <li>
                                <a href="/calculator">Calculator
                                </a>
                            </li>

                            <li>
                                <a href="/clients">Clients
                                </a>
                            </li>

                            <li>
                                <a href="#">Delivery
                                    <span class="badge badge-pill badge-danger">Disabled</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </li>




                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="fa fa-chart-line"></i>
                        <span class="menu-text">Charts</span>
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="/charts/occupation">Occupation</a>
                            </li>

                            <li>
                                <a href="/charts/checkin-out">Checks IN - OUT
                                </a>
                            </li>

                            <li>
                                <a href="/charts/bookings">Bookings
                                </a>
                            </li>
                           
                        </ul>
                    </div>
                </li>


                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="fas fa-file-invoice"></i>
                        <span class="menu-text">Invoices</span>
                        <span class="badge badge-pill badge-danger">Disabled</span>
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="#">Providers</a>
                            </li>
                            <li>
                                <a href="#">Reservations</a>
                            </li>
                        </ul>
                    </div>
                </li>





                <li class="header-menu">
                    <span>Extra</span>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span class="menu-text">Documentation</span>
                        <span class="badge badge-pill badge-danger">Disabled</span>
                    </a>
                </li>



                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="fa fa-tools"></i>
                        <span class="menu-text">Administration</span>
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="/users">Users
                                    <span class="badge badge-pill badge-success">Admin</span>
                                </a>
                            </li>

                            <li>
                                <a href="/operator">Companies
                                </a>
                            </li>

                            <li>
                                <a href="/provider">Providers
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>


            </ul>
        </div>
        <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-footer  -->
    <div class="sidebar-footer">
        <div class="dropdown">

            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bell"></i>
                <span class="badge badge-pill badge-warning notification">0</span>
            </a>
            <div class="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
                <div class="notifications-header">
                    <i class="fa fa-bell"></i> Notifications
                </div>
                <div class="dropdown-divider"></div>
                <!-- <a class="dropdown-item" href="#">
                    <div class="notification-content">
                        <div class="icon">
                            <i class="fas fa-check text-success border border-success"></i>
                        </div>
                        <div class="content">
                            <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                            <div class="notification-time">
                                All Day
                            </div>
                        </div>
                    </div>
                </a> -->
                <!-- <a class="dropdown-item" href="#">
                    <div class="notification-content">
                        <div class="icon">
                            <i class="fas fa-exclamation text-info border border-info"></i>
                        </div>
                        <div class="content">
                            <div class="notification-detail">Employees must carry out the control of hours of work performed</div>
                            <div class="notification-time">
                                All Day
                            </div>
                        </div>
                    </div>
                </a> -->
                <!-- <a class="dropdown-item" href="#">
                    <div class="notification-content">
                        <div class="icon">
                            <i class="fas fa-exclamation-triangle text-warning border border-warning"></i>
                        </div>
                        <div class="content">
                            <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                            <div class="notification-time">
                                 All Day
                            </div>
                        </div>
                    </div>
                </a> -->
                <!-- <div class="dropdown-divider"></div>
                <a class="dropdown-item text-center" href="#">View all notifications</a> -->
            </div>
        </div>
        <div class="dropdown">
            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-envelope"></i>
                <span class="badge badge-pill badge-success notification">0</span>
            </a>
            <div class="dropdown-menu messages" aria-labelledby="dropdownMenuMessage">
                <div class="messages-header">
                    <i class="fa fa-envelope"></i> Messages
                </div>
                <div class="dropdown-divider"></div>
               
              
                <!-- <a class="dropdown-item" href="#">
                    <div class="message-content">
                        <div class="pic">
                            <img src="img/user.jpg" alt="">
                        </div>
                        <div class="content">
                            <div class="message-title">
                                <strong> Jhon doe</strong>
                            </div>
                            <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                        </div>
                    </div>
                </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-center" href="#">View all messages</a>

            </div>
        </div>
        <div class="dropdown">
            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-cog"></i>
                <span class="badge-sonar"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuMessage">
                <a class="dropdown-item" href="#">My profile</a>
            </div>
        </div>
        <div>
            <!-- <a href="#">
                        <i class="fa fa-power-off"></i>
                    </a> -->


            <a href="">
                <form action="{{ url('/logout') }}" method="POST">
                    {{ csrf_field() }}
                    <button style="background-color: transparent;" type="submit" class="border-0 text-right">
                        <i style="color: #811414;" class="fa fa-power-off"></i>
                    </button>
                </form>
            </a>



        </div>
        <div class="pinned-footer">
            <a href="#">
                <i class="fas fa-ellipsis-h"></i>
            </a>
        </div>
    </div>
   
</nav>


<!-- page-wrapper -->