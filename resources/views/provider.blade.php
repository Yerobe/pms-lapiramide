@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">



            <button type="button" class="btn btn-outline-success rounded-0 text-left" data-toggle="modal" data-target="#ModalNewProvider">
                <i class="fa fa-plus mr-2"></i>
                <span>New Provider</span>
            </button>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">


            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Providers</p>
                            <h4>{{count($providers)}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Providers Enabled</p>
                            <h4>{{$providersEnabled->total()}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Providers Disabled</p>
                            <h4>{{$providersDisabled->total()}}</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="4">
                                    Enabled Providers
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>CIF</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Information</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(!empty($providersEnabled))


                            @foreach ($providersEnabled as $provider)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $provider->CIF }}</td>
                                <td class="align-middle">{{ $provider->Nombre }}</td>
                                <td class="align-middle">{{ $provider->Direccion }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/provider/information/{{$provider->Id}}" target="_blank"><i class="fas fa-info-circle fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>




                        @if(count($providersEnabled)==0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no providers that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>
            </div>

            @if(count($providersEnabled)!=0)
            <div class="row d-flex justify-content-left">
                <div class="col-xl-12">
                    {{ $providersEnabled->links() }}
                </div>
            </div>
            @endif



            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="4">
                                    Disabled Providers
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>CIF</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Information</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">


                            @if(!empty($providersDisabled))


                            @foreach ($providersDisabled as $provider)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $provider->CIF }}</td>
                                <td class="align-middle">{{ $provider->Nombre }}</td>
                                <td class="align-middle">{{ $provider->Direccion }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/provider/information/{{$provider->Id}}" target="_blank"><i class="fas fa-info-circle fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif





                        </tbody>

                        @if(count($providersDisabled) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no providers that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif



                    </table>
                </div>
            </div>


            @if(count($providersDisabled)!=0)
            <div class="row d-flex justify-content-left">
                <div class="col-xl-12">
                    {{ $providersDisabled->links() }}
                </div>
            </div>
            @endif



        </div>



    </div>


    @include('utils.modals.provider')
    @stop