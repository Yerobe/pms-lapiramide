@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/create/reservation">
                <i class="fa fa-plus mr-2"></i>
                <span>New Reservation</span>
            </a>


            <a class="btn btn-outline-dark rounded-0 text-left" data-toggle="modal" data-target="#ModalSeeker">
                <i class="fas fa-sort mr-2"></i>
                <span>Show Filter</span>
            </a>




            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">



        <div class="col col-12 mt-3 mb-3 justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #5A6268; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-home fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Reservations found</p>
                            <h4>{{count($reservas)}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-home fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Active reservations</p>
                            <h4>{{$contadorReservasActivas}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-home fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Canceled reservations</p>
                            <h4>{{$contadorReservasCanceladas}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FEF165; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-euro-sign fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Paid reservations</p>
                            <h4>{{$contadorReservasPagadas}}</h4>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col col-12">

                <div class="row p-3 justify-content-around">

                    <div class="col col-11 collapse rounded shadow bg-white" id="collapseExample">
                        <div class="card card-body border-0">
                            <form action="" method="POST">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="type">Reservation number</label>
                                        <input type="text" class="form-control" name="numeroReserva" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorReserva']}}" <?php } ?>>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">BookDay</label>
                                        <input type="date" class="form-control" name="bookDay" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorBookDay']}}" <?php } ?>>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">CheckIn</label>
                                        <input type="date" class="form-control" name="fechaEntrada" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorFechaEntrada']}}" <?php } ?>>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">CheckOut</label>
                                        <input type="date" class="form-control" name="fechaSalida" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorFechaSalida']}}" <?php } ?>>
                                    </div>


                                </div>

                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">Travel Agent</label>
                                        <select name="operador" class="form-control">
                                            <option value="Everyone">Everyone</option>

                                            @foreach ($operadores as $operador)
                                            <option value="{{$operador->Id}}" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorOperador'] == $operador->Id) {
                                                                                    echo ("selected");
                                                                                } ?>>{{$operador->Nombre}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">Number Room</label>
                                        <select name="habitacion" class="form-control">
                                            <option value="Everyone">Everyone</option>

                                            @foreach ($habitaciones as $habitacion)
                                            <option value="{{$habitacion->Id}}" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorHabitacion'] == $habitacion->Id) {
                                                                                    echo ("selected");
                                                                                } ?>>{{$habitacion->NHabitacion}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">Payment</label>
                                        <select name="pagado" class="form-control">
                                            <option value="Everyone">Everyone</option>

                                            <option value="2" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorPagado'] == 2) {
                                                                    echo ("selected");
                                                                } ?>>Paid</option>
                                            <option value="1" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorPagado'] == 1) {
                                                                    echo ("selected");
                                                                } ?>>UnPaid</option>

                                        </select>
                                    </div>

                                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                        <label style="text-align: left;" for="bath">Status</label>
                                        <select name="estado" class="form-control">
                                            <option value="Everyone">Everyone</option>
                                            <option value="2" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorEstado'] == 2) {
                                                                    echo ("selected");
                                                                } ?>>Active</option>
                                            <option value="1" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorEstado'] == 1) {
                                                                    echo ("selected");
                                                                } ?>>Cancelled</option>

                                        </select>
                                    </div>



                                </div>


                                <div class="form-row">
                                    <div class="form-group col-xl-9 col-lg-9 col-md-0 col-sm-0"></div>
                                    <div class="form-group col-xl-3 col-lg-3 col-md-12 col-sm-12 text-right my-auto">
                                        <button type="submit" class="btn btn-secondary btn-block">Search</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>

                </div>
            </div>



            <div class="col col-12 pl-5 pr-5 mt-1">
                <div class="table-responsive">
                    <table class="table rounded shadow">



                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NºReservation</th>
                                <th>CheckIn</th>
                                <th>CheckOut</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Pets</th>
                                <th>Total</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Apartaments</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(count($reservas) != 0 && $conficionesActivadas == TRUE)

                            <?php $i = 0; ?>

                            @foreach ($reservas as $reserva)

                            <?php $i++; ?>


                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle <?php if ($reserva->Estado == 0) {
                                                            echo ("text-danger");
                                                        } ?>">{{ $reserva->NumeroReserva }}</td>
                                <td class="align-middle <?php if ($reserva->FechaEntrada == date("Y-m-d")) {
                                                            echo ("text-success");
                                                        } ?>"><?php echo (date("d-m-Y", strtotime($reserva->FechaEntrada))); ?></td>
                                <td class="align-middle <?php if ($reserva->FechaSalida == date("Y-m-d")) {
                                                            echo ("text-danger");
                                                        } ?>"><?php echo (date("d-m-Y", strtotime($reserva->FechaSalida))); ?></td>

                                <?php if ($reserva->EstadoPago == 'Operator') { ?>
                                    <td class="align-middle text-warning">
                                        Operator
                                    </td>
                                <?php } else { ?>

                                    <?php if ($reserva->Prepago == $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-success">
                                            Paid
                                        </td>
                                    <?php } ?>

                                    <?php if ($reserva->Prepago != $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-danger">
                                            UnPaid
                                        </td>
                                <?php }
                                } ?>
                                <td class="align-middle">{{ $reserva->NombreCliente }}</td>
                                <td class="align-middle">{{ $reserva->ApellidosCliente }}</td>
                                <td class="align-middle">{{ $reserva->NumeroMascotas }}</td>
                                <td class="align-middle">{{ $reserva->Precio_Total }}€</td>
                                <td class="align-middle">{{ $reserva->NumeroAdultos }}</td>
                                <td class="align-middle">{{ $reserva->NumeroNinios}}</td>
                                <td class="align-middle">{{ $reserva->NumeroHabitacion}}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/edit/reservation/{{$reserva->Id}}" target="_blank"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>



                        @if(count($reservas) == 0 && $conficionesActivadas == TRUE)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no reservations that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif


                        @if($conficionesActivadas == FALSE)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    You must select an option
                                </td>
                            </tr>
                        </tbody>
                        @endif


                    </table>
                </div>





            </div>





        </div>



    </div>


    @include('utils.modals.seeker.modals_seeker')

    @stop