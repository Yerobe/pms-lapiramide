<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>La Piramide</title>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>



    <style>
        * {
            margin: 0px;
            font: sans-serif;
        }

        body {
            background-color: white;
            height: 100%;
            width: 100%;
        }

        .img-logo {
            width: 120px;
            position: absolute;
            top: 90px;
            left: 80px;
        }

        .numero-factura {
            position: absolute;
            font-weight: bold;
            top: 120px;
            right: 70px;
            font-size: 20px;
        }

        .numero-factura p {
            display: inline-block;
        }

        .fecha-factura {
            position: absolute;
            top: 170px;
            right: 209px;
            font-size: 18px;
        }

        .fecha-factura p {
            display: inline-block;
        }

        .datos-empresa-factura {
            position: absolute;
            top: 250px;
            left: 60px;
            line-height: 30px;
        }

        .datos-informacion {
            position: absolute;
            left: 413px;
            top: 250px;
            line-height: 30px;
            font-weight: bold;
            font-size: 15px;
        }

        .informacion-empresa-facturada {
            position: absolute;
            left: 525px;
            top: 250px;
            line-height: 30px;
            font-size: 15px;
        }

        .bloque-desgloce {
            position: absolute;
            border: 1px solid black;
            width: 94%;
            height: 650px;
            bottom: 20px;
            left: 20px;
        }

        table,
        td,
        th {
            border: 1px solid black;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            padding: 10px;
        }

        th,
        td {
            padding: 5px;
        }


        .informacion-banco{
            position: absolute;
            bottom: 70px;
            left: 50px;
            line-height: 25px;
        }

        .informacion-copyright{
            left: 35%;
            bottom: 30px;
            position: absolute;
        }

        .texto-precio{
            position: absolute;
            left: 470px;
            bottom: 200px;
            line-height: 30px;
        }

        .informacion-precio{
            position: absolute;
            left: 600px;
            bottom: 200px;
            line-height: 30px;
        }

    </style>

</head>

<body>

    <img class="img-logo" src="<?php echo $logo ?>" alt=""></img>

    <div class="numero-factura">
        <p>Numero de Factura:</p>
        <p style="margin-left: 10px;">{{$informacion['NumeroFactura']}}</p>
    </div>

    <div class="fecha-factura">
        <p>Fecha:</p>
        <p style="margin-left: 10px;">{{date("d-m-Y")}}</p>
    </div>


    <div class="datos-empresa-factura">
        <p style="font-weight: bold; font-size: 20px;">{{$informacion['Empresa']['Nombre']}}</p>
        <p style="font-size: 15px;">{{$informacion['Empresa']['Direccion']}} - {{$informacion['Empresa']['CP']}}</p>
        <p style="font-size: 15px;">{{$informacion['Empresa']['Provincia']}} - Tel: {{$informacion['Empresa']['Telefono']}}</p>
        <p style="font-size: 15px;">CIF : {{$informacion['Empresa']['CIF']}}</p>
    </div>



    <div class="datos-informacion">
        <p>Nombre</p>
        <p>CIF / NIF</p>
        <p>Direccion</p>
        <p>CP - Ciudad</p>
    </div>


    <div class="informacion-empresa-facturada">
        <p>{{$informacion['cliente'][0]->Nombre}}</p>
        <p>{{$informacion['cliente'][0]->CIF}}</p>
        <p>{{$informacion['cliente'][0]->Direccion}}</p>
        <p>{{$informacion['cliente'][0]->CP}}</p>
    </div>


    <div class="bloque-desgloce">
        <table>
            <tr>
                <th>Numero Reserva</th>
                <th>Nombre</th>
                <th>Fecha de Entrada</th>
                <th>Fecha de Salida</th>
                <th>Pax</th>
                <th>Precio</th>
            </tr>
            <tr>
                <td>{{$informacion['NumeroReserva']}}</td>
                <td>{{$informacion['NombreCliente']}}</td>
                <td>{{$informacion['FechaEntrada']}}</td>
                <td>{{$informacion['FechaSalida']}}2</td>
                <td>{{$informacion['NumeroAdultos']}} <?php if($informacion['NumeroNinios'] != 0){echo (' + '  . $informacion['NumeroNinios']);} ?> </td>
                <td>{{$informacion['PrecioTotal']}} €</td>
            </tr>
        </table>
    </div>


    <div class="informacion-banco">
        <p style="font-weight: bold;">{{$informacion['Empresa']['Nombre']}}</p>
        <p>IBAN : {{$informacion['Empresa']['IBAN']}} </p>
    </div>


    <div class="texto-precio">
        <p>Total Neto</p>
        <p>IGIC 7%</p>
        <p style="font-weight: bold;">Total</p>
    </div>


    <div class="informacion-precio">
        <p>{{$informacion['PrecioFinal']}} €</p>
        <p>{{$informacion['CalculatedIGIG']}} €</p>
        <p style="font-weight: bold;">{{$informacion['PrecioTotal']}} €</p>
    </div>


    <div class="informacion-copyright">
        <p>Apartamentos La Piramide © <?php echo date('Y'); ?></p>
    </div>

</body>

</html>