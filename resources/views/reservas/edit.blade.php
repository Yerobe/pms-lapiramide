@extends('layouts.master')

@section('content')


<?php



foreach ($errors->all() as $error) {

    if ($error == "NumeroReserva") {
        $mensajeNumeroReserva = "border border-danger";
    }

    if ($error == "BookDay") {
        $mensajeBookDay = "border border-danger";
    }

    if ($error == "FechaEntrada") {
        $mensajeFechaEntrada = "border border-danger";
    }

    if ($error == "FechaSalida") {
        $mensajeFechaSalida = "border border-danger";
    }

    if ($error == "Adultos") {
        $mensajeAdultos = "border border-danger";
    }

    if ($error == "Ninos") {
        $mensajeNinos = "border border-danger";
    }

    if ($error == "Mascotas") {
        $mensajeMascotas = "border border-danger";
    }

    if ($error == "Nombre") {
        $mensajeNombre = "border border-danger";
    }

    if ($error == "Apellidos") {
        $mensajeApellidos = "border border-danger";
    }

    if ($error == "Prepago") {
        $mensajePrepago = "border border-danger";
    }

    if ($error == "PrecioTotal") {
        $mensajePrecioTotal = "border border-danger";
    }

    if ($error == "Regimen") {
        $mensajeRegimen = "border border-danger";
    }

    if ($error == "Operador") {
        $mensajeOperador = "border border-danger";
    }

    if ($error == "Habitacion") {
        $mensajeHabitacion = "border border-danger";
    }

    if ($error == "Pais") {
        $mensajePais = "border border-danger";
    }


    if ($error == "Email") {
        $mensajeEmail = "border border-danger";
    }
}
?>

<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">


    <div class="row">


        <div class="form-group col-md-12">

            <a class="btn btn-outline-success rounded-0 text-left" href="{{ URL::previous() }}">
                <i class="fa fa-home mr-2"></i>
                <span>Go Back</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>





    <div style="background-color: #EFF3FB;" class="row d-flex justify-content-around p-3">


        <div class="col col-11 bg-white rounded shadow">


            <form action="" method="POST">
                @method('PUT')
                @csrf

                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Reservation Information</h3>
                    </div>
                </div>


                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">NoReserva :</label>
                        <input value="{{$reserva->NumeroReserva}}" type="text" class="form-control {{$mensajeNumeroReserva ?? ''}}" id="numero_reserva" name="numero_reserva" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Book day :</label>

                        <input value="{{$reserva->BookDay}}" name="book_day" type="date" class="form-control {{$mensajeBookDay ?? ''}}" id="fecha_registro" required>
                    </div>
                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check in :</label>

                        <input value="{{$reserva->FechaEntrada}}" onchange="checkCommunity()" name="fecha_entrada" type="date" class="form-control {{$mensajeFechaEntrada ?? ''}}" id="fecha_entrada" required>
                    </div>
                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check out :</label>
                        <input value="{{$reserva->FechaSalida}}" name="fecha_salida" type="date" class="form-control {{$mensajeFechaSalida ?? ''}}" id="fecha_salida" required>
                    </div>


                </div>


                <div class="form-row mt-3">

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="adults">Adults :</label>

                        <input value="{{$reserva->NumeroAdultos}}" name="adults" type="number" class="form-control {{$mensajeAdultos ?? ''}}" min="1" required>

                    </div>


                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="children">Children :</label>

                        <input value="{{$reserva->NumeroNinios}}" name="children" type="number" class="form-control {{$mensajeNinos ?? ''}}" min="0" required>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="children">Pets :</label>

                        <input value="{{$reserva->NumeroMascotas}}" name="pets" type="number" class="form-control {{$mensajeMascotas ?? ''}}" min="0" required>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="regimen">Regime :</label>

                        <select name="regimen" id="regimen" class="form-control {{$mensajeRegimen ?? ''}}">

                            <option value="Only bed" <?php if ($reserva->Regimen == "Only Bed") {
                                                            echo ("selected");
                                                        } ?>>Only Bed</option>
                            <option value="Breakfast" <?php if ($reserva->Regimen == "Breakfast") {
                                                            echo ("selected");
                                                        } ?>>Breakfast</option>
                            <option value="Half Board" <?php if ($reserva->Regimen == "Half Board") {
                                                            echo ("selected");
                                                        } ?>>Half Board</option>
                            <option value="Full Board" <?php if ($reserva->Regimen == "Full Board") {
                                                            echo ("selected");
                                                        } ?>>Full Board</option>


                        </select>

                    </div>


                </div>





                <div class="form-row mt-3">


                    <div class="form-group col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">

                        <label for="travelagent">Travel Agent :</label>

                        <select name="travelagent" id="operadores" class="form-control {{$mensajeOperador ?? ''}}">

                            @foreach($travelagents as $travelagent)
                            <option value="{{$travelagent->Id}}" <?php if ($reserva->IdOperador == $travelagent->Id) {
                                                                        echo ("selected");
                                                                    } ?>>{{$travelagent->Nombre}}</option>
                            @endforeach

                        </select>
                    </div>


                    <div class="form-group col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">

                        <label for="room">Number Room :</label>

                        <select name="room" id="d" class="form-control {{$mensajeHabitacion ?? ''}}">

                            <option value="null">Not Assigned</option>

                            @foreach($habitaciones as $habitacion)
                            <option value="{{$habitacion->Id}}" <?php if ($reserva->IdHabitacion == $habitacion->Id) {
                                                                    echo ("selected");
                                                                } ?>>{{$habitacion->NHabitacion}}</option>
                            @endforeach

                        </select>
                    </div>


                </div>



                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Personal Information</h3>
                    </div>
                </div>



                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="name">Name :</label>
                        <input value="{{$reserva->NombreCliente}}" type="text" class="form-control {{$mensajeNombre ?? ''}}" id="name" name="name" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="surname">Surname</label>

                        <input value="{{$reserva->ApellidosCliente}}" name="surname" type="text" class="form-control {{$mensajeApellidos ?? ''}}" id="surname" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Email</label>

                        <input value="{{$reserva->Email}}" name="email" type="email" class="form-control {{$mensajeEmail ?? ''}}" id="email">
                    </div>

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="phone">Phone</label>

                        <input value="{{$reserva->Telefono}}" name="phone" type="text" class="form-control {{$mensajePhone ?? ''}}" id="phone">
                    </div>



                </div>



                <div class="form-row mt-3">
                    <div class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">

                        <label for="country">Country :</label>

                        <select name="country" id="country" class="form-control {{$mensajePais ?? ''}}" onchange="checkCountry()">

                            <option value='null'>Not Assigned</option>

                            @foreach($paises as $pais)
                            <option value="{{$pais->Id}}" <?php if ($reserva->IdPais == $pais->Id) {
                                                                echo ("selected");
                                                            } ?>>{{$pais->Nombre}}</option>
                            @endforeach

                        </select>
                    </div>


                    <div id="ComunidadAutonoma" class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">

                        <label for="community">Autonomous Community :</label>

                        <select name="community" id="community" class="form-control" onchange="myCheckIsland()">

                            <option value="null" selected>Not Assigned</option>

                            @foreach($comunidadesautonomas as $comunidad)
                            <option value="{{$comunidad->Id}}" <?php if ($reserva->IdComunidadAutonoma == $comunidad->Id) {
                                                                    echo ("selected");
                                                                } ?>>{{$comunidad->Nombre}}</option>
                            @endforeach

                        </select>

                    </div>


                    <div id="IslasCanarias" class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">
                        <label for="canaryIsland">Canary Island :</label>
                        <select name="canaryIsland" id="canaryIsland" class="form-control">
                            <option value="null" <?php if ($reserva->IslaCanaria == null) {
                                                        echo "selected";
                                                    } ?>>Not Assigned</option>
                            @foreach($canaryIsland as $islacanaria)
                            <option value="{{$islacanaria->Id}}" <?php if ($reserva->IdCanarias == $islacanaria->Id) {
                                                                        echo ("selected");
                                                                    } ?>>{{$islacanaria->Nombre}}</option>
                            @endforeach
                        </select>
                    </div>


                </div>



                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Payment</h3>
                    </div>
                </div>


                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="paymentmade">Payment made :</label>
                        <input value="{{$reserva->Prepago}}" type="number" step="0.01" class="form-control {{$mensajePrepago ?? ''}}" id="paymentmade" name="paymentmade" placeholder="0.00€" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="totalprice">Total price :</label>
                        <input value="{{$reserva->Precio_Total}}" type="number" step="0.01" class="form-control {{$mensajePrecioTotal ?? ''}}" id="totalprice" name="totalprice" placeholder="0.00€" required>
                    </div>

                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="methodpayment">Method Payment :</label>
                        <select name="methodpayment" id="methodpayment" class="form-control">
                            <option value="null" <?php if ($reserva->EstadoPago == null) {
                                                        echo ('selected');
                                                    } ?>>Not Assigned</option>
                            <option value="Card" <?php if ($reserva->EstadoPago == 'Card') {
                                                        echo ('selected');
                                                    } ?>>Card</option>
                            <option value="Cash" <?php if ($reserva->EstadoPago == 'Cash') {
                                                        echo ('selected');
                                                    } ?>>Cash</option>
                            <option value="Operator" <?php if ($reserva->EstadoPago == 'Operator') {
                                                            echo ('selected');
                                                        } ?>>Operator</option>
                            <option value="Cash and Card" <?php if ($reserva->EstadoPago == 'Cash and Card') {
                                                                echo ('selected');
                                                            } ?>>Cash and Card</option>
                            <option value="Virtual Card" <?php if ($reserva->EstadoPago == 'Virtual Card') {
                                                                echo ('selected');
                                                            } ?>>Virtual Card</option>
                            <option value="Transfer" <?php if ($reserva->EstadoPago == 'Transfer') {
                                                            echo ('selected');
                                                        } ?>>Transfer</option>
                        </select>
                    </div>




                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Cancelled</h3>
                    </div>
                </div>

                <div class="form-row mt-3">





                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex">
                        <label style="margin-top: 42px;" class="mr-4" for="email">Cancelled Reservation :</label>
                        <label class="chkbx" style="margin-top: 40px;">
                            <input name="estado" type="checkbox" <?php if ($reserva->Estado == 0) {
                                                                        echo ("checked");
                                                                    } ?>>
                            <span class="x "></span>
                        </label>
                    </div>

                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Comments</h3>
                    </div>
                </div>


                <div class="form-row mt-3">


                    <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <textarea name="comentario" id="comentario" cols="30" rows="8">{{$reserva->Comentario}}</textarea>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-0 col-sm-0 col-0">

                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 mt-5">
                        <button style="background-color: #5A6268;" type="submit" class="btn btn-block text-white rounded-0">Update</button>
                        <a href="{{ URL::previous() }}" class="btn btn-dark btn-block rounded-0">Cancel</a>
                    </div>

                    <div class="col col-1"></div>



                </div>

            </form>


            <div class="form-row border-bottom mt-2">
                <div class="col col-12">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Members</h3>
                </div>
            </div>



            <div class="form-row mt-3">
                <div class="col col-12">
                    <div class="table-responsive">
                        <table class="table rounded shadow">


                            <thead class="table-light">
                                <tr>
                                    <th class="text-right" style="color: #5B626B;" colspan="4">
                                        <button style="background-color: #FDFDFE;" type="button" class="text-dark text-decoration-none border-0" data-toggle="modal" data-target="#ModalNewMember">
                                            +
                                        </button>
                                    </th>
                                </tr>
                            </thead>

                            <thead class="table-light">
                                <tr class="text-center">
                                    <th>Identification Number</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody class="table-light">


                                @if (count($miembros) != 0)
                                @foreach ($miembros as $miembro)
                                <tr class="text-center h-100">
                                    <td class="align-middle">{{$miembro['NIF']}}</td>
                                    <td class="align-middle">{{$miembro['Nombre']}}</td>
                                    <td class="align-middle">{{$miembro['Apellidos']}}</td>
                                    <td><a href="/edit/reservation/{{$idReserve}}/{{$miembro['Id']}}"><i class="fas fa-times text-dark"></i></a></td>
                                </tr>

                                @endforeach
                                @else

                                <tr class="text-center h-100">
                                    <td colspan="4" class="align-middle">The reservastion has no Registered Members</td>
                                </tr>

                                @endif






                            </tbody>









                        </table>
                    </div>





                </div>
            </div>



            <div class="form-row border-bottom mt-2">
                <div class="col col-12">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Files</h3>
                </div>
            </div>





            <div class="form-row mt-3">
                <div class="col col-8">
                    <form action="/{{$idReserve}}/generate/dni" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="urlpdf" required>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>

                </div>

                <div class="col col-4 text-center">
                    <input type="submit" class="btn btn-dark rounded" value="Register File">
                </div>
                </form>
            </div>

            <div class="form-row mt-3">
                <div class="col col-12">
                    <?php if (count($documents) == 0) { // Controlamos las vistas de los documentos asociados a la reserva 
                    ?>
                        <p class="font-weight-bold float-left">Note:</p>
                        <p class="float-left ml-2">There are no Files related to this reservation</p>
                        <?php } else {
                        $i = 1;
                        foreach ($documents as $document) {
                        ?>
                            <p class="font-weight-bold float-left">Document - {{$i}}:</p>
                            <a class="float-left ml-3 text-dark" target="_blank" href="/view/document/pdf/<?php echo ($document->Nombre); ?>">{{$document->Nombre}}</a>
                            </br>
                    <?php $i++;
                        }
                    } ?>
                </div>
            </div>

            <!--
                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Files</h3>
                    </div>
                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                     <form action="" enctype="multipart/form-data">
                         <input type="file" name="file">
                     </form>
                    </div>
                </div> -->




            <div class="form-row border-bottom mt-2">
                <div class="col col-12">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Logs</h3>
                </div>
            </div>



            <div class="form-row mt-3">
                <div class="col col-12">
                    <div class="d-flex">
                        <p class="font-weight-bold d-inline-block">User who created the reservation:</p>
                        <p class="d-inline-block ml-2">{{$empleadoCreado}}</p>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="">
                        <p class="font-weight-bold">User who modified the reservation:</p>

                        @foreach ($empleadosModificados as $empleadoModificado)
                        <div class="d-flex">
                            <p class="ml-2">- {{$empleadoModificado -> NombreEmpleado}}</p>
                            <p class="ml-3"><?php echo (date_format(date_create($empleadoModificado->FechaModificacion), "d-m-Y")); ?></p>
                        </div>

                        @endforeach




                    </div>
                </div>
            </div>


            <div class="form-row border-bottom mt-2">
                <div class="col col-12">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Documentation</h3>
                </div>
            </div>



            <div class="form-row mt-3 mb-3">
                <div class="col col-12">
                    <a href="/edit/reservation/{{$idReserve}}/generate/voucher" class="btn btn-dark rounded" target="_blank">Generate Voucher</a>
                    
                    <button type="button" class="btn btn-dark rounded" data-toggle="modal" data-target="#ModalCreateInvoice">
                        Generate Invoice
                    </button>
                </div>
            </div>



        </div>




    </div>






</div>



</div>



</div>

@include('utils.modals.reservation.modals_reservation')

@stop