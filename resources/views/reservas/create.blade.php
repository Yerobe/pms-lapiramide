@extends('layouts.master')

@section('content')


<?php



foreach ($errors->all() as $error) {

    if ($error == "NumeroReserva") {
        $mensajeNumeroReserva = "border border-danger";
    }

    if ($error == "BookDay") {
        $mensajeBookDay = "border border-danger";
    }

    if ($error == "FechaEntrada") {
        $mensajeFechaEntrada = "border border-danger";
    }

    if ($error == "FechaSalida") {
        $mensajeFechaSalida = "border border-danger";
    }

    if ($error == "Adultos") {
        $mensajeAdultos = "border border-danger";
    }

    if ($error == "Ninos") {
        $mensajeNinos = "border border-danger";
    }

    if ($error == "Mascotas") {
        $mensajeMascotas = "border border-danger";
    }

    if ($error == "Nombre") {
        $mensajeNombre = "border border-danger";
    }

    if ($error == "Apellidos") {
        $mensajeApellidos = "border border-danger";
    }

    if ($error == "Prepago") {
        $mensajePrepago = "border border-danger";
    }

    if ($error == "PrecioTotal") {
        $mensajePrecioTotal = "border border-danger";
    }

    if ($error == "Regimen") {
        $mensajeRegimen = "border border-danger";
    }

    if ($error == "Operador") {
        $mensajeOperador = "border border-danger";
    }

    if ($error == "Habitacion") {
        $mensajeHabitacion = "border border-danger";
    }

    if ($error == "Pais") {
        $mensajePais = "border border-danger";
    }


    if ($error == "Email") {
        $mensajeEmail = "border border-danger";
    }
}
?>

<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">


    <div class="row">


        <div class="form-group col-md-12">

            <a class="btn btn-outline-success rounded-0 text-left" href="{{ URL::previous() }}">
                <i class="fa fa-home mr-2"></i>
                <span>Go Back</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>





    <div style="background-color: #EFF3FB;" class="row d-flex justify-content-around p-3">


        <div class="col col-11 bg-white rounded shadow">


            <form action="" method="POST">
                @csrf

                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Reservation Information</h3>
                    </div>
                </div>


                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">NoReserva :</label>
                        <input value="{{ old('numero_reserva') }}" type="text" class="form-control {{$mensajeNumeroReserva ?? ''}}" id="numero_reserva" name="numero_reserva" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Book day :</label>

                        <input value="{{old('book_day') ?? $fecha_actual}}" name="book_day" type="date" class="form-control {{$mensajeBookDay ?? ''}}" id="fecha_registro" required>
                    </div>
                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check in :</label>

                        <input value="{{old('fecha_entrada') ?? $fecha_actual}}" onchange="checkDates()" name="fecha_entrada" type="date" class="form-control {{$mensajeFechaEntrada ?? ''}}" id="fecha_entrada" required>
                    </div>
                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check out :</label>
                        <input value="{{old('fecha_salida') ?? $fechaCheckOut}}" name="fecha_salida" type="date" class="form-control {{$mensajeFechaSalida ?? ''}}" id="fecha_salida" required>
                    </div>


                </div>


                <div class="form-row mt-3">

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="adults">Adults :</label>

                        <input value="{{old('adults') ?? 1}}" name="adults" type="number" class="form-control {{$mensajeAdultos ?? ''}}" min="1" required>

                    </div>


                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="children">Children :</label>

                        <input value="{{old('children') ?? 0}}" name="children" type="number" class="form-control {{$mensajeNinos ?? ''}}" min="0" required>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="children">Pets :</label>

                        <input value="{{old('pets') ?? 0}}" name="pets" type="number" class="form-control {{$mensajeMascotas ?? ''}}" min="0" required>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                        <label for="regimen">Regime :</label>

                        <select name="regimen" id="regimen" class="form-control {{$mensajeRegimen ?? ''}}">

                            <option value="Only bed" <?php if (old('regimen') == "Only Bed") {
                                                            echo ("selected");
                                                        } ?>>Only Bed</option>
                            <option value="Breakfast" <?php if (old('regimen') == "Breakfast") {
                                                            echo ("selected");
                                                        } ?>>Breakfast</option>
                            <option value="Half Board" <?php if (old('regimen') == "Half Board") {
                                                            echo ("selected");
                                                        } ?>>Half Board</option>
                            <option value="Full Board" <?php if (old('regimen') == "Full Board") {
                                                            echo ("selected");
                                                        } ?>>Full Board</option>


                        </select>

                    </div>


                </div>


                <div class="form-row mt-3">


                    <div class="form-group col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">

                        <label for="travelagent">Travel Agent :</label>

                        <select name="travelagent" id="operadores" class="form-control {{$mensajeOperador ?? ''}}">

                            @foreach($travelagents as $travelagent)
                            <option value="{{$travelagent->Id}}" <?php if (old('travelagent') == $travelagent->Id) {
                                                                        echo ("selected");
                                                                    } ?>>{{$travelagent->Nombre}}</option>
                            @endforeach

                        </select>
                    </div>


                    <div class="form-group col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">

                        <label for="room">Number Room :</label>

                        <select name="room" id="d" class="form-control {{$mensajeHabitacion ?? ''}}">

                            <option value="null">Not Assigned</option>

                            @foreach($habitaciones as $habitacion)
                            <option value="{{$habitacion->Id}}" <?php if (old('room') == $habitacion->Id) {
                                                                    echo ("selected");
                                                                } ?>>{{$habitacion->NHabitacion}}</option>
                            @endforeach

                        </select>
                    </div>


                </div>



                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Personal Information</h3>
                    </div>
                </div>



                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="name">Name :</label>
                        <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNombre ?? ''}}" id="name" name="name" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="surname">Surname</label>

                        <input value="{{old('surname') ?? ''}}" name="surname" type="text" class="form-control {{$mensajeApellidos ?? ''}}" id="surname" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Email</label>

                        <input value="{{old('email') ?? ''}}" name="email" type="email" class="form-control {{$mensajeEmail ?? ''}}" id="email">
                    </div>

                    <div class="form-group  col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="phone">Phone</label>

                        <input value="{{old('phone') ?? ''}}" name="phone" type="text" class="form-control {{$mensajePhone ?? ''}}" id="phone">
                    </div>


                </div>


                <div class="form-row mt-3">
                    <div class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">

                        <label for="country">Country :</label>


                        <select name="country" id="country" class="form-control {{$mensajePais ?? ''}}" onchange="checkCountry()">

                        <option value = 'null'>Not Assigned</option>
                        
                            @foreach($paises as $pais)
                            <option value="{{$pais->Id}}" <?php if (old('country') == $pais->Id) {
                                                                echo ("selected");
                                                            } ?>>{{$pais->Nombre}}</option>
                            @endforeach

                        </select>
                    </div>


                    <div id="ComunidadAutonoma" class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">

                        <label for="community">Autonomous Community :</label>

                        <select name="community" id="community" class="form-control" onchange="myCheckIsland()">

                            <option value="null" selected>Not Assigned</option>

                            @foreach($comunidadesautonomas as $comunidad)
                            <option value="{{$comunidad->Id}}" <?php if (old('community') == $comunidad->Id) {
                                                                    echo ("selected");
                                                                } ?>>{{$comunidad->Nombre}}</option>
                            @endforeach

                        </select>

                    </div>


                    <div id="IslasCanarias" class="form-group col-xl-3 col-lg-2 col-md-6 col-sm-6 col-12">
                        <label for="canaryIsland">Canary Island :</label>
                        <select name="canaryIsland" id="canaryIsland" class="form-control">
                            <option value="null" selected>Not Assigned</option>
                            @foreach($canaryIsland as $isla)
                            <option value="{{$isla->Id}}" <?php if (old('canaryIsland') == $isla->Id) {
                                                                    echo ("selected");
                                                                } ?>>{{$isla->Nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>



                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Payment</h3>
                    </div>
                </div>


                <div class="form-row mt-3">

                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="paymentmade">Payment made :</label>
                        <input value="{{old('paymentmade') ?? 0}}" type="number" step="0.01" class="form-control {{$mensajePrepago ?? ''}}" id="paymentmade" name="paymentmade" placeholder="0.00€" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="totalprice">Total price :</label>
                        <input value="{{old('totalprice') ?? 0}}" type="number" step="0.01" class="form-control {{$mensajePrecioTotal ?? ''}}" id="totalprice" name="totalprice" placeholder="0.00€" required>
                    </div>


                    <div class="form-group  col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12">
                        <label for="methodpayment">Method Payment :</label>
                        <select name="methodpayment" id="methodpayment" class="form-control">
                            <option value="null" selected>Not Assigned</option>
                          
                            <option value="Card" <?php if (old('methodpayment') == "Card") {echo ("selected");} ?>>Card</option>
                            <option value="Cash"  <?php if (old('methodpayment') == "Cash") {echo ("selected");} ?>>Cash</option>
                            <option value="Operator"  <?php if (old('methodpayment') == "Operator") {echo ("selected");} ?>>Operator</option>
                            <option value="Cash and Card"  <?php if (old('methodpayment') == "Cash and Card") {echo ("selected");} ?>>Cash and Card</option>
                            <option value="Virtual Card"  <?php if (old('methodpayment') == "Virtual Card") {echo ("selected");} ?>>Virtual Card</option>
                            <option value="Transfer"  <?php if (old('methodpayment') == "Transfer") {echo ("selected");} ?>>Transfer</option>
                        

                        </select>
                    </div>


                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Comments</h3>
                    </div>
                </div>


                <div class="form-row mt-3">


                    <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <textarea name="comentario" id="comentario" cols="30" rows="8">{{old('comentario') ?? ''}}</textarea>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-0 col-sm-0 col-0">

                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 mt-5">
                        <button style="background-color: #5A6268;" type="submit" class="btn btn-block text-white rounded-0">Insert</button>
                        <a href="{{ URL::previous() }}" class="btn btn-dark btn-block rounded-0">Cancel</a>
                    </div>

                    <div class="col col-1"></div>



                </div>



        </div>




    </div>


    </form>



</div>



</div>



</div>




@stop