<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>La Piramide</title>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>



    <style>
        body {
            background-color: #E5E5E5;
            margin: -33;
            height: 100vh;
            width: 100%;
        }

        .firstRow {
            margin-left: auto;
            margin-right: auto;
            margin-top: 60px;
            width: 90%;
            height: 30px;
            background-color: #E4B224;

        }

        .secondRow {
            margin-left: auto;
            margin-right: auto;
            width: 90%;
            height: 180px;
            background-color: white;
        }

        #logoImage {
            position: absolute;
            top: 30px;
            width: 120px;
            margin-left: 42%;
        }

        .secondRow h2 {
            position: absolute;
            right: 140px;
            top: 160px;
        }

        .secondRow h3 {
            position: absolute;
            left: 300px;
            top: 210px;
        }

        #informationImage {
            position: absolute;
            width: 90%;
            height: 200px;
            top: 300px;
            left: 41px;
        }

        #mapImage {
            position: absolute;
            width: 90%;
            top: 520px;
            left: 41px;
            height: 200px;
        }


        .thirdRow {
            position: absolute;
            width: 90%;
            top: 740px;
            left: 41px;
            height: 300px;
            background-color: white;
        }

        .thirdRow h2 {
            margin-left: 20px;
            margin-top: 20px;
            font-family: verdana;
        }

        .thirdRow .title {
            font-weight: bold;
            font-size: 20px;
            text-align: center;
        }

        .textCheckIn {
            position: absolute;
            left: 100px;
            top: 80px;
        }

        .textCheckOut {
            position: absolute;
            left: 230px;
            top: 60px;
        }

        .textNights {
            position: absolute;
            left: 360px;
            top: 60px;
        }

        .textAdults {
            position: absolute;
            left: 450px;
            top: 60px;
        }

        .textChildrens {
            position: absolute;
            left: 550px;
            top: 60px;
        }

        .thirdRow .requestText {
            position: absolute;
            font-size: 20px;
        }

        .requestCheckIn {
            left: 95px;
            top: 90px;
        }

        .requestCheckOut {
            left: 233px;
            top: 90px;
        }

        .requestNights {
            left: 380px;
            top: 90px;
        }

        .requestAdults {
            left: 475px;
            top: 90px;
        }

        .requestChildrens {
            left: 585px;
            top: 90px;
        }

        .thirdRow .titleUnder {
            display: inline-block;
            font-weight: bold;
            font-size: 20px;
            margin-left: 15px;
        }

        .thirdRow .textUnder {
            display: inline-block;
            font-size: 20px;
            margin-left: 15px;
        }

        .thirdRow .titleStatus {
            font-weight: bold;
            position: absolute;
            right: 100px;
            top: 160px;
            font-size: 25px;
        }

        .thirdRow .textStatus {
            position: absolute;
            top: 200px;
            font-size: 20px;
        }
    </style>

</head>

<body>




    <div class="firstRow"></div>
    <div class="secondRow">
        <img id="logoImage" src="<?php echo $logo ?>" alt=""></img>
        <h2>Thank you very much, your reservation is confirmed</h2>
        <h3>Reservation Code : {{$reserva[0]['NumeroReserva']}}</h3>
    </div>


    <img id="informationImage" src="<?php echo $information ?>" alt=""></img>

    <img id="mapImage" src="<?php echo $map ?>" alt=""></img>

    <div class="thirdRow">
        <h2>Details of the Reservation</h2>
        <p class="title textCheckIn">Check-In</p>
        <p class="title textCheckOut">Check-Out</p>
        <p class="title textNights">Nights</p>
        <p class="title textAdults">Adults</p>
        <p class="title textChildrens">Childrens</p>

        <p class="requestText requestCheckIn">{{date("d-m-Y", strtotime($reserva[0]['FechaEntrada']))}}</p>
        <p class="requestText requestCheckOut">{{date("d-m-Y", strtotime($reserva[0]['FechaSalida']))}}</p>
        <?php 
        $date1 = new DateTime($reserva[0]['FechaEntrada']);
        $date2 = new DateTime($reserva[0]['FechaSalida']);
        $diff = $date1->diff($date2);
        ?>
        <p class="requestText requestNights">{{$diff->days}}</p>
        <p class="requestText requestAdults">{{$reserva[0]['NumeroAdultos']}}</p>
        <p class="requestText requestChildrens">{{$reserva[0]['NumeroNinios']}}</p>

        <p style="margin-top: 50px;" class="titleUnder">Name:</p>
        <p style="margin-top:50px;" class="textUnder">{{$reserva[0]['NombreCliente']}} {{$reserva[0]['ApellidosCliente']}}</p>

        </br>

        <p class="titleUnder">Price:</p>
        <p style="margin-top:20px;" class="textUnder">{{$reserva[0]['Precio_Total']}} €</p>

        </br>

        <p style="margin-top: 5px;" class="titleUnder">Pets:</p>
        <p style="margin-top:7px;" class="textUnder">{{$reserva[0]['NumeroMascotas']}}</p>



        <p class="titleStatus">Status</p>
        <p style="color: <?php if ($reserva[0]['Estado'] == 1) {
                echo ('green');
            } else {
                echo ('red');
            } ?>; left: <?php if ($reserva[0]['Estado'] == 1) {
                echo ('552px');
            } else {
                echo ('540px');
            } ?> ;" class="textStatus">
            <?php if ($reserva[0]['Estado'] == 1) {
                echo ('Active');
            } else {
                echo ('Cancelled');
            } ?>
        </p>


    </div>






</body>

</html>