@extends('layouts.master')

@section('content')





<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/create/reservation">
                <i class="fa fa-plus mr-2"></i>
                <span>New Reservation</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-calendar-alt fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Bookings Today</p>
                            <h4>{{$cantidadBookingToday}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FD7E14; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-calendar-alt fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Bookings 2 Days</p>
                            <h4>{{$cantidadBookingTwoDays}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-calendar-alt fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Bookings 7 Days</p>
                            <h4>{{$cantidadBookingSevenDays}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-calendar-alt fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Bookings 1 Month</p>
                            <h4>{{$cantidadBookingThirtyDays}}</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    Today Bookings Details
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NºReservation</th>
                                <th>CheckIn</th>
                                <th>CheckOut</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Pets</th>
                                <th>Total</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Apartaments</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(!empty($allBookings))

                            <?php $i = 0; ?>


                            @foreach ($allBookings as $reserva)

                            <?php $i++; ?>


                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle">{{ $reserva->NumeroReserva }}</td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($reserva->FechaEntrada))); ?></td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($reserva->FechaSalida))); ?></td>

                                <?php if ($reserva->EstadoPago == 'Operator') { ?>
                                    <td class="align-middle text-warning">
                                        Operator
                                    </td>
                                <?php } else { ?>

                                    <?php if ($reserva->Prepago == $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-success">
                                            Paid
                                        </td>
                                    <?php } ?>

                                    <?php if ($reserva->Prepago != $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-danger">
                                            UnPaid
                                        </td>
                                <?php }
                                } ?>
                                <td class="align-middle">{{ $reserva->NombreCliente }}</td>
                                <td class="align-middle">{{ $reserva->ApellidosCliente }}</td>
                                <td class="align-middle">{{ $reserva->NumeroMascotas }}</td>
                                <td class="align-middle">{{ $reserva->Precio_Total }}€</td>
                                <td class="align-middle">{{ $reserva->NumeroAdultos }}</td>
                                <td class="align-middle">{{ $reserva->NumeroNinios}}</td>
                                <td class="align-middle">{{ $reserva->NumeroHabitacion}}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/edit/reservation/{{$reserva->Id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>




                        @if(count($allBookings)==0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no reservations that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>
            </div>



            <div class="col col-12 pl-5 pr-5 mt-5">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    Yesterday Bookings Details
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NºReservation</th>
                                <th>CheckIn</th>
                                <th>CheckOut</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Pets</th>
                                <th>Total</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Apartaments</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(!empty($allBookingsYesterday))

                            <?php $i = 0; ?>


                            @foreach ($allBookingsYesterday as $reserva)

                            <?php $i++; ?>

                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle">{{ $reserva->NumeroReserva }}</td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($reserva->FechaEntrada))); ?></td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($reserva->FechaSalida))); ?></td>
                                <?php if ($reserva->EstadoPago == 'Operator') { ?>
                                    <td class="align-middle text-warning">
                                        Operator
                                    </td>
                                <?php } else { ?>

                                    <?php if ($reserva->Prepago == $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-success">
                                            Paid
                                        </td>
                                    <?php } ?>

                                    <?php if ($reserva->Prepago != $reserva->Precio_Total) { ?>
                                        <td class="align-middle text-danger">
                                            UnPaid
                                        </td>
                                <?php }
                                } ?>
                                <td class="align-middle">{{ $reserva->NombreCliente }}</td>
                                <td class="align-middle">{{ $reserva->ApellidosCliente }}</td>
                                <td class="align-middle">{{ $reserva->NumeroMascotas }}</td>
                                <td class="align-middle">{{ $reserva->Precio_Total }}€</td>
                                <td class="align-middle">{{ $reserva->NumeroAdultos }}</td>
                                <td class="align-middle">{{ $reserva->NumeroNinios}}</td>
                                <td class="align-middle">{{ $reserva->NumeroHabitacion}}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/edit/reservation/{{$reserva->Id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>




                        @if(count($allBookingsYesterday)==0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no reservations that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>
            </div>



        </div>


    </div>

    @stop