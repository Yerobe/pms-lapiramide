@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/home">
                <i class="fa fa-home mr-2"></i>
                <span>Go Home</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">



        <div class="col col-12 text-left mt-3 mb-3 row justify-content-center">

            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <h3 class="text-left">Reservation Calculator</h3>


                        <form action="" method="POST" class="mt-5 mb-3">
                            @csrf
                            <div id="fila_1" class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="text-left" for="FechaEntrada">CheckIn</label>
                                    <input name="FechaEntrada" type="date" class="form-control" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-left" for="FechaSalida">CheckOut</label>
                                    <input name="FechaSalida" type="date" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label class="text-left" for="Price">Price Per Day</label>
                                    <input name="Price" type="number" class="form-control" required value="0">
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-left" for="Suplemento">Supplement</label>
                                    <input name="Suplemento" type="number" class="form-control" value="0">
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-left" for="Descuento">Discount %</label>
                                    <input name="Descuento" type="number" class="form-control" value="0">
                                </div>


                            </div>


                            <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') { ?>


                                <div>
                                    <p class="float-left font-weight-bold">Note:</p>
                                    <p class="float-right">To calculate the final price of the reservation, you must establish the date of entry and exit, the price per day, and if there is any discount (5% - 10%). In the event that there are animals, or supplements as an extra person, establish it in the 'Supplement' field that will be added to the total and the discount will be applied jointly.</p>
                                </div>

                                <div class="mt-3">
                                    <p class="float-left font-weight-bold">Example:</p>
                                    <p class="float-right">The reservation is for 1 person, in low season, so it has a price of 40 euros per night. We establish the dates, which are in total 7 Nights, it comes with a pet, so in Supplement, we establish 35 euros.
                                        We introduce a 5% discount, since it is a LongStay reservation, and this result will be the following:
                                        ((40 * 7) + 35) - 5% = 299.25</p>
                                </div>

                            <?php } ?>


                            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>

                                <div class="form-row">
                                    <div class="mt-3">
                                        <p class="float-left font-weight-bold">Check-In: </p>
                                        <p class="float-left ml-2"><?php echo (date("d/m/Y", strtotime($fechaEntrada))); ?></p>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="">
                                        <p class="float-left font-weight-bold">Check-Out: </p>
                                        <p class="float-left ml-2"><?php echo (date("d/m/Y", strtotime($fechaSalida))); ?></p>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="">
                                        <p class="float-left font-weight-bold">Total Days: </p>
                                        <p class="float-left ml-2"><?php echo ($diasReserva); ?></p>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="">
                                        <p class="float-left font-weight-bold">Suplement: </p>
                                        <p class="float-left ml-2"><?php echo ($suplemento . ' Є'); ?></p>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="">
                                        <p class="float-left font-weight-bold">Discount: </p>
                                        <p class="float-left ml-2"><?php echo ($descuento . '%'); ?></p>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="">
                                        <p class="float-left font-weight-bold">Total Price: </p>
                                        <p class="float-left ml-2"><?php echo ($reservePrice . ' Є'); ?></p>
                                    </div>
                                </div>


                            <?php } ?>

                            <button type="submit" class="btn btn-block btn-secondary mt-3">Calculate</button>




                            <!-- <div class="float-right">
                                <p class="float-left mr-4">Add Dates</p>
                                <i id="signo" class="fa fa-plus mr-2 text-primary" onclick="addDate();"></i>
                            </div> -->


                        </form>





                    </div>
                </div>
            </div>



        </div>



    </div>


    <script>
        window.onload = function() {
            if (typeof document.getElementById("fechaEntrada2") != "undefined") {

                document.getElementById("fila_2").style.display = "none";

            }
        }
    </script>



    @stop