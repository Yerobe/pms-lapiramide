@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/create/reservation">
                <i class="fa fa-plus mr-2"></i>
                <span>New Reservation</span>
            </a>

            <a class="btn btn-outline-dark rounded-0 text-left" data-toggle="modal" data-target="#ModalFilterRestaurant">
            <i class="fas fa-sort mr-2"></i>
                <span>Show Filter</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">


        <div class="col col-12 mt-3 mb-3 justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-chart-line fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Calculated Days</p>
                            <h4>{{$informacionSuperior['DiasCalculados']}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-chart-line fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Average of Breakfasts</p>
                            <h4>{{$informacionSuperior['Desayunos']}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-chart-line fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Average of Lunches</p>
                            <h4>{{$informacionSuperior['Almuerzos']}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-chart-line fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Average Dinners</p>
                            <h4>{{$informacionSuperior['Cenas']}}</h4>
                        </div>
                    </div>

                </div>
            </div>

           

            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">



                        <thead class="table-light">
                            <tr class="text-center">
                                <th>Date</th>
                                <th>Rooms</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Breakfast</th>
                                <th>Lunch</th>
                                <th>Dinner</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">


                            <?php $i = 0 ?>

                            @foreach ($informacionRestaurante as $diaIndividual)

                            <?php $i++ ?>

                            <tr class="text-center h-100 <?php if(fmod($i,2) == 0){echo('bg-light');} ?>">
                                <td class="align-middle">{{ $diaIndividual['Fecha'] }}</td>
                                <td class="align-middle">{{ $diaIndividual['Habitaciones'] }}</td>
                                <td class="align-middle">{{ $diaIndividual['Adultos'][0]->Adultos ?? 0}}</td>
                                <td class="align-middle">{{ $diaIndividual['Ninos'][0]->Ninos ?? 0}}</td>
                                <td class="align-middle">{{ $diaIndividual['Desayunos'][0]->Desayunos ?? 0}}</td>
                                <td class="align-middle">{{ $diaIndividual['Almuerzos'][0]->Almuerzos ?? 0}}</td>
                                <td class="align-middle">{{ $diaIndividual['Cenas'][0]->Cenas ?? 0}}</td>
                            </tr>

                            @endforeach




                        </tbody>



                    </table>
                </div>





            </div>





        </div>



    </div>



    @include('utils.modals.restaurant.modals_restaurant')

    @stop