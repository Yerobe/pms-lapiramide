@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/create/reservation">
                <i class="fa fa-plus mr-2"></i>
                <span>New Reservation</span>
            </a>

            <a class="btn btn-outline-dark rounded-0 text-left" data-toggle="modal" data-target="#ModalFilterRoom">
            <i class="fas fa-sort mr-2"></i>
                <span>Show Filter</span>
            </a>



            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 mt-3 mb-3 justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #5A6268; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Occupied Apartments</p>
                            <h4>{{$apartamentosOcupados}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Free Apartments</p>
                            <h4>{{$apartamentosLibres}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Blocked Apartments</p>
                            <h4>{{$apartamentosBloqueados}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FEF165; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Dirty Apartments</p>
                            <h4>{{$apartamentosSucios}}</h4>
                        </div>
                    </div>

                </div>
            </div>
           
            <div class="col col-12">
                <div class="row p-3 justify-content-around">

                    <?php if(count($habitaciones) == 0){ ?>
                    <div class="col col-11 bg-white rounded p-2 mb-3 shadow text-center">
                        <p class="font-weight-bold mt-2">No rooms have been found with the specified conditions</p>
                    </div>

                    <?php } ?>


                </div>


            </div>



            <div class="col col-12">


                <div class="row p-3 d-flex justify-content-around">

                    @foreach ($habitaciones as $habitacion)
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 rounded p-3 m-3 shadow <?php if ($habitacion->Estado == "Disponible") {
                                                                                                        echo ("bg-success");
                                                                                                    } elseif ($habitacion->Estado == "No Limpio") {
                                                                                                        echo ("bg-warning");
                                                                                                    } elseif ($habitacion->Estado == "Bloqueado") {
                                                                                                        echo ("bg-danger");
                                                                                                    } elseif ($habitacion->Estado == "Ocupado") {
                                                                                                        echo ("bg-secondary");
                                                                                                    } ?>">
                        <?php if ($formularioDesactivado == NULL) { ?>
                            <a class="text-decoration-none" href="/rooms/{{$habitacion->Id}}?page={{$paginaActual}}"><?php } ?>
                            <div style="width: 50%;" class="icono text-center p-1 rounded m-auto">
                                <i style="color: white;" class="fas fa-bed fa-4x"></i>
                            </div>

                            <div class="text-center">
                                <h4 class="font-weight-bold text-white">{{$habitacion->NHabitacion}}</h4>
                            </div>
                            <?php if ($formularioDesactivado == NULL) { ?>
                            </a> <?php } ?>
                    </div>
                    @endforeach
                </div>

                <?php if($formularioDesactivado == FALSE){ ?>

                <div class="row d-flex justify-content-center">
                    <div class="col-xl-5">
                        {{ $habitaciones->links() }}
                    </div>
                </div>

                <?php } ?>



            </div>


        </div>



    </div>


   @include('utils.modals.room.modals_room')



    @stop