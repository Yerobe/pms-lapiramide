<script>
    $(document).ready(function() {
        $('#ModalNotice').modal('toggle')
    });


    $(document).ready(function() {
        $('#ModalRoom').modal('toggle')
    });

    $(document).ready(function() {
        $('#ModalUser').modal('toggle')
    });




    $(document).ready(function() { // Confirmación de la Eliminación de Un Teléfono de los Proveedores
        $('#ModalDeletePhone').modal('toggle')
    });


    $(document).ready(function() { // Confirmación de la Eliminación de Un Email de los Proveedores
        $('#ModalDeleteEmail').modal('toggle')
    });

    $(document).ready(function() { // Confirmación de la Eliminación de Un Miembro de la Reserva
        $('#ModalDeleteMember').modal('toggle')
    });
</script>




<?php

if (count($errors) != 0) { // Caso de Errores mediante FormRequest



    // Control de Modales en la parte de Proveedores [/provider/information/{id}]
    // Control de Apertura de las Ventanas modales en caso de Errores
    // Este se debe, a que en caso de error, se abren ambas ventanas modales,
    // determinaremos cual es el caso de error, y abriremos la correspondiente de manera automática.

    foreach ($errors->all() as $error) {

        if ($error == 'NameByPhone' || $error == 'PhoneByPhone') {
?>
            <script>
                $(document).ready(function() {
                    $('#ModalNewPhone').modal('toggle')
                });
            </script>
        <?php
            break;
        } elseif ($error == 'NameByEmail' || $error == 'EmailByEmail') {
        ?>
            <script>
                $(document).ready(function() {
                    $('#ModalNewEmail').modal('toggle')
                });
            </script>
    <?php
            break;
        }
    }


    ?>




    <script>
        $(document).ready(function() { // Usuarios - Caso de Error de Inserción
            $('#newUserModal').modal('toggle')
        });

        $(document).ready(function() { // Proveedores - Caso de Error de Inserción
            $('#ModalNewProvider').modal('toggle')
        });

        $(document).ready(function() { // Operadores - Caso de Error de Inserción
            $('#ModalNewOperator').modal('toggle')
        });

        $(document).ready(function() { // Miembros - Caso de Error de Inserción
            $('#ModalNewMember').modal('toggle')
        });
    </script>

<?php
}

?>

@if(Session::has('DeleteUserError'))
<script>
    $(document).ready(function() {
        $('#deleteUserModal').modal('toggle')
    });
</script>
@endif



<script>
    $(document).ready(function() {
        $('#ModalNoticeUser').modal('toggle')
    });
</script>



<!-- Ventana Modales de Clientes -->

@if(Session::has('ClientModalNotice'))
<script>
    $(document).ready(function() { // Confirmacion de la Creacion de un Cliente
        $('#ModalNoticeClient').modal('toggle')
    });
</script>
@endif


<script>
    $(document).ready(function() { // Confirmacion de la Creacion de un Cliente
        $('#updateClientModal').modal('toggle')
    });
</script>