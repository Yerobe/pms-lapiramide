


    <!-- Ventana Modal de Eliminar-->



    <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/users/delete" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete a User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">


                        <div class="form-row justify-content-around">
                            <div class="form-group col-md-10">
                                <p class="font-weight-bold ">Warning:</p>
                                <p class="">Remember that when deleting the user, it will not be able to be recovered again, confirm this, before proceeding with the elimination</p>
                            </div>
                        </div>

                        <div class="form-row justify-content-around">
                            <div class="form-group col-md-8">
                                <label for="nif">Please Insert NIF : </label>
                                <input value="" type="text" class="form-control" name="nif" required>
                            </div>
                        </div>

                        @if(Session::has('DeleteUserError'))

                        <div class="form-row justify-content-around">
                            <div class="form-group col-md-10 text-danger d-inline-block">
                                <p class="font-weight-bold">Error: </p>
                                {{Session::get('DeleteUserError')}}
                            </div>
                        </div>

                        @endif

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete User</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- Ventana Modal de Confirmación-->

    @if(Session::has('InsertUser') or Session::has('DeleteUserCorrect'))
    <div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {{Session::get('InsertUser')}}
                    {{Session::get('DeleteUserCorrect')}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @endif


    <!-- Ventana Modal de Actualización -->

    <?php if ($ventanaModal == TRUE) { ?>
        <div class="modal fade" id="ModalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">User {{$usuarioEdit->name ?? ''}} Update</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Name</label>
                                    <input value="{{$usuarioEdit->name ?? ''}}" type="text" class="form-control" placeholder="Name" name="name" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="surname">Surname</label>
                                    <input value="{{$usuarioEdit->surname ?? ''}}" type="text" class="form-control" placeholder="Surname" name="surname" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="nif">NIF</label>
                                    <input value="{{$usuarioEdit->NIF ?? ''}}" type="text" class="form-control" placeholder="Nif" name="nif" required>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input value="{{$usuarioEdit->email ?? ''}}" type="email" class="form-control" placeholder="Email" name="email" required>
                                </div>


                            </div>



                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="rol">Rol</label>
                                    <select name="rol" class="form-control">
                                        <option <?php if ($usuarioEdit->Rol == "Administrador") {
                                                    echo ("selected");
                                                } ?> value="Administrador">Administrator</option>
                                        <option <?php if ($usuarioEdit->Rol == "Recepcionista") {
                                                    echo ("selected");
                                                } ?> value="Recepcionista">Receptionist</option>
                                        <option <?php if ($usuarioEdit->Rol == "Camarero") {
                                                    echo ("selected");
                                                } ?> value="Camarero">Waiter</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="estado">Status</label>
                                    <select name="estado" class="form-control">
                                        <option <?php if ($usuarioEdit->Estado == 1) {
                                                    echo ("selected");
                                                } ?> value="1">Enabled</option>
                                        <option <?php if ($usuarioEdit->Estado == 0) {
                                                    echo ("selected");
                                                } ?> value="0">Disabled</option>
                                    </select>
                                </div>


                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php } ?>




    <!-- Modal Usuario Actualizado -->
    @if(Session::has('UserUpdate'))
    <div class="modal fade" id="ModalNoticeUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {{Session::get('UserUpdate')}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @endif



    <!-- Modal Usuario Creado -->



    <div class="modal fade" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/users/create" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create a New User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeName ?? ''}}" placeholder="Name" name="name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="surname">Surname</label>
                                <input value="{{old('surname') ?? ''}}" type="text" class="form-control {{$mensajeSurname ?? ''}}" placeholder="Surname" name="surname">
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="nif">NIF</label>
                                <input value="{{old('nif') ?? ''}}" type="text" class="form-control {{$mensajeNif ?? ''}}" placeholder="Nif" name="nif" required>
                            </div>


                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input value="{{old('email') ?? ''}}" type="email" class="form-control {{$mensajeEmail ?? ''}}" placeholder="Email" name="email" required>
                            </div>


                        </div>



                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="rol">Rol</label>
                                <select name="rol" class="form-control {{$mensajeNRol ?? ''}}">
                                    <option <?php if (old('rol') == "Administrador") {
                                                echo ("selected");
                                            } ?> value="Administrador">Administrator</option>
                                    <option <?php if (old('rol') == "Recepcionista") {
                                                echo ("selected");
                                            } ?> value="Recepcionista">Receptionist</option>
                                    <option <?php if (old('rol') == "Camarero") {
                                                echo ("selected");
                                            } ?> value="Camarero">Waiter</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="estado">Status</label>
                                <select name="estado" class="form-control {{$mensajeEstado ?? ''}}">
                                    <option <?php if (old('estado') == 1) {
                                                echo ("selected");
                                            } ?> value="1">Enabled</option>
                                    <option <?php if (old('estado') == 0) {
                                                echo ("selected");
                                            } ?> value="0">Disabled</option>
                                </select>
                            </div>


                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input value="" type="password" class="form-control {{$mensajePassword ?? ''}}" placeholder="Password" name="password" required>
                            </div>
                        </div>




                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Create User</button>
                        </div>
                    </div>


            </form>
        </div>
    </div>