 <!-- Ventana Modal del formulario de filtros de reservas -->


 <div class="modal fade" id="ModalSeeker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 <form action="" method="POST">
                     @csrf
                     <div class="form-row">
                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="type">Reservation number</label>
                             <input type="text" class="form-control" name="numeroReserva" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorReserva']}}" <?php } ?>>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">BookDay</label>
                             <input type="date" class="form-control" name="bookDay" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorBookDay']}}" <?php } ?>>
                         </div>

                         </div> 
                     <div class="form-row">

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">CheckIn</label>
                             <input type="date" class="form-control" name="fechaEntrada" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorFechaEntrada']}}" <?php } ?>>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">CheckOut</label>
                             <input type="date" class="form-control" name="fechaSalida" <?php if ($valoresSeleccionados != NULL) { ?> value="{{$valoresSeleccionados['valorFechaSalida']}}" <?php } ?>>
                         </div>


                     </div>

                     <div class="form-row">
                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">Travel Agent</label>
                             <select name="operador" class="form-control">
                                 <option value="Everyone">Everyone</option>

                                 @foreach ($operadores as $operador)
                                 <option value="{{$operador->Id}}" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorOperador'] == $operador->Id) {
                                                                        echo ("selected");
                                                                    } ?>>{{$operador->Nombre}}</option>
                                 @endforeach

                             </select>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">Number Room</label>
                             <select name="habitacion" class="form-control">
                                 <option value="Everyone">Everyone</option>

                                 @foreach ($habitaciones as $habitacion)
                                 <option value="{{$habitacion->Id}}" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorHabitacion'] == $habitacion->Id) {
                                                                            echo ("selected");
                                                                        } ?>>{{$habitacion->NHabitacion}}</option>
                                 @endforeach

                             </select>
                         </div>

                         </div> 
                     <div class="form-row">

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">Payment</label>
                             <select name="pagado" class="form-control">
                                 <option value="Everyone">Everyone</option>

                                 <option value="2" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorPagado'] == 2) {
                                                        echo ("selected");
                                                    } ?>>Paid</option>
                                 <option value="1" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorPagado'] == 1) {
                                                        echo ("selected");
                                                    } ?>>UnPaid</option>

                             </select>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="bath">Status</label>
                             <select name="estado" class="form-control">
                                 <option value="Everyone">Everyone</option>
                                 <option value="2" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorEstado'] == 2) {
                                                        echo ("selected");
                                                    } ?>>Active</option>
                                 <option value="1" <?php if ($valoresSeleccionados != NULL && $valoresSeleccionados['valorEstado'] == 1) {
                                                        echo ("selected");
                                                    } ?>>Cancelled</option>

                             </select>
                         </div>



                     </div>




             </div>
             <div class="modal-footer">

               
                     <button type="submit" class="btn btn-secondary">Start Search</button>
         

             </div>
             </form>
         </div>
     </div>
 </div>