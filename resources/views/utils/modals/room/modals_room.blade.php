 <!-- Ventana Modal del Apartamento Seleccionado-->


 <?php if ($habitacionSeleccionada != null  && $ventanaModal == TRUE) { ?>
     <div class="modal fade" id="ModalRoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <form action="" method="POST">
                 @method('PUT')
                 @csrf
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Apartment {{$habitacionSeleccionada->NHabitacion}} Update</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span onclick="changeURL(<?php echo ($paginaActual); ?>);" aria-hidden="true">&times;</span>
                         </button>
                     </div>

                     <div class="modal-body">

                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="Apartment number">Apartment number</label>
                                 <input value="{{$habitacionSeleccionada->NHabitacion}}" type="number" class="form-control" id="Apartment number" placeholder="Apartment number" disabled>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="level">Level</label>
                                 <select id="level" class="form-control" disabled>
                                     <option <?php if ($habitacionSeleccionada->Nivel == "Alto") {
                                                    echo ("selected");
                                                } ?>>Above</option>
                                     <option <?php if ($habitacionSeleccionada->Nivel == "Bajo") {
                                                    echo ("selected");
                                                } ?>>Under</option>
                                 </select>
                             </div>
                         </div>

                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="Type">Type</label>
                                 <select name="tipoApartamento" id="Type" class="form-control">
                                     <option value="Superior" <?php if ($habitacionSeleccionada->Tipo == "Superior") {
                                                                    echo ("selected");
                                                                } ?>>Suite</option>
                                     <option value="Normal" <?php if ($habitacionSeleccionada->Tipo == "Normal") {
                                                                echo ("selected");
                                                            } ?>>Normal</option>
                                 </select>
                             </div>

                             <div class="form-group col-md-6">
                                 <label for="Bath">Bath Type</label>
                                 <select name="tipoBano" id="Bath" class="form-control">
                                     <option value="Plato Ducha" <?php if ($habitacionSeleccionada->TipoBano == "Plato Ducha") {
                                                                        echo ("selected");
                                                                    } ?>>Shower plate</option>
                                     <option value="Banera" <?php if ($habitacionSeleccionada->TipoBano == "Banera") {
                                                                echo ("selected");
                                                            } ?>>Bath</option>
                                 </select>
                             </div>
                         </div>



                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="Bed">Type of Bed</label>
                                 <select name="tipoCama" id="Bed" class="form-control">
                                     <option value="Individual" <?php if ($habitacionSeleccionada->TipoCama == "Individual") {
                                                                    echo ("selected");
                                                                } ?>>Single Bed</option>
                                     <option value="Matrimonio" <?php if ($habitacionSeleccionada->TipoCama == "Matrimonio") {
                                                                    echo ("selected");
                                                                } ?>>Double Bed</option>
                                 </select>
                             </div>

                             <div class="form-group col-md-6">
                                 <label for="zona">Zone</label>
                                 <select name="zona" id="zona" class="form-control">
                                     <option value="Zona 1" <?php if ($habitacionSeleccionada->Zona == "Zona 1") {
                                                                echo ("selected");
                                                            } ?>>Zone 1</option>
                                     <option value="Zona 2" <?php if ($habitacionSeleccionada->Zona == "Zona 2") {
                                                                echo ("selected");
                                                            } ?>>Zone 2</option>

                                     <option value="Zona 3" <?php if ($habitacionSeleccionada->Zona == "Zona 3") {
                                                                echo ("selected");
                                                            } ?>>Zone 3</option>

                                     <option value="Zona 4" <?php if ($habitacionSeleccionada->Zona == "Zona 4") {
                                                                echo ("selected");
                                                            } ?>>Zone 4</option>
                                 </select>
                             </div>
                         </div>


                         <div class="form-row">

                             <div class="form-group col-md-6">
                                 <label for="pets">Accept Pets</label>
                                 <select name="mascotas" id="pets" class="form-control">
                                     <option value="1" <?php if ($habitacionSeleccionada->Mascotas == 1) {
                                                            echo ("selected");
                                                        } ?>>Yes</option>
                                     <option value="0" <?php if ($habitacionSeleccionada->Mascotas == 0) {
                                                            echo ("selected");
                                                        } ?>>No</option>
                                 </select>
                             </div>

                             <div class="form-group col-md-6">
                                 <label for="disabledPeople">Disabled People</label>
                                 <select name="discapacitados" id="disabledPeople" class="form-control">
                                     <option value="1" <?php if ($habitacionSeleccionada->Discapacitados == 1) {
                                                            echo ("selected");
                                                        } ?>>Yes</option>
                                     <option value="0" <?php if ($habitacionSeleccionada->Discapacitados == 0) {
                                                            echo ("selected");
                                                        } ?>>No</option>
                                 </select>
                             </div>

                         </div>


                         <div class="form-row">
                             <div class="form-group col-md-12">
                                 <label for="status">Status</label>
                                 <select name="estado" id="status" class="form-control">
                                     <option value="Disponible" <?php if ($habitacionSeleccionada->Estado == "Disponible") {
                                                                    echo ("selected");
                                                                } ?>>Available</option>
                                     <option value="Bloqueado" <?php if ($habitacionSeleccionada->Estado == "Bloqueado") {
                                                                    echo ("selected");
                                                                } ?>>Locked</option>
                                     <option value="No Limpio" <?php if ($habitacionSeleccionada->Estado == "No Limpio") {
                                                                    echo ("selected");
                                                                } ?>>Not Clen</option>
                                     <option value="Ocupado" <?php if ($habitacionSeleccionada->Estado == "Ocupado") {
                                                                    echo ("selected");
                                                                } ?>>Occupied</option>
                                 </select>
                             </div>
                         </div>


                     </div>
                     <div class="modal-footer">
                         <button onclick="changeURL(<?php echo ($paginaActual); ?>);" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         <button type="submit" class="btn btn-success">Save changes</button>
                     </div>
                 </div>
             </form>
         </div>
     </div>
 <?php } ?>











 <!-- Ventana Modal del formulario de filtros de reservas -->


 <div class="modal fade" id="ModalFilterRoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 <form action="" method="POST">
                     @csrf
                     <div class="form-row">
                         <div class="form-group col-md-6">
                             <label style="text-align: left;" for="type">Type</label>
                             <select name="tipoApartamento" id="type" class="form-control">
                                 <option>All</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorApartamento"] == "Normal") {
                                                echo ("selected");
                                            } ?> value="Normal">Normal</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorApartamento"] == "Superior") {
                                                echo ("selected");
                                            } ?> value="Superior">Suite</option>
                             </select>
                         </div>

                         <div class="form-group col-md-6">
                             <label style="text-align: left;" for="bath">Bath Type</label>
                             <select name="tipoBanos" id="bath" class="form-control">
                                 <option>All</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorBanos"] == "Plato Ducha") {
                                                echo ("selected");
                                            } ?> value="Plato Ducha">Shower Plate</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorBanos"] == "Banera") {
                                                echo ("selected");
                                            } ?> value="Banera">Bath</option>
                             </select>
                         </div>
                         </div>
                     <div class="form-row">
                         <div class="form-group col-md-6">
                             <label style="text-align: left;" for="bed">Type of Bed</label>
                             <select name="tipoCamas" id="bed" class="form-control">
                                 <option>All</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorCamas"] == "Individual") {
                                                echo ("selected");
                                            } ?> value="Individual">Single Bed</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorCamas"] == "Matrimonio") {
                                                echo ("selected");
                                            } ?> value="Matrimonio">Double Bed</option>
                             </select>
                         </div>
                         <div class="form-group col-md-6">
                             <label style="text-align: left;" for="pets">Pets</label>
                             <select name="mascotas" id="pets" class="form-control">
                                 <option>All</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorMascotas"] == 1) {
                                                echo ("selected");
                                            } ?> value="1">Yes</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorMascotas"] == 0) {
                                                echo ("selected");
                                            } ?> value="0">No</option>
                             </select>
                         </div>
                         </div>
                     <div class="form-row">
                         <div class="form-group col-md-6">
                             <label style="text-align: left;" for="disabled">Disabled People</label>
                             <select name="discapacitados" id="disabled" class="form-control">
                                 <option>All</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorDiscapacitados"] == 1) {
                                                echo ("selected");
                                            } ?> value="1">Yes</option>
                                 <option <?php if (isset($valoresSeleccionados) && $valoresSeleccionados["valorDiscapacitados"] == 0) {
                                                echo ("selected");
                                            } ?> value="0">No</option>
                             </select>
                         </div>
                       
                     </div>





             </div>
             <div class="modal-footer">


                 <button type="submit" class="btn btn-secondary">Start Search</button>

                 </form>
             </div>
         </div>
     </div>
 </div>