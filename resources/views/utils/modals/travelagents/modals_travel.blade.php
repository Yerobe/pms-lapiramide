<!-- Ventana Modal del formulario de filtros de Travel Agents -->


<div class="modal fade" id="ModalFilterTravel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="" method="POST">
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-xl-6">
                            <label style="text-align: left;" for="operador">Operator</label>
                            <select class="form-control {{$mensajeOperador ?? ''}}" name="operador" id="operador">
                                @foreach ($operadores as $operador)
                                <option value="{{$operador->Id}}" <?php if (old('operador') == $operador->Id || $valorOperador == $operador->Id) {
                                                                        echo ('selected');
                                                                    } ?>>{{$operador->Nombre}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-xl-6">
                            <label style="text-align: left;" for="fechaEntrada">From</label>
                            <input value="{{old('fechaEntrada') ?? $valorFechaEntrada}}" onchange="checkDates()" id="fecha_entrada" type="date" class="form-control {{$mensajeFechaEntrada ?? ''}}" name="fechaEntrada" required>
                        </div>


                    </div>

                    <div class="form-row">
                        <div class="form-group col-xl-6">
                            <label style="text-align: left;" for="fechaSalida">To</label>
                            <input value="{{old('fechaSalida') ?? $valorFechaSalida}}" id="fecha_salida" type="date" class="form-control {{$mensajeFechaSalida ?? ''}}" name="fechaSalida" required>
                        </div>

                        <div class="form-group col-xl-6">
                            <label style="text-align: left;" for="status">Status</label>
                            <select class="form-control" name="status" id="status">
                                <option value="null" <?php if ($valorEstado == null) {
                                                            echo ('selected');
                                                        } ?>>Everyone</option>
                                <option value="1" <?php if ($valorEstado == '1') {
                                                            echo ('selected');
                                                        } ?>>Active</option>
                                <option value="0" <?php if ($valorEstado == '0') {
                                                            echo ('selected');
                                                        } ?>>Cancelled</option>
                            </select>
                        </div>

                    </div>


            </div>
            <div class="modal-footer">


                <button type="submit" class="btn btn-secondary">Start Search</button>

                </form>
            </div>
        </div>
    </div>
</div>