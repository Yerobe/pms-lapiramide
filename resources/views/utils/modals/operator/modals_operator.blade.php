
<?php

if (count($errors) != 0) {


    foreach ($errors->all() as $error) {

        switch ($error) {

            case 'NameByPhone':
                $mensajeNameByPhone = "border border-danger";
                break;

            case 'PhoneByPhone':
                $mensajePhoneByPhone = "border border-danger";
                break;

            case 'NameByEmail':
                $mensajeNameByEmail = "border border-danger";
                break;

            case 'EmailByEmail':
                $mensajeEmailByEmail = "border border-danger";
                break;
        }
    }
}

?>


<!-- Ventana Modal Creación de Teléfono -->


<div class="modal fade" id="ModalNewPhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/operator/information/create/phone/{{$idOperator}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Phone</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Name">Name</label>
                            <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNameByPhone ?? ''}}" id="Name" placeholder="Name" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Phone">Phone</label>
                            <input value="{{old('phone') ?? ''}}" type="text" class="form-control {{$mensajePhoneByPhone ?? ''}}" id="Phone" placeholder="Phone" name="phone">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Phone</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Ventana Modal Creación de Email -->

<div class="modal fade" id="ModalNewEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/operator/information/create/email/{{$idOperator}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Name">Name</label>
                            <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNameByEmail ?? ''}}" id="Name" placeholder="Name" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Email">Email</label>
                            <input value="{{old('email') ?? ''}}" type="email" class="form-control {{$mensajeEmailByEmail ?? ''}}" id="Email" placeholder="Email" name="email">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Email</button>
                </div>
            </div>
        </form>
    </div>
</div>




<!-- Ventana Modal de Confirmaciónes-->

@if(Session::has('ModifyOperator') or Session::has('InsertPhoneOperator') or Session::has('DeletePhoneOperator') or Session::has('InsertEmailOperator') or Session::has('DeleteEmailOperator'))
<div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {{Session::get('InsertPhoneOperator')}}
                {{Session::get('DeletePhoneOperator')}}
                {{Session::get('InsertEmailOperator')}}
                {{Session::get('DeleteEmailOperator')}}
                {{Session::get('ModifyOperator')}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif



<!-- Ventana Modal Confirmación de Eliminación de Teléfono -->

@if($modalDeletePhone == TRUE)
<div class="modal fade" id="ModalDeletePhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Phone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p> Once you delete this record, you will not be able to access it again. Are you sure to delete it?</p>

                <p>Number Phone: {{$datoOperator}}</p>



            </div>
            <div class="modal-footer">
                <form action="/operator/information/delete/phone/{{$idOperator}}/{{$datoOperator}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif



<!-- Ventana modal Confirmacion de Eliminacion de Email -->

@if($modalDeleteEmail == TRUE)
<div class="modal fade" id="ModalDeleteEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p> Once you delete this record, you will not be able to access it again. Are you sure to delete it?</p>

                <p>Email: {{$datoOperator}}</p>



            </div>
            <div class="modal-footer">
                <form action="/operator/information/delete/email/{{$idOperator}}/{{$datoOperator}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif