 <!-- Ventana Modal de Creación de Proveedor -->



 <?php

    if (count($errors) != 0) {

        foreach ($errors->all() as $error) {

            switch ($error) {
                case 'Cif':
                    $mensajeCif = "border border-danger";
                    break;

                case 'Name':
                    $mensajeName = "border border-danger";
                    break;

                case 'Address':
                    $mensajeAddress = "border border-danger";
                    break;

                case 'Name':
                    $mensajeName = "border border-danger";
                    break;

                case 'Phone':
                    $mensajePhone = "border border-danger";
                    break;
            }
        }
    }

    ?>

 <div class="modal fade" id="ModalNewProvider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <form action="" method="POST">
             @csrf
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">New Provider</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>

                 <div class="modal-body">

                     <div class="form-row">
                         <div class="form-group col-md-6">
                             <label for="CIF">CIF</label>
                             <input value="{{old('cif') ?? ''}}" type="text" class="form-control {{$mensajeCif ?? ''}}" id="CIF" placeholder="CIF" name="cif" required>
                         </div>
                         <div class="form-group col-md-6">
                             <label for="Name">Name</label>
                             <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeName ?? ''}}" id="Name" placeholder="Name" name="name" required>
                         </div>
                     </div>




                     <div class="form-row">
                         <div class="form-group col-md-12">
                             <label for="Address">Address</label>
                             <input value="{{old('address') ?? ''}}" type="text" class="form-control {{$mensajeAddress ?? ''}}" id="Address" placeholder="Address" name="address" required>
                         </div>
                     </div>

                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-success">Create Provider</button>
                 </div>
             </div>
         </form>
     </div>
 </div>





 <!-- Ventana Modal de Confirmación-->

 @if(Session::has('InsertProvider') or Session::has('ModifyProvider'))
 <div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 {{Session::get('InsertProvider')}}
                 {{Session::get('ModifyProvider')}}

             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div>
         </div>
     </div>
 </div>
 @endif


