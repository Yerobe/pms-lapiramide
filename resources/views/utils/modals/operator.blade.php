


 <?php

if (count($errors) != 0) {

    foreach ($errors->all() as $error) {

        switch ($error) {
            case 'Name':
                $mensajeName = "border border-danger";
                break;
        }
    }
}

?>




<!-- Ventana Modal de Creación de Operadores -->

<div class="modal fade" id="ModalNewOperator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <form action="" method="POST">
             @csrf
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">New Operator</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>

                 <div class="modal-body">

                     <div class="form-row">
                         <div class="form-group col-md-12">
                             <label for="Name">Name</label>
                             <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeName ?? ''}}" id="Name" placeholder="Name" name="name">
                         </div>
                     </div>

                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-dark">Create Operator</button>
                 </div>
             </div>
         </form>
     </div>
 </div>














  <!-- Ventana Modal de Confirmación-->

  @if(Session::has('InsertOperator') or Session::has('ModifyOperator'))
 <div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 {{Session::get('InsertOperator')}}
                 {{Session::get('ModifyOperator')}}

             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div>
         </div>
     </div>
 </div>
 @endif