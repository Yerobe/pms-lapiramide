<!-- Ventana Modal de Creación de Proveedor -->



<?php

if (count($errors) != 0) {


    foreach ($errors->all() as $error) {

        switch ($error) {

            case 'NameByPhone':
                $mensajeNameByPhone = "border border-danger";
                break;

            case 'PhoneByPhone':
                $mensajePhoneByPhone = "border border-danger";
                break;

            case 'NameByEmail':
                $mensajeNameByEmail = "border border-danger";
                break;

            case 'EmailByEmail':
                $mensajeEmailByEmail = "border border-danger";
                break;
        }
    }
}

?>

<!-- Ventana Modal Creación de Teléfono -->



<div class="modal fade" id="ModalNewPhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/provider/information/create/phone/{{$idProveedor}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Phone</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Name">Name</label>
                            <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNameByPhone ?? ''}}" id="Name" placeholder="Name" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Phone">Phone</label>
                            <input value="{{old('phone') ?? ''}}" type="text" class="form-control {{$mensajePhoneByPhone ?? ''}}" id="Phone" placeholder="Phone" name="phone">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Phone</button>
                </div>
            </div>
        </form>
    </div>
</div>




<!-- Ventana Modal Creación de Email -->



<div class="modal fade" id="ModalNewEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/provider/information/create/email/{{$idProveedor}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Name">Name</label>
                            <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNameByEmail ?? ''}}" id="Name" placeholder="Name" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Email">Email</label>
                            <input value="{{old('email') ?? ''}}" type="email" class="form-control {{$mensajeEmailByEmail ?? ''}}" id="Email" placeholder="Email" name="email">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Email</button>
                </div>
            </div>
        </form>
    </div>
</div>




<!-- Ventana Modal de Confirmación-->

@if(Session::has('ModifyProvider') or Session::has('InsertPhoneProvider') or Session::has('DeletePhoneProvider') or Session::has('InsertEmailProvider') or Session::has('DeleteEmailProvider'))
<div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {{Session::get('InsertPhoneProvider')}}
                {{Session::get('DeletePhoneProvider')}}
                {{Session::get('InsertEmailProvider')}}
                {{Session::get('DeleteEmailProvider')}}
                {{Session::get('ModifyProvider')}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif




<!-- Ventana modal Confirmacion de Eliminacion de Telefono -->

@if($modalDeletePhone == TRUE)
<div class="modal fade" id="ModalDeletePhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Phone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p> Once you delete this record, you will not be able to access it again. Are you sure to delete it?</p>

                <p>Number Phone: {{$datoProveedor}}</p>



            </div>
            <div class="modal-footer">
                <form action="/provider/information/delete/phone/{{$idProveedor}}/{{$datoProveedor}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif



<!-- Ventana modal Confirmacion de Eliminacion de Email -->

@if($modalDeleteEmail == TRUE)
<div class="modal fade" id="ModalDeleteEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p> Once you delete this record, you will not be able to access it again. Are you sure to delete it?</p>

                <p>Email: {{$datoProveedor}}</p>



            </div>
            <div class="modal-footer">
                <form action="/provider/information/delete/email/{{$idProveedor}}/{{$datoProveedor}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif