<?php

if (count($errors) != 0) {


    foreach ($errors->all() as $error) {

        switch ($error) {

            case 'NifByMember':
                $mensajeNifByNif = "border border-danger";
                break;

            case 'NameByMember':
                $mensajeNameByName = "border border-danger";
                break;

            case 'SurnameByMember':
                $mensajeSurnameBySurname = "border border-danger";
                break;
        }
    }
}

?>


<div class="modal fade" id="ModalNewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/edit/reservation/{{$idReserve}}/create/member" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Member</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="Nif">Identification Number</label>
                            <input value="{{old('nif') ?? ''}}" type="text" class="form-control {{$mensajeNifByNif ?? ''}}" id="Nif" placeholder="Identification Number" name="nif" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Name">Name</label>
                            <input value="{{old('name') ?? ''}}" type="text" class="form-control {{$mensajeNameByName ?? ''}}" id="Name" placeholder="Name" name="name" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="surname">Surname</label>
                            <input value="{{old('surname') ?? ''}}" type="text" class="form-control {{$mensajeSurnameBySurname ?? ''}}" id="Surname" placeholder="Surname" name="surname" required>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Member</button>
                </div>
            </div>
        </form>
    </div>
</div>





<!-- Confirmation Modal-->

@if(Session::has('InsertMember') or Session::has('DeleteMember'))
<div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {{Session::get('InsertMember')}}
                {{Session::get('DeleteMember')}}


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endif





<!-- Ventana modal Confirmacion de Eliminacion de un Cliente -->

@if($modalDeleteMember == TRUE)
<div class="modal fade" id="ModalDeleteMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p> Once you delete this record, you will not be able to access it again. Are you sure to delete it?</p>

                @foreach ($miembroDelete as $miembro)

                <?php $idMiembro = $miembro['Id']; ?>

                <p><b>Number:</b> {{$miembro['NIF']}}</p>
                <p><b>Name:</b> {{$miembro['Nombre']}}</p>
                <p><b>Surname:</b> {{$miembro['Apellidos']}}</p>

                @endforeach

            </div>
            <div class="modal-footer">
                <form action="/edit/reservation/{{$idReserve}}/{{$idMiembro}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif







<!-- Confirmation Subida de Documentos /  Error de Documentos -->

@if(Session::has('InserDNI') or Session::has('ErrorInsertDNI'))
<div class="modal fade" id="ModalNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if (Session::has('ErrorInsertDNI')) {
                ?>
                    <p class="<?php echo ('text-danger'); ?>">Error</p>
                <?php
                } ?>
                {{Session::get('InserDNI')}}
                {{Session::get('ErrorInsertDNI')}}


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endif






<!-- Modal Formulario Creacion Factura -->


<div class="modal fade" id="ModalCreateInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/create/invoice/{{$idReserve}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">


                    <label for="client">Select the Client :</label>

                    <select name="client" id="client" class="form-control">


                        @foreach($clientes as $cliente)
                        <option value="{{$cliente->Id}}">{{$cliente->Nombre}}</option>
                        @endforeach

                    </select>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Create Invoice</button>
                </div>
            </div>
        </form>
    </div>
</div>