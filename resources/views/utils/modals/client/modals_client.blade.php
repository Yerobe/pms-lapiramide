 <!-- Ventana Modal del formulario de filtros de reservas -->


 <div class="modal fade" id="newClientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">New Client</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 <form action="/clients/create" method="POST">
                     @csrf
                     <div class="form-row">
                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="type">CIF</label>
                             <input type="text" class="form-control" name="cif" value="" required>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="type">Name</label>
                             <input type="text" class="form-control" name="name" value="" required>
                         </div>

                     </div>

                     <div class="form-row">
                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="type">Direccion</label>
                             <input type="text" class="form-control" name="direccion" value="" required>
                         </div>

                         <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                             <label style="text-align: left;" for="type">CP - Province</label>
                             <input type="text" class="form-control" name="cp" value="" required>
                         </div>

                     </div>


                     <div class="form-row">
                         <div class="d-inline-block font-weight-bold">Note:</div>
                         <div class="d-inline-block mt-3">All fields are required. It must be taken into account that these data will be reflected in the next invoices, which are related to them.</div>
                     </div>

                     <div class="modal-footer">
                         <button type="submit" class="btn btn-secondary">Create Client</button>
                     </div>
                 </form>
             </div>
         </div>
     </div>

 </div>




 <!-- Ventana Modal de Actualizacion del Cliente -->

 <?php if ($clienteUpdate != null) { ?>

     <div class="modal fade" id="updateClientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Update Client</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">

                     <form action="/clients/update" method="POST">
                         @method('PUT')
                         @csrf

                         <input type="hidden" name="id" value="<?php echo ($clienteUpdate[0]->Id); ?>">

                         <div class="form-row">
                             <div class="form-group col-12">
                                 <label style="text-align: left;" for="type">Id</label>
                                 <input type="text" class="form-control" name="id-null" value="<?php echo ($clienteUpdate[0]->Id); ?>" disabled>
                             </div>
                         </div>
                   


                         <div class="form-row">
                             <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                 <label style="text-align: left;" for="type">CIF</label>
                                 <input type="text" class="form-control" name="cif" value="<?php echo ($clienteUpdate[0]->CIF); ?>" required>
                             </div>

                             <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                 <label style="text-align: left;" for="type">Name</label>
                                 <input type="text" class="form-control" name="name" value="<?php echo ($clienteUpdate[0]->Nombre); ?>" required>
                             </div>

                         </div>

                         <div class="form-row">
                             <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                 <label style="text-align: left;" for="type">Direccion</label>
                                 <input type="text" class="form-control" name="direccion" value="<?php echo ($clienteUpdate[0]->Direccion); ?>" required>
                             </div>

                             <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                 <label style="text-align: left;" for="type">CP - Province</label>
                                 <input type="text" class="form-control" name="cp" value="<?php echo ($clienteUpdate[0]->CP); ?>" required>
                             </div>

                         </div>


                         <div class="form-row">
                             <div class="d-inline-block font-weight-bold">Note:</div>
                             <div class="d-inline-block mt-3">All fields are required. It must be taken into account that these data will be reflected in the next invoices, which are related to them.</div>
                             <div class="d-inline-block mt-3">When modifying the client's data, the invoices made prior to the previous modification are not modified.</div>
                         </div>

                         <div class="modal-footer">
                             <button type="submit" class="btn btn-secondary">Update Client</button>
                         </div>
                     </form>
                 </div>
             </div>
         </div>



     <?php } ?>



     <!-- Confirmation Modal-->

     @if(Session::has('ClientModalNotice'))
     <div class="modal fade" id="ModalNoticeClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">

                     {{Session::get('ClientModalNotice')}}

                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 </div>
             </div>
         </div>
     </div>

     @endif