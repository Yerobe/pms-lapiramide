<!-- Ventana Modal del formulario de filtros de Travel Agents -->


<div class="modal fade" id="ModalFilterRestaurant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            <form action="" method="POST">
                            @csrf
                            <div class="form-row">

                                <div class="form-group col-xl-6">
                                    <label style="text-align: left;" for="bath">Initial Date</label>
                                    <input value="{{$valorFechaSeleccionadaInicial}}" id="fecha_entrada" type="date" class="form-control" name="fechaSeleccionadaInicial" onchange="checkDates()">
                                </div>

                                <div class="form-group col-xl-6">
                                    <label style="text-align: left;" for="bath">Final Date</label>
                                    <input value="{{$valorFechaSeleccionadaFinal}}" id="fecha_salida" type="date" class="form-control" name="fechaSeleccionadaFinal">
                                </div>

                            </div>


            </div>
            <div class="modal-footer">


                <button type="submit" class="btn btn-secondary">Start Search</button>

                </form>
            </div>
        </div>
    </div>
</div>