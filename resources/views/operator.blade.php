@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">



            <button type="button" class="btn btn-outline-success rounded-0 text-left" data-toggle="modal" data-target="#ModalNewOperator">
                <i class="fa fa-plus mr-2"></i>
                <span>New Operator</span>
            </button>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">


            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Operators</p>
                            <h4>{{count($operators)}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Operators Enabled</p>
                            <h4>{{$operatorsEnabled->total()}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-address-book fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Operators Disabled</p>
                            <h4>{{$operatorsDisabled->total()}}</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-6 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="4">
                                    Enabled Operators
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>Name</th>
                                <th>Information</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(!empty($operatorsEnabled))


                            @foreach ($operatorsEnabled as $operator)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $operator->Nombre }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/operator/information/{{$operator->Id}}" target="_blank"><i class="fas fa-info-circle fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>




                        @if(count($operatorsEnabled)==0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no Operators that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>

                @if(count($operatorsEnabled)!=0)
            <div class="row d-flex justify-content-left">
                <div class="col-xl-6">
                    {{ $operatorsEnabled->links() }}
                </div>
            </div>
            @endif
            
            </div>

           



            <div class="col col-6 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="4">
                                    Disabled Operators
                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>Name</th>
                                <th>Information</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">


                            @if(!empty($operatorsDisabled))


                            @foreach ($operatorsDisabled as $operator)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $operator->Nombre }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/operator/information/{{$operator->Id}}" target="_blank"><i class="fas fa-info-circle fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif





                        </tbody>

                        @if(count($operatorsDisabled) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no Operators that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif



                    </table>
                </div>

                @if(count($operatorsDisabled)!=0)
                <div class="row d-flex justify-content-left">
                    <div class="col-xl-12">
                        {{ $operatorsDisabled->links() }}
                    </div>
                </div>
                @endif
            </div>






        </div>



    </div>


    @include('utils.modals.operator')
    @stop