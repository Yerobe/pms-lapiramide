@extends('layouts.master')

@section('content')



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">





            <a class="btn btn-outline-success rounded-0 text-left" href="/create/reservation">
                <i class="fa fa-plus mr-2"></i>
                <span>New Reservation</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">



            <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">

                <div class="bg-white h-100 p-2 rounded shadow w-25 font-weight-bold">
                    <a style="margin-right: 5%;" href="/home/yesterday" class="d-inline-block my-auto text-decoration-none text-dark fas fa-arrow-left"></a>
                    <a href="/home" class="d-inline-block my-auto text-decoration-none text-dark">Today</a>
                    <a style="margin-left: 5%;" href="/home/tomorrow" class="d-inline-block my-auto text-decoration-none text-dark fas fa-arrow-right"></a>
                </div>



            </div>

            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-paste fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Bookings</p>
                            <h4>{{$totalBookings}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FD7E14; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-bed fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Rooms Available</p>
                            <h4>{{$freeRooms}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-users fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Check In</p>
                            <h4>{{$totalEntradas}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Check Out</p>
                            <h4>{{$totalSalidas}}</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    @if ($fecha == "yesterday" || $fecha == 'tomorrow')
                                    CheckIn Details - Day: <?php echo (date("d-m-Y", strtotime($fechaactual))); ?>
                                    @else
                                    CheckIn Today Details
                                    @endif

                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NºReservation</th>
                                <th>CheckIn</th>
                                <th>CheckOut</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Pets</th>
                                <th>Total</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Apartaments</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(!empty($entradas))


                            <?php $i = 0; ?>

                            @foreach ($entradas as $entrada)

                            <?php $i++; ?>

                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle" style="<?php if($entrada->IdPais === null){echo("color: #FF8F00");}else{
                                    if($entrada->IdComunidadAutonoma === null){
                                        echo("color: #FF8F00");
                                    }else{
                                        if($entrada->IdComunidadAutonoma == 5){
                                            if($entrada->IdCanarias === null){
                                                echo("color: #FF8F00");
                                            }
                                        }
                                    }
                                } ?>">{{ $entrada->NumeroReserva }}</td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($entrada->FechaEntrada))); ?></td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($entrada->FechaSalida))); ?></td>


                                <?php if ($entrada->EstadoPago == 'Operator') { ?>
                                    <td class="align-middle text-warning">
                                        Operator
                                    </td>
                                <?php } else { ?>

                                    <?php if ($entrada->Prepago == $entrada->Precio_Total) { ?>
                                        <td class="align-middle text-success">
                                            Paid
                                        </td>
                                    <?php } ?>

                                    <?php if ($entrada->Prepago != $entrada->Precio_Total) { ?>
                                        <td class="align-middle text-danger">
                                            UnPaid
                                        </td>
                                <?php }
                                } ?>



                                <td class="align-middle">{{ $entrada->NombreCliente }}</td>
                                <td class="align-middle">{{ $entrada->ApellidosCliente }}</td>
                                <td class="align-middle">{{ $entrada->NumeroMascotas }}</td>
                                <td class="align-middle">{{ $entrada->Precio_Total }}€</td>
                                <td class="align-middle">{{ $entrada->NumeroAdultos }}</td>
                                <td class="align-middle">{{ $entrada->NumeroNinios}}</td>
                                <td class="align-middle">{{ $entrada->NumeroHabitacion}}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/edit/reservation/{{$entrada->Id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>




                        @if(count($entradas)==0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no reservations that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>
            </div>



            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">

                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    @if ($fecha == "yesterday" || $fecha == 'tomorrow')
                                    CheckOut Details - Day: <?php echo (date("d-m-Y", strtotime($fechaactual))); ?>
                                    @else
                                    CheckOut Today Details
                                    @endif

                                </th>
                            </tr>
                        </thead>

                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NºReservation</th>
                                <th>CheckIn</th>
                                <th>CheckOut</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Pets</th>
                                <th>Total</th>
                                <th>Adults</th>
                                <th>Children</th>
                                <th>Apartaments</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">


                            @if(!empty($salidas))


                            <?php $i = 0; ?>

                            @foreach ($salidas as $salida)

                            <?php $i++; ?>

                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle">{{ $salida->NumeroReserva }}</td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($salida->FechaEntrada))); ?></td>
                                <td class="align-middle"><?php echo (date("d-m-Y", strtotime($salida->FechaSalida))); ?></td>

                                <?php if ($salida->EstadoPago == 'Operator') { ?>
                                    <td class="align-middle text-warning">
                                        Operator
                                    </td>
                                <?php } else { ?>

                                    <?php if ($salida->Prepago == $salida->Precio_Total) { ?>
                                        <td class="align-middle text-success">
                                            Paid
                                        </td>
                                    <?php } ?>

                                    <?php if ($salida->Prepago != $salida->Precio_Total) { ?>
                                        <td class="align-middle text-danger">
                                            UnPaid
                                        </td>
                                <?php }
                                } ?>

                                <td class="align-middle">{{ $salida->NombreCliente }}</td>
                                <td class="align-middle">{{ $salida->ApellidosCliente }}</td>
                                <td class="align-middle">{{ $salida->NumeroMascotas }}</td>
                                <td class="align-middle">{{ $salida->Precio_Total }}€</td>
                                <td class="align-middle">{{ $salida->NumeroAdultos }}</td>
                                <td class="align-middle">{{ $salida->NumeroNinios}}</td>
                                <td class="align-middle">{{ $salida->NumeroHabitacion}}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/edit/reservation/{{$salida->Id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>
                            </tr>

                            @endforeach
                            @endif





                        </tbody>

                        @if(count($salidas) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently, there are no reservations that match this search
                                </td>
                            </tr>
                        </tbody>
                        @endif



                    </table>
                </div>
            </div>



        </div>



    </div>



    @include('utils.modals.home')


    @stop