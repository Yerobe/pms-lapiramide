<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LaPiramide</title>

    <style>
        
    *{
        text-align: center;
        margin: auto;
    }

    img{
        width: 20%;
        margin-top: 2%;
    }

    h1{
        margin-top: 1%;
        font-size: 150px;
    }

    p{
        font-size: 30px;
        margin-top: 20px;
    }

    a{
        color: black;
    }

    .small{
        font-size: 20px;
    }
    </style>
</head>



<body>

<img src="{{asset('images/404.png')}}" alt="Flaticon: Pixel Perfect">

    <h1>Error 404</h1>
    <p>Woops. Looks Like this page doesn´t exist.</p>
    <p><a href="/home">Go La Piramide Home</a></p>
    <p class="small">If the error persists, contact Jerobel José - [Administrator]</p>



</body>

</html>