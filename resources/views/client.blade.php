@extends('layouts.master')

@section('content')


<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">




            <button type="button" class="btn btn-outline-success rounded-0 text-left" data-toggle="modal" data-target="#newClientModal">
                <i class="fa fa-plus mr-2"></i>
                <span>New Client</span>
            </button>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 92vh;" class="row">



        <div class="col col-12 mt-3 justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-center">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 shadow ">
                        <div style="background-color: #5A6268; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Clients</p>
                            <h4>{{$totalClients}}</h4>
                        </div>
                    </div>
                    </div>

                </div>
            


            <div class="col col-12 pl-5 pr-5 mt-3">
                <div class="table-responsive">
                    <table class="table rounded shadow">


                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="6">
                                    Last 10 Clients
                                </th>
                            </tr>
                        </thead>


                        <thead class="table-light">
                            <tr class="text-center">
                                <th>Id</th>
                                <th>CIF</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>CP</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(count($clientes) != 0)

                            <?php $i = 0; ?>

                            @foreach ($clientes as $cliente)

                            <?php $i++; ?>

                            <tr class="text-center h-100 <?php if ($i % 2 == 0) {
                                                                echo ('bg-light');
                                                            } ?>">
                                <td class="align-middle">{{ $cliente->Id }}</td>
                                <td class="align-middle">{{ $cliente->CIF }}</td>
                                <td class="align-middle">{{ $cliente->Nombre }}</td>
                                <td class="align-middle">{{ $cliente->Direccion }}</td>
                                <td class="align-middle">{{ $cliente->CP }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/clients/{{$cliente->Id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>



                        @if(count($clientes) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="6">
                                    Currently there is no record with the specified search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>

            </div>





        </div>



    </div>



@include('utils.modals.client.modals_client')





    @stop