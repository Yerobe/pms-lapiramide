@extends('layouts.master')

@section('content')


<?php

if (count($errors) != 0) {

    foreach ($errors->all() as $error) {

        switch ($error) {

            case 'Name':
                $mensajeName = "border border-danger";
                break;

            case 'Cif':
                $mensajeCIF = "border border-danger";
                break;

            case 'Address':
                $mensajeAddress = "border border-danger";
                break;
        }
    }
}

?>

<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">


    <div class="row">


        <div class="form-group col-md-12">

            <a class="btn btn-outline-success rounded-0 text-left" href="/provider">
                <i class="fa fa-arrow-left mr-2"></i>
                <span>Go Back</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>





    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row d-flex justify-content-around p-3">


        <div class="col col-11 bg-white rounded shadow">


            <form action="" method="POST">
                @method('PUT')
                @csrf

                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Provider Information</h3>
                    </div>
                </div>


                <div class="form-row mt-3">

                    <div class="form-group  col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="cif">CIF :</label>
                        <input value="{{$provider[0]['CIF']}}" type="text" class="form-control {{$mensajeCIF ?? ''}}" id="cif" name="cif" required>
                    </div>


                    <div class="form-group  col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="name">Name :</label>
                        <input value="{{$provider[0]['Nombre']}}" type="text" class="form-control {{$mensajeName ?? ''}}" id="name" name="name" required>
                    </div>

                    <div class="form-group  col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="address">Address :</label>
                        <input value="{{$provider[0]['Direccion']}}" type="text" class="form-control {{$mensajeAddress ?? ''}}" id="address" name="address" required>
                    </div>


                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Status</h3>
                    </div>
                </div>

                <div class="form-row mt-3">





                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 d-flex">
                        <label style="margin-top: 42px;" class="mr-4" for="email">Disabled Provider :</label>
                        <label class="chkbx" style="margin-top: 40px;">
                            <input name="estado" type="checkbox" <?php if ($provider[0]['Estado'] == 0) {
                                                                        echo ("checked");
                                                                    } ?>>
                            <span class="x "></span>
                        </label>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 mt-4">
                        <button style="background-color: #5A6268;" type="submit" class="btn btn-block text-white rounded-0 p-3">Update</button>
                    </div>

                    <div class="form-group col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 mt-4">
                        <a href="/provider" class="btn btn-block btn-dark btn-block rounded-0 mt-0 p-3">Cancel</a>
                    </div>

                </div>


            </form>







            <div class="form-row border-bottom mt-5">
                <div class="col col-6">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Related Phones</h3>
                </div>
                <div class="col col-6">
                    <h3 style="font-size: 20px;" class="display-3 blockquote mt-3">Related Emails</h3>
                </div>
            </div>



            <div class="form-row">
                <div class="col col-6 ml-auto mr-auto">
                    <div class="table-responsive">
                        <table class="table rounded shadow">

                            <thead class="table-light">
                                <tr>
                                    <th class="text-right" style="color: #5B626B;" colspan="3">
                                        <button style="background-color: #FDFDFE;" type="button" class="text-dark text-decoration-none border-0" data-toggle="modal" data-target="#ModalNewPhone">
                                            +
                                        </button>
                                    </th>
                                </tr>
                            </thead>

                            <thead class="table-light">
                                <tr class="text-center">
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody class="table-light">



                                @if(!empty($telefonosProvider))


                                @foreach ($telefonosProvider as $telefono)



                                <tr class="text-center h-100">
                                    <td class="align-middle">{{ $telefono->Nombre }}</td>
                                    <td class="align-middle">{{ $telefono->Telefono }}</td>
                                    <td class="align-middle"><a class="text-dark text-decoration-none" href="/provider/information/{{$telefono->IdProveedor}}/phone/{{$telefono->Telefono}}"><i class="fas fa-times"></i></a></td>

                                </tr>

                                @endforeach
                                @endif



                            </tbody>




                            @if(count($telefonosProvider)==0)
                            <tbody class="table-light">
                                <tr class="text-center">
                                    <td class="align-middle" colspan="3">
                                        Currently, there are no telefone that match this search
                                    </td>
                                </tr>
                            </tbody>
                            @endif





                        </table>
                    </div>
                </div>

                <div class="col col-6 ml-auto mr-auto">
                    <div class="table-responsive ">
                        <table class="table rounded shadow">

                            <thead class="table-light">
                                <tr>
                                    <th class="text-right" style="color: #5B626B;" colspan="3">
                                        <button style="background-color: #FDFDFE;" type="button" class="text-dark text-decoration-none border-0" data-toggle="modal" data-target="#ModalNewEmail">
                                            +
                                        </button>
                                    </th>
                                </tr>
                            </thead>

                            <thead class="table-light">
                                <tr class="text-center">
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody class="table-light">



                                @if(!empty($emailsProvider))


                                @foreach ($emailsProvider as $email)



                                <tr class="text-center h-100">
                                    <td class="align-middle">{{ $email->Nombre }}</td>
                                    <td class="align-middle">{{ $email->Email }}</td>
                                    <td class="align-middle"><a class="text-dark text-decoration-none" href="/provider/information/{{$email->IdProveedor}}/email/{{$email->Email}}"><i class="fas fa-times"></i></a></td>

                                </tr>

                                @endforeach
                                @endif



                            </tbody>




                            @if(count($emailsProvider)==0)
                            <tbody class="table-light">
                                <tr class="text-center">
                                    <td class="align-middle" colspan="3">
                                        Currently, there are no email that match this search
                                    </td>
                                </tr>
                            </tbody>
                            @endif





                        </table>
                    </div>
                </div>
            </div>















        </div>




    </div>





</div>



</div>



</div>


@include('utils.modals.provider.modals_provider')

@stop