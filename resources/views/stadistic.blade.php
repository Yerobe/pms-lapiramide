@extends('layouts.master')

@section('content')





<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">



            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-5 row justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Reserves</p>
                            <h4>{{count($totalReservation)}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-door-open fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Best Operator</p>
                            <h4>{{$getBestOperator[0]["Nombre"] ?? "None"}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-building fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">La Piramide</p>
                            <h4>{{$totalLaPiramide ?? 0}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FD7E14; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-building fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Half Price</p>
                            <h4>{{$halfPrice}}€</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-12 pl-5 pr-5">
                <div class="table-responsive">
                    <table class="table rounded shadow">



                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B; text-align:left;" class="align-middle" colspan="9">
                                    Statistics Section
                                </th>
                                <form action="" method="POST">
                                    <th style="text-align:right;" class="align-middle" colspan="1">

                                        @csrf
                                        <input id="fecha_entrada" value="{{$firstDay}}" name="diaInicial" class="d-inline-block form-control" type="date" onchange="checkDates()" required>

                                    </th>
                                    <th colspan="1">
                                        <input id="fecha_salida" value="{{$lastDay}}" name="diaFinal" class="d-inline-block form-control" type="date" required>
                                    </th>
                                    <th class="align-middle" colspan="1">
                                        <button type="submit" class="border-0 btn btn-block btn-dark">
                                            Search
                                        </button>

                                    </th>
                                </form>
                            </tr>
                        </thead>


                        <tbody class="table-light">

                            <tr class="text-center h-100">
                                <th class="align-middle" colspan="11">Show Country Statistics</th>

                                <th class="align-middle" colspan="1">
                                    <a class="text-secondary" href="/stadistics/countrys/{{$firstDay}}/{{$lastDay}}" target="_blank">
                                        <i class="fa fa-eye"></i></a>
                                </th>

                            </tr>

                            <tr class="text-center h-100">
                                <th class="align-middle border-top" colspan="11">Show Statistics of the Autonomous Communities of Spain</th>
                                <th class="align-middle border-top" colspan="1"> <a class="text-secondary" href="/stadistics/communities/{{$firstDay}}/{{$lastDay}}" target="_blank"><i class="fa fa-eye"></i></th>
                            </tr>


                            <tr class="text-center h-100">
                                <th class="align-middle border-top" colspan="11">Show Statistics of the Canary Islands</th>
                                <th class="align-middle border-top" colspan="1"> <a class="text-secondary" href="/stadistics/canaryisland/{{$firstDay}}/{{$lastDay}}" target="_blank"><i class="fa fa-eye"></i></th>
                            </tr>

                            <!-- 
                            <tr class="text-center text-secondary h-100">
                                <th class="align-middle border-top" colspan="11">If you want to download using Excel, you can do it on a monthly basis</th>
                                <th class="align-middle border-top" colspan="1"> <a class="text-secondary" href="/stadistics/communities"><i class="fa fa-file-excel"></i></th>
                             </tr> -->

                        </tbody>





                    </table>
                </div>
            </div>


            @if(count($totalReservation) != 0)
            <div class="col-12 pl-5 pr-5 ">

                <div class="row justify-content-around">
                    <div class="col col-5 bg-white rounded shadow m-2">
                        <canvas class="p-3" style="width: 100%; height: 100%;" id="myChart"></canvas>
                    </div>

                    <div class="col col-5 bg-white rounded shadow m-2">
                        <canvas class="p-3" style="width: 100%; height: 100%;" id="mySecondChart"></canvas>
                    </div>
                </div>

            </div>
            @endif

        </div>


    </div>



    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [<?php foreach ($operatorCount as $operador) {

                                if ($operador === end($operatorCount)) {
                                    echo (' " ' . $operador["Nombre"] . ' " ');
                                } else {
                                    echo (' " ' . $operador["Nombre"] . ' " ' . " , ");
                                }
                            } ?>],
                datasets: [{
                    label: 'Reservation By Operators',
                    data: [<?php foreach ($operatorCount as $operador) {

                                if ($operador === end($operatorCount)) {
                                    echo (' " ' . $operador["CantidadReservas"] . ' " ');
                                } else {
                                    echo (' " ' . $operador["CantidadReservas"] . ' " ' . " , ");
                                }
                            } ?>],
                    backgroundColor: [
                        'rgba(255,0,0,0.5)',
                        'rgba(0,255,0,0.5)',
                        'rgba(100,100,100,0.5)',
                        'rgba(220,100,50,0.5)',
                        'rgba(220,255,0,0.5)',
                        'rgba(0,0,255,0.5)'
                    ],
                    borderColor: [
                        'dark'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Reservation by Operator'
                    }
                }
            },
        });



        var ctx2 = document.getElementById('mySecondChart').getContext('2d');
        var mySecondChart = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: [<?php foreach ($operatorCount as $operador) {

                                if ($operador === end($operatorCount)) {
                                    echo (' " ' . $operador["Nombre"] . ' " ');
                                } else {
                                    echo (' " ' . $operador["Nombre"] . ' " ' . " , ");
                                }
                            } ?>],
                datasets: [{
                    label: 'Bookings',
                    data: [<?php foreach ($operatorCount as $operador) {

                                if ($operador === end($operatorCount)) {
                                    echo (' " ' . $operador["CantidadReservas"] . ' " ');
                                } else {
                                    echo (' " ' . $operador["CantidadReservas"] . ' " ' . " , ");
                                }
                            } ?>],
                    backgroundColor: [
                        'dark'
                    ],
                    borderColor: [
                        'dark'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>

    @stop