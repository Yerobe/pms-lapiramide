@extends('layouts.master')

@section('content')




<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">



            <a class="btn btn-outline-dark rounded-0 text-left" data-toggle="modal" data-target="#ModalFilterCharts">
                <i class="fas fa-sort mr-2"></i>
                <span>Show Filter</span>
            </a>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 text-center mt-3 mb-3 row justify-content-center">




            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #6F42C1; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-paste fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total CheckIn</p>
                            <h4>{{$totalCheckIn}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FD7E14; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-paste fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total CheckOut</p>
                            <h4>{{$totalCheckOut}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-divide fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">CheckIn Average</p>
                            <h4>{{$averageCheckIn}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-divide fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">CheckOut Average</p>
                            <h4>{{$averageCheckOut}}</h4>
                        </div>
                    </div>

                </div>
            </div>

            @if($_SERVER['REQUEST_METHOD'] === 'GET')

            <div class="col col-11 pl-5 pr-5 bg-white shadow">
                <p class="p-2 m-1 font-weight-bold">You must previously specify the required dates. The week is currently displayed</p>
            </div>

            @endif


            <div class="col col-11 mt-5 bg-white shadow">
                <div>
                    <canvas id="myChart"></canvas>
                </div>
            </div>





        </div>



    </div>


    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php foreach ($arrayCheckIn as $calculo) {

                                if ($calculo === end($arrayCheckIn)) {
                                    echo ("'" . date("d-m-Y", strtotime($calculo['Day'])) . "'");
                                } else {
                                    echo ("'" . date("d-m-Y", strtotime($calculo['Day'])) . "'" . ",");
                                }
                            } ?>],
                datasets: [{
                    label: 'Check-In',
                    data: [<?php foreach ($arrayCheckIn as $calculo) {

                                if ($calculo === end($arrayCheckIn)) {
                                    echo ("'" . $calculo['Number'] . "'");
                                } else {
                                    echo ("'" . $calculo['Number'] . "'" . ",");
                                }
                            } ?>],
                    backgroundColor: [
                        'green'
                    ],
                    borderColor: [
                        'green'
                    ],
                    borderWidth: 1
                }, {
                    label: 'Check-OUT',
                    data: [<?php foreach ($arrayCheckOut as $calculo) {

                                if ($calculo === end($arrayCheckOut)) {
                                    echo ("'" . $calculo['Number'] . "'");
                                } else {
                                    echo ("'" . $calculo['Number'] . "'" . ",");
                                }
                            } ?>],
                    backgroundColor: [
                        'red'
                    ],
                    borderColor: [
                        'red'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>

    @include('utils.modals.charts.modals_charts')

    @stop