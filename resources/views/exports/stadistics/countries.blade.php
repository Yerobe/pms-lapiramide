
@if($contador != 0)
<table style="padding: 20px; border: 1px solid black; width: 100%;">
    <thead>
        <tr>
            <th style=" border: 1px solid black;">Date</th>
            @foreach($paises as $pais)
            <th colspan="3" style="border: 1px solid black;">{{ $pais[0]->Nombre }}</th>
            @endforeach
        </tr>

       
        <tr>
        <th style="border: 1px solid black;" colspan="1"></th>
        @foreach($paises as $pais)
        
        <th style="border: 1px solid black;" colspan="1">Entradas</th>
        <th style="border: 1px solid black;" colspan="1">Salidas</th>
        <th style="border: 1px solid black;" colspan="1">Pernoctaciones</th>
        @endforeach
        </tr>

    </thead>
    <tbody>
        <?php $i = 0; ?>

        @foreach($dates as $date)
        <tr style="text-align: center;">



            <td style="border: 1px solid black; background-color: #F38B8B;"><?php echo (date("d-m-Y", strtotime($date))); ?></td>

        
            <?php 
            
            $e = 0;

            for ($j = 0; $j < count($paises); $j++) { ?>
                <td style="border: 1px solid black;"><?php if($paises[$e][0]['Fechas'][$i]['Entradas'] == 0){echo('-');}else{echo($paises[$e][0]['Fechas'][$i]['Entradas']);} ?></td>
                <td style="border: 1px solid black;"><?php if($paises[$e][0]['Fechas'][$i]['Salidas'] == 0){echo('-');}else{echo($paises[$e][0]['Fechas'][$i]['Salidas']);} ?></td>
                <td style="border: 1px solid black;"><?php if($paises[$e][0]['Fechas'][$i]['Pernoctaciones'] == 0){echo('-');}else{echo($paises[$e][0]['Fechas'][$i]['Pernoctaciones']);} ?></td>
            <?php
            
                $e++;
            
        } 
        
        
        ?>

            <?php $i++; ?>
        </tr>
        @endforeach

    </tbody>
</table>
@else
<h1 style="color: red;">Error - No reservations found</h1>
<p class="text-bold">No reservations have been found with the stipulated dates</p>
<p>The error may be due to Country / Community / Canary Island = null, or there are no reservations</p>
<p>If the Error persists, consult Jerobel Jose Marrero Moreno - Administrator</p>
@endif
