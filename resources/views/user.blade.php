@extends('layouts.master')

@section('content')



<?php

if (count($errors) != 0) {




    foreach ($errors->all() as $error) {


        switch ($error) {
            case 'Name':
                $mensajeName = "border border-danger";
                break;

            case 'Surname':
                $mensajeSurname = "border border-danger";
                break;

            case 'NIF':
                $mensajeNif = "border border-danger";
                break;

            case 'Email':
                $mensajeEmail = "border border-danger";
                break;

            case 'Rol':
                $mensajeRol = "border border-danger";
                break;

            case 'Estado':
                $mensajeEstado = "border border-danger";
                break;

            case 'Password':
                $mensajePassword = "border border-danger";
                break;
        }
    }
}




?>



<div id="overlay" class="overlay"></div>
<div class="container-fluid mt-2">
    <div class="row">


        <div class="form-group col-md-12">




            <button type="button" class="btn btn-outline-success rounded-0 text-left" data-toggle="modal" data-target="#newUserModal">
                <i class="fa fa-plus mr-2"></i>
                <span>New User</span>
            </button>

            <button type="button" class="btn btn-outline-danger rounded-0 text-left ml-3" data-toggle="modal" data-target="#deleteUserModal">
                <i class="fa fa-minus mr-2"></i>
                <span>Delete User</span>
            </button>


            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 float-right mr-3 ml-3" href="#">
                <i class="fa fa-times"></i>
            </a>


            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0 float-right" href="#">
                <i class="fa fa-bars"></i>
            </a>

        </div>
    </div>




    <div style="background-color: #EFF3FB; min-height: 90vh;" class="row">



        <div class="col col-12 mt-3 mb-3 justify-content-center">



            <div class="col col-12">
                <div class="row p-3 d-flex justify-content-around">
                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #5A6268; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Total Users</p>
                            <h4>{{count($usuariosActivados) + count($usuariosDesactivados)}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #28A745; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Active Users</p>
                            <h4>{{count($usuariosActivados)}}</h4>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #EB393F; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Disabled Users</p>
                            <h4>{{count($usuariosDesactivados)}}</h4>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-5 col-md-5 col-sm-12 col-12 bg-white rounded p-3 mb-3 shadow ">
                        <div style="background-color: #FFAF58; width: 50%;" class="icono text-center p-3 rounded m-auto">
                            <i style="color: white;" class="fas fa-user fa-2x"></i>
                        </div>

                        <div class="mt-3 text-center">
                            <p class="font-weight-bold">Admin Users</p>
                            <h4>{{count($usuariosAdministradores)}}</h4>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">


                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    Enabled Users
                                </th>
                            </tr>
                        </thead>


                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NIF</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Rol</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(count($usuariosActivados) != 0)


                            @foreach ($usuariosActivados as $usuario)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $usuario->NIF }}</td>
                                <td class="align-middle">{{ $usuario->name }}</td>
                                <td class="align-middle">{{ $usuario->surname }}</td>
                                <td class="align-middle">{{ $usuario->email }}</td>
                                <td class="align-middle">{{ $usuario->Rol }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/users/{{$usuario->id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>



                        @if(count($usuariosActivados) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently there is no record with the specified search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>

            </div>


            <div class="col col-12 pl-5 pr-5 mt-4">
                <div class="table-responsive">
                    <table class="table rounded shadow">


                        <thead class="table-light">
                            <tr>
                                <th style="color: #5B626B;" colspan="12">
                                    Disabled Users
                                </th>
                            </tr>
                        </thead>


                        <thead class="table-light">
                            <tr class="text-center">
                                <th>NIF</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Rol</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody class="table-light">



                            @if(count($usuariosDesactivados) != 0)


                            @foreach ($usuariosDesactivados as $usuario)



                            <tr class="text-center h-100">
                                <td class="align-middle">{{ $usuario->NIF }}</td>
                                <td class="align-middle">{{ $usuario->name }}</td>
                                <td class="align-middle">{{ $usuario->surname }}</td>
                                <td class="align-middle">{{ $usuario->email }}</td>
                                <td class="align-middle">{{ $usuario->Rol }}</td>
                                <td class="align-middle"><a class="text-dark text-decoration-none" href="/users/{{$usuario->id}}"><i class="fas fa-pen-square fa-2x"></i></a></td>

                            </tr>

                            @endforeach
                            @endif



                        </tbody>



                        @if(count($usuariosDesactivados) == 0)
                        <tbody class="table-light">
                            <tr class="text-center">
                                <td class="align-middle" colspan="12">
                                    Currently there is no record with the specified search
                                </td>
                            </tr>
                        </tbody>
                        @endif





                    </table>
                </div>





            </div>





        </div>



    </div>



@include('utils.modals.user.modals_user')









    @stop