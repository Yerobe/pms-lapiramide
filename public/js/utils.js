window.onload = function () {

    tinymce.init({
        selector: 'textarea'
    });

    if (typeof document.getElementById("ComunidadAutonoma") != "undefined") {

        countryselected = document.getElementById("country").value;

        if (countryselected == 1) {
            document.getElementById("ComunidadAutonoma").style.display = "block";
        } else {
            document.getElementById("ComunidadAutonoma").style.display = "none";
        }

    }


    if (typeof document.getElementById("IslasCanarias") != "undefined") {

        islandSelected = document.getElementById("community").value;

        if (islandSelected == 5) {
            document.getElementById("IslasCanarias").style.display = "block";
        } else {
            document.getElementById("IslasCanarias").style.display = "none";
        }

    }

}


function checkCountry() {

    countrySelected = document.getElementById("country").value;

    if (countrySelected == 1) {
        document.getElementById("ComunidadAutonoma").style.display = "block";
    } else {
        document.getElementById("ComunidadAutonoma").style.display = "none";
        document.getElementById("community").value = "null";
        document.getElementById("IslasCanarias").style.display = "none";
        document.getElementById("canaryIsland").value = "null";
    }

}

function myCheckIsland() {

    communitySelected = document.getElementById("community").value;

    if (communitySelected == 5) {
        document.getElementById("IslasCanarias").style.display = "block";
    } else {
        document.getElementById("IslasCanarias").style.display = "none";
        document.getElementById("canaryIsland").value = "null";
    }

}


function checkDates() {



    var entrada = document.getElementById("fecha_entrada"); // Introduzo en String de la fecha


    var annios = entrada.value.slice(0, 4); // Obtengo el AÑO del Check-IN del String
    var mes = entrada.value.slice(5, 7); // Obtengo el MES del Check-IN del String
    var dia = entrada.value.slice(8, 10); // Obtengo el DIA del Check-IN del String

    ensamble = mes + "-" + dia + "-" + annios; // Adaptamos la fecha en formato requerido
    fecha = new Date(ensamble); // Creamos un valor fecha con los datos sustraido


    fecha.setDate(fecha.getDate() + 1); // Suma 1 dia a la fecha recogida
    var dia_nuevo = ("0" + fecha.getDate()).slice(-2); // Obtengo el día con dos cifras - 1 == 01
    var mes_nuevo = ("0" + (fecha.getMonth() + 1)).slice(-2); // Obtengo el mes con dos cifras - 5 == 05
    var anio_nuevo = fecha.getFullYear(); // Obtenemos el año con 4 cifras

    var input = document.getElementById("fecha_salida");
    input.min = anio_nuevo + "-" + mes_nuevo + "-" + dia_nuevo;


}





function changeURL(paginaActual) { // Me permite que al abrir una Modal y cerrarla, al realizar cambio de pagina, no se quede $id en la URL
    location.href = "/rooms?page=" + paginaActual;

}






// function myFunction() {


//     window.alert("hola");


//     var entrada = document.getElementById("fecha_entrada"); // Introduzo en String de la fecha


//     var annios = entrada.value.slice(0, 4); // Obtengo el AÑO del Check-IN del String
//     var mes = entrada.value.slice(5, 7); // Obtengo el MES del Check-IN del String
//     var dia = entrada.value.slice(8, 10); // Obtengo el DIA del Check-IN del String

//     ensamble = mes + "-" + dia + "-" + annios; // Adaptamos la fecha en formato requerido
//     fecha = new Date(ensamble); // Creamos un valor fecha con los datos sustraido


//     fecha.setDate(fecha.getDate() + 1); // Suma 1 dia a la fecha recogida
//     var dia_nuevo = ("0" + fecha.getDate()).slice(-2); // Obtengo el día con dos cifras - 1 == 01
//     var mes_nuevo = ("0" + (fecha.getMonth() + 1)).slice(-2); // Obtengo el mes con dos cifras - 5 == 05
//     var anio_nuevo = fecha.getFullYear(); // Obtenemos el año con 4 cifras

//     var input = document.getElementById("fecha_salida");
//     input.min = anio_nuevo + "-" + mes_nuevo + "-" + dia_nuevo;


// }