<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReserveController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\TodayController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\SeekerController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\OperatorController;
use App\Http\Controllers\TravelAgentController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\StadisticController;
use App\Http\Controllers\Utils\PriceCalculatorController;

use App\Http\Controllers\DownloadFileController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();



Route::get('/crearusuario', function(){ // Eliminar al Terminar el Proyecto

    $user = new App\Models\User;
    $user->NIF = '78821497Z';
    $user->name = 'Yerobe';
    $user->surname = 'Marrero Moreno';
    $user->email = 'jmamor2016@gmail.com';
    $user->password = Hash::make('12345');
    $user->Rol = 'Administrador';
    $user->Estado = 1;

    $user->save();

    redirect('/');

}); 


Route::group(['middleware' => 'web'], function () {
    

    // Home Page

    Route::get('/', [HomeController::class, 'getHome'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/home/{fecha?}', [HomeController::class, 'getHome'])->middleware('auth')->middleware(['password.confirm']);



    // Reservation Page

    Route::get('/create/reservation',[ReserveController::class,'getReserve'])->middleware(['password.confirm']);
    Route::post('/create/reservation',[ReserveController::class,'insertReserve'])->middleware(['password.confirm']);

    Route::get('/edit/reservation/{id}/{idMiembro?}', [ReserveController::class, 'getEdit'])->middleware(['password.confirm']);
    Route::put('/edit/reservation/{id}', [ReserveController::class, 'putEdit'])->middleware(['password.confirm']);

    Route::get('/edit/reservation/{id}/generate/voucher', [ReserveController::class, 'getVoucher'])->name('printVoucher')->middleware(['password.confirm']); // Crea el PDF de la reserva
    

    // Reservas - PDF DNI

    Route::post('/{id}/generate/dni', [ReserveController::class, 'savePDF'])->middleware(['password.confirm']); // Sube el PDF de los DNI
    
    Route::get('/view/document/pdf/{name}', [ReserveController::class, 'viewPDF'])->middleware(['password.confirm']); // Sube el PDF de los DNI
    
    // Reservas - Invoices 

    Route::post('/create/invoice/{id}', [ReserveController::class, 'createInvoice'])->middleware(['password.confirm']); // Recoge la informacion y crea la factura
    

    // Members 
    Route::post('/edit/reservation/{id}/create/member', [MemberController::class, 'createMember'])->middleware(['password.confirm']);
    Route::delete('/edit/reservation/{id}/{idMiembro}', [MemberController::class, 'deleteMember'])->middleware('auth')->middleware(['password.confirm']);

    // Clientes

    Route::get('/clients/{idCliente?}', [ClientController::class, 'getClients'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/clients/create', [ClientController::class, 'postClient'])->middleware('auth')->middleware(['password.confirm']);
    Route::put('/clients/update', [ClientController::class, 'putClient'])->middleware('auth')->middleware(['password.confirm']);

    

    // Booking Page

    Route::get('/booking', [BookingController::class, 'getBooking'])->middleware('auth')->middleware(['password.confirm']);

    // Today Page

    Route::get('/today', [TodayController::class, 'getToday'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/today', [TodayController::class, 'getToday'])->middleware('auth')->middleware(['password.confirm']);

    // Rooms Page

    Route::get('/rooms/{idHabitacion?}', [RoomController::class, 'getRoom'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/rooms', [RoomController::class, 'getRoom'])->middleware('auth')->middleware(['password.confirm']);
    Route::put('/rooms/{idHabitacion}', [RoomController::class, 'putEdit'])->middleware('auth')->middleware(['password.confirm']);

    // Seeker Page

    Route::get('/seeker', [SeekerController::class, 'getSeeker'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/seeker', [SeekerController::class, 'getSeeker'])->middleware('auth')->middleware(['password.confirm']);

    // Resurant Page

    Route::get('/restaurant', [RestaurantController::class, 'getRestaurant'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/restaurant', [RestaurantController::class, 'getRestaurant'])->middleware('auth')->middleware(['password.confirm']);

    // User Page

    Route::get('/users', [UserController::class, 'getUsers'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/users/create', [UserController::class, 'postCreate'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/users/delete', [UserController::class, 'postDelete'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/users/{idUsuario}', [UserController::class, 'getUsers'])->middleware('auth')->middleware(['password.confirm']);
    Route::put('/users/{idUsuario}', [UserController::class, 'putEdit'])->middleware('auth')->middleware(['password.confirm']);


    // Proveedores Page

    Route::get('/provider', [ProviderController::class, 'getProviders'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/provider/information/{idProveedor}/{opcion?}/{datoProveedor?}', [ProviderController::class, 'getProvider'])->middleware('auth')->middleware(['password.confirm']); // Esta
    Route::post('/provider', [ProviderController::class, 'postCreate'])->middleware('auth')->middleware(['password.confirm']);
    Route::put('/provider/information/{idProveedor}', [ProviderController::class, 'putEdit'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/provider/information/create/phone/{idProveedor}', [ProviderController::class, 'postCreatePhone'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/provider/information/create/email/{idProveedor}', [ProviderController::class, 'postCreateEmail'])->middleware('auth')->middleware(['password.confirm']);
    Route::delete('/provider/information/delete/phone/{idProveedor}/{telefono}', [ProviderController::class, 'postDeletePhone'])->middleware('auth')->middleware(['password.confirm']);
    Route::delete('/provider/information/delete/email/{idProveedor}/{email}', [ProviderController::class, 'postDeleteEmail'])->middleware('auth')->middleware(['password.confirm']);


    // Operators Page

    Route::get('/operator', [OperatorController::class, 'getOperators'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/operator/information/{idOperator}/{opcion?}/{datoOperator?}', [OperatorController::class, 'getOperator'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/operator', [OperatorController::class, 'postCreate'])->middleware('auth')->middleware(['password.confirm']);
    Route::put('/operator/information/{idOperator}', [OperatorController::class, 'putEdit'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/operator/information/create/phone/{idOperator}', [OperatorController::class, 'postCreatePhone'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/operator/information/create/email/{idOperator}', [OperatorController::class, 'postCreateEmail'])->middleware('auth')->middleware(['password.confirm']);
    Route::delete('/operator/information/delete/phone/{idOperator}/{telefono}', [OperatorController::class, 'postDeletePhone'])->middleware('auth')->middleware(['password.confirm']);
    Route::delete('/operator/information/delete/email/{idOperator}/{email}', [OperatorController::class, 'postDeleteEmail'])->middleware('auth')->middleware(['password.confirm']);


    // Travel Agent Page

    Route::get('/travelagent',[TravelAgentController::class,'getTravelAgents'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/travelagent',[TravelAgentController::class,'postTravelAgents'])->middleware('auth')->middleware(['password.confirm']);

    // Clientes Page

    Route::get('/clients', [ClientController::class, 'getClients'])->middleware('auth')->middleware(['password.confirm']);

    // Charts

    Route::get('/charts/occupation', [ChartController::class, 'getOccupation'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/charts/occupation', [ChartController::class, 'getOccupation'])->middleware('auth')->middleware(['password.confirm']);

    Route::get('/charts/checkin-out', [ChartController::class, 'getCheckInOut'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/charts/checkin-out', [ChartController::class, 'getCheckInOut'])->middleware('auth')->middleware(['password.confirm']);

    Route::get('/charts/bookings', [ChartController::class, 'getBookings'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/charts/bookings', [ChartController::class, 'getBookings'])->middleware('auth')->middleware(['password.confirm']);
    

    // Services

    Route::get('/stadistics', [StadisticController::class, 'getStadistics'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/stadistics', [StadisticController::class, 'getStadistics'])->middleware('auth')->middleware(['password.confirm']);


    Route::get('/stadistics/countrys/{firstDay}/{lastDay}', [StadisticController::class, 'getCountriesStadistics'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/stadistics/communities/{firstDay}/{lastDay}', [StadisticController::class, 'getCommunitiesStadistics'])->middleware('auth')->middleware(['password.confirm']);
    Route::get('/stadistics/canaryisland/{firstDay}/{lastDay}', [StadisticController::class, 'getCanaryIslandStadistics'])->middleware('auth')->middleware(['password.confirm']);
   
    // Route::get('/stadistics/ineExport', [StadisticController::class, 'exportINE'])->middleware('auth')->middleware(['password.confirm']);


    // Price Calculator

    Route::get('/calculator', [PriceCalculatorController::class, 'getCalcPrice'])->middleware('auth')->middleware(['password.confirm']);
    Route::post('/calculator', [PriceCalculatorController::class, 'postCalcPrice'])->middleware('auth')->middleware(['password.confirm']);
    

    // Page for download the document on relation to APP Documentation
    
    Route::get('/dowloand-document/{nombre}', [DownloadFileController::class, 'index'])->middleware('auth')->middleware(['password.confirm'])->name('file.download.index');

    
 

    
});
