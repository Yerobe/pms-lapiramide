<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

use App\Models\Reserva;
use Illuminate\Http\Client\Request;

class StadisticsExport implements FromView
{

    public function view(): View
    {

        return view('exports.stadistics.ine', [
            'reservas' => Reserva::all()
        ]);
    }

}
