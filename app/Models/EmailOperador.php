<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailOperador extends Model
{
    use HasFactory;

    protected $table = "email_operadores";// <-- El nombre personalizado
}
