<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IslasCanaria extends Model
{
    use HasFactory;

    protected $table = "islascanarias";// <-- El nombre personalizado
}
