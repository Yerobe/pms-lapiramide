<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MiembroReserva extends Model
{
    use HasFactory;

    protected $table = "reservas_has_miembros";// <-- El nombre personalizado
}
