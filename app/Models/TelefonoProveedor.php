<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelefonoProveedor extends Model
{
    use HasFactory;

    protected $table = "telefono_proveedores";// <-- El nombre personalizado
}
