<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailProveedor extends Model
{
    use HasFactory;

    protected $table = "email_proveedores";// <-- El nombre personalizado

}
