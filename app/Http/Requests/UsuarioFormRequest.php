<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UsuarioFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'nif' => 'required|max:25',
            'email' => 'required|email:rfc,dns',
            'rol' => 'required',
            'estado' => 'required|digits_between:0,1',
            'password' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'Name',

            'surname.required' => 'Surname',

            'nif.required' => 'NIF',
            'nif.max' => 'NIF',

            'email.required' => 'Email',
            'email.email' => 'Email',

            'rol.required' => 'Rol',

            'estado.required' => 'Estado',
            'estado.digits_between' => 'Estado',

            'password.required' => 'Password',


        ];
    }
}
