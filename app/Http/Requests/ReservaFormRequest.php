<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ReservaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {


     


        return [
            'numero_reserva' => 'required',
            'book_day' => 'required|date|before_or_equal:' . $request->input('fecha_entrada'),
            'fecha_entrada' => 'required|date|before:' . $request->input('fecha_salida'),
            'fecha_salida' => 'required|date|after:' . $request->input('fecha_entrada'),
            'adults' => 'required|min:1',
            'children' => 'required|min:0',
            'pets' => 'required|min:0',
            'name' => 'required',
            'surname' => 'required',
            'paymentmade' => 'required|numeric',
            'totalprice' => 'required|numeric',
            'regimen' => 'required',
            'travelagent' => 'required',
            'room' => 'required',
            'idPais' => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'numero_reserva.required' => 'NumeroReserva',

            'book_day.required' => 'BookDay',
            'book_day.date' => 'BookDay',
            'book_day.before_or_equal' => 'BookDay',

            'fecha_Entrada.required' => 'FechaEntrada',
            'fecha_Entrada.date' => 'FechaEntrada',
            'fecha_Entrada.before' => 'FechaEntrada',

            'fecha_salida.required' => 'FechaSalida',
            'fecha_salida.date' => 'FechaSalida',
            'fecha_salida.after' => 'FechaSalida',

            'adults.required' => 'Adultos',
            'adults.min' => 'Adultos',

            'children.required' => 'Ninos',
            'children.min' => 'Ninos',

            'pets.required' => 'Mascotas',
            'pets.min' => 'Mascotas',

            'name.required' => 'Nombre',

            'surname.required' => 'Apellidos',

            'paymentmade.required' => 'Prepago',
            'paymentmade.numeric' => 'Prepago',

            'totalprice.required' => 'PrecioTotal',
            'totalprice.numeric' => 'PrecioTotal',

            'regimen.required' => 'Regimen',

            'travelagent.required' => 'Operador',

            'room.required' => 'Habitacion',

            'idPais.numeric' => 'Pais',

        ];
    }
}
