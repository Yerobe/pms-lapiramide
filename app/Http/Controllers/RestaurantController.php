<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Reserva;

class RestaurantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRestaurant(Request $request)
    {
        if (Auth::check()) {



            if ($request->method() == "POST") {
                $fechaSeleccionadaInicial = $request->input('fechaSeleccionadaInicial'); // Recogemos el Valor de Fecha Inicial
                $fechaSeleccionadaFinal = $request->input('fechaSeleccionadaFinal'); // Recogemos el Valor de Fecha Final

                $valorFechaSeleccionadaInicial = $request->input('fechaSeleccionadaInicial');
                $valorFechaSeleccionadaFinal = $request->input('fechaSeleccionadaFinal');
            } else {
                $fechaSeleccionadaInicial = date("Y-m-d");
                $fechaSeleccionadaFinal = date("Y-m-d");
            }



            $informacionRestaurante = array();
            $informacionSuperior = array( // Contador para la informacion Superior
                "DiasCalculados" => 0,
                "Desayunos" => 0,
                "Almuerzos" => 0,
                "Cenas" => 0,
            );

            for ($i = 0; date($fechaSeleccionadaInicial) <= date($fechaSeleccionadaFinal); $fechaSeleccionadaInicial = date("Y-m-d", strtotime($fechaSeleccionadaInicial . "+ 1 days"))) {
                $date = date_create($fechaSeleccionadaInicial);
                $informacionRestaurante[$i]["Fecha"] = date_format($date, "d/m/Y");
                $informacionRestaurante[$i]["Habitaciones"] = DB::table('reservas')
                    ->where('FechaEntrada', '<=', $fechaSeleccionadaInicial)
                    ->where('FechaSalida', '>=', $fechaSeleccionadaInicial)
                    ->where('Estado', 1)
                    ->count();
                $informacionRestaurante[$i]["Adultos"] = DB::select("SELECT SUM(NumeroAdultos) AS Adultos FROM reservas WHERE Estado = 1 AND FechaEntrada <= '" . $fechaSeleccionadaInicial . "' AND FechaSalida >= '" . $fechaSeleccionadaInicial . "' "); // Obtenemos los Adultos
                $informacionRestaurante[$i]["Ninos"] = DB::select("SELECT SUM(NumeroNinios) AS Ninos FROM reservas WHERE Estado = 1 AND FechaEntrada <= '" . $fechaSeleccionadaInicial . "' AND FechaSalida >= '" . $fechaSeleccionadaInicial . "' "); // Obtenemos los Niños
                $informacionRestaurante[$i]["Desayunos"] = DB::select("SELECT SUM(NumeroAdultos)+SUM(NumeroNinios) AS Desayunos FROM reservas WHERE Estado = 1 AND (Regimen = 'Breakfast' OR Regimen = 'Half Board' OR Regimen = 'Full Board') AND  FechaEntrada < '" . $fechaSeleccionadaInicial . "' AND FechaSalida >= '" . $fechaSeleccionadaInicial . "'"); // Obtenemos los Desayunos
                $informacionRestaurante[$i]["Almuerzos"] = DB::select("SELECT SUM(NumeroAdultos)+SUM(NumeroNinios) AS Almuerzos FROM reservas WHERE Estado = 1 AND Regimen = 'Full Board' AND  FechaEntrada < '" . $fechaSeleccionadaInicial . "' AND FechaSalida > '" . $fechaSeleccionadaInicial . "'"); // Obtenemos los Almuerzos
                $informacionRestaurante[$i]["Cenas"] = DB::select("SELECT SUM(NumeroAdultos)+SUM(NumeroNinios) AS Cenas FROM reservas WHERE Estado = 1 AND (Regimen = 'Half Board' OR Regimen = 'Full Board') AND  FechaEntrada <= '" . $fechaSeleccionadaInicial . "' AND FechaSalida > '" . $fechaSeleccionadaInicial . "'"); // Obtenemos las Cenas

                $i++;
            }


            foreach ($informacionRestaurante as $diaIndividual) { // Calculamos la Media de la Informacion Superior, como Estadística

                $informacionSuperior["DiasCalculados"] = count($informacionRestaurante);
                $informacionSuperior["Desayunos"] =  round(($informacionSuperior["Desayunos"] + $diaIndividual['Desayunos'][0]->Desayunos ?? 0) / count($informacionRestaurante));


                $informacionSuperior["Almuerzos"] =  round(($informacionSuperior["Almuerzos"] + $diaIndividual['Almuerzos'][0]->Almuerzos ?? 0) / count($informacionRestaurante));
                $informacionSuperior["Cenas"] =  round(($informacionSuperior["Cenas"] + $diaIndividual['Cenas'][0]->Cenas ?? 0) / count($informacionRestaurante));
            }


            return view('restaurant')->with(
                array(
                    'informacionRestaurante' => $informacionRestaurante,
                    'valorFechaSeleccionadaInicial' => $valorFechaSeleccionadaInicial ?? date("Y-m-d"),
                    'valorFechaSeleccionadaFinal' => $valorFechaSeleccionadaFinal ?? date("Y-m-d"),
                    'informacionSuperior' => $informacionSuperior
                )
            );;
        }
    }
}
