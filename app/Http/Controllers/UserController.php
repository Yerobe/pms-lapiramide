<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\UsuarioFormRequest;

use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUsers(Request $request, $idUsuario = NULL)
    {
        if (Auth::check()) {

            if (auth()->user()->Rol == "Administrador") {

                if ($idUsuario != NULL) {
                    $ventanaModal = TRUE;
                    $usuarioEdit = User::find($idUsuario);
                } else {
                    $ventanaModal = FALSE;
                    $usuarioEdit = NULL;
                }

                $usuariosActivados = User::where('Estado', 1)->get();
                $usuariosDesactivados = User::where('Estado', 0)->get();

                $usuariosAdministradores = User::where('Rol', 'Administrador')
                    ->where('Estado', 0)
                    ->orWhere('Estado', 1)->get();


                return view('user')->with(
                    array(
                        'usuariosActivados' => $usuariosActivados,
                        'usuariosDesactivados' => $usuariosDesactivados,
                        'usuariosAdministradores' => $usuariosAdministradores,
                        'ventanaModal' => $ventanaModal,
                        'usuarioEdit' => $usuarioEdit
                    )
                );;
            } else {
                return redirect('/');
            }
        }
    }


    public function postCreate(UsuarioFormRequest $request)
    {
        if (Auth::check()) {

            if (auth()->user()->Rol == "Administrador") {


                $user = User::where("NIF", $request->input('nif'))
                    ->orWhere("Email", $request->input('email'))
                    ->get();

                if (count($user) != 0) {
                    
                    User::where("NIF", $request->input('nif'))
                    ->orWhere("Email", $request->input('email'))
                    ->update(['Estado'=> 0]);

                    Session::flash('UserUpdate', 'A user has been found with the DNI / Email already registered previously, what has been done is to change its status to Disabled. The previous information that this same contained has been maintained.');
                    return redirect('/users');

                } else {

                    $newUser = array(
                        'NIF' => strtoupper($request->input('nif')),
                        'name' => ucwords(strtolower($request->input('name'))),
                        'surname' => ucwords(strtolower($request->input('surname'))),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'Rol' => $request->input('rol'),
                        'Estado' => $request->input('estado'),
                    );


                    DB::table('users')->insert($newUser);

                    Session::flash('InsertUser', 'The User has been created successfully');
                    return redirect('/users');
                }
            }
        }
    }

    public function putEdit(Request $request, $idUsuario)
    {
        if (Auth::check()) {

            if (auth()->user()->Rol == "Administrador") {






                DB::table('users')
                    ->where('Id', $idUsuario)
                    ->update([
                        'NIF' => $request->input('nif'),
                        'name' => $request->input('name'),
                        'surname' => $request->input('surname'),
                        'email' => $request->input('email'),
                        'Rol' => $request->input('rol'),
                        'Estado' => $request->input('estado'),

                    ]);


                Session::flash('UserUpdate', 'The User has been update successfully');
                return redirect('/users');
            }
        }
    }





    public function postDelete(Request $request)
    {
        if (Auth::check()) {

            if (auth()->user()->Rol == "Administrador") {




                $user = User::where("NIF", "=", $request->input('nif'))->get();

                if (count($user) == 0) { // No Existe el Usuario con el DNI establecido
                    Session::flash('DeleteUserError', 'The established identification number is not associated with any user. If the error persists, contact the administrator');
                    return redirect('/users');
                } else {

                    DB::table('users')
                        ->where('NIF', $request->input('nif'))
                        ->update([
                            'Estado' => NULL, // Le cambiamos el Estado, que interpretamos como "Eliminado", ya que mantenemos Restrict en Action FK
                        ]);

                    Session::flash('DeleteUserCorrect', 'User has been successfully removed');
                    return redirect('/users');
                }
            }
        }
    }
}
