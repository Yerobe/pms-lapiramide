<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\MemberFormRequest;
use App\Models\Miembro;
use App\Models\MiembroReserva;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function deleteMember($id, $idMember)
    {


        $findMember = MiembroReserva::all()->where('IdMiembro',$idMember);

        if(count($findMember) == 1){ // Eliminamos el Cliente por Completo de todas las tablas, ya que este solo existe en esta reserva

            MiembroReserva::where('IdMiembro',$idMember)
            ->where('IdReserva',$id)
            ->delete();

            Miembro::where('Id',$idMember)
            ->delete();

            Session::flash('DeleteMember', 'The client has been completely removed successfully');

            
        }else{ // Eliminamos el Cliente, únicamente en la tabla de asociación con la reserva.
            MiembroReserva::where('IdMiembro',$idMember)
            ->where('IdReserva',$id)
            ->delete();

            Session::flash('DeleteMember', 'The client has been partially eliminated, since he is registered in another reservation.');

        }

        return redirect('/edit/reservation/' . $id);

    }


    public function createMember(MemberFormRequest $request, $id)
    {

        if (Auth::check()) {


            $members = Miembro::where('NIF', $request->input('nif'))->get();


            if (count($members) == 0) { // En caso de que no exista Miembro, relacionado con el Identificador introducido


                $newMember = new Miembro;
                $newMember->NIF = strtoupper($request->input('nif'));
                $newMember->Nombre =  ucwords(strtolower($request->input('name')));
                $newMember->Apellidos =  ucwords(strtolower($request->input('surname')));

                $newMember->save();

                $newMemberWithReservation = array(
                    'IdReserva' => $id,
                    'IdMiembro' => $newMember->id,
                );

                DB::table('reservas_has_miembros')->insert($newMemberWithReservation);


                Session::flash('InsertMember', 'The Member has been created successfully');
            } else {


                $memberHasReservation = MiembroReserva::where('IdReserva', $id)->where('IdMiembro', $members[0]->Id)->get();

                if (count($memberHasReservation) != 0) { // El usuario intenta introducir un Miembro ya existente en la Rerserva
                    Session::flash('InsertMember', 'The member who has been inserted was already registered in this reservation. In case of error, contact the administrator');
                } else {
                    $newMemberWithReservation = array(
                        'IdReserva' => $id,
                        'IdMiembro' => $members[0]->Id,
                    );

                    DB::table('reservas_has_miembros')->insert($newMemberWithReservation);

                    Session::flash('InsertMember', 'The member was already created, we have associated it with this reservation');
                }
            }

            return redirect('/edit/reservation/' . $id);
        }
    }

    public function postDeleteMember($idReserva, $idMiembro)
    {
        if (Auth::check()) {

            // MiembroReserva::where('IdReserva',$idReserva)
            // ->where('IdMiembro',$idMiembro)
            // ->delete();

            // Session::flash('DeleteMember', 'The mobile phone has been delete successfully');
            // return redirect('/edit/reservation/'.$idReserva);

        }
    }
}
