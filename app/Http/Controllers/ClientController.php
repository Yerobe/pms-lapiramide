<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Models\Cliente;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getClients($idCliente = null)
    {

        if (Auth::check()) {

            if($idCliente != null){
                $cliente = Cliente::where('Id',$idCliente)->get();
            }else{
                $cliente = null;
            }

            $totalClients = Cliente::all()->count();

            $clients = Cliente::orderBy('Id', 'desc')
                ->take(10)
                ->get();


            return view('client')->with(
                array(
                    'clientes' => $clients,
                    'totalClients' => $totalClients,
                    'clienteUpdate' => $cliente
                )
            );;
        }
    }

    public function postClient(Request $request)
    {

        $newClient = array(
            'Nombre' => $request->input('name'),
            'CIF' => $request->input('cif'),
            'Direccion' => $request->input('direccion'),
            'CP' => $request->input('cp')
        );


        DB::table('clientes')->insert($newClient);

        Session::flash('ClientModalNotice', 'The Client has been created successfully');

        return redirect('/clients');
    }

    public function putClient(Request $request){
        DB::table('clientes')
                ->where('Id', $request->input('id'))
                ->update([
                    'Nombre' => $request->input('name'),
                    'CIF' => $request->input('cif'),
                    'Direccion' => $request->input('direccion'),
                    'CP' => $request->input('cp'),
                ]);

                Session::flash('ClientModalNotice', 'The Client has been updated successfully');
                return redirect('/clients');
    }
}
