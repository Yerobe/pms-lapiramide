<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Operador;
use App\Http\Requests\TravelAgentFormRequest;
use App\Models\Reserva;
use App\Models\Habitacion;

class TravelAgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTravelAgents()
    {
        if (Auth::check()) {

            $operadores = Operador::all();

            $reservas = array();
            $condicionesActivadas = FALSE;


            $valorFechaEntrada = null;
            $valorFechaSalida = null;
            $valorOperador = null;



            return view('travelAgents')->with(
                array(
                    'operadores' => $operadores,
                    'reservas' => $reservas,
                    'condicionesActivadas' => $condicionesActivadas,
                    'valorFechaEntrada' => $valorFechaEntrada,
                    'valorFechaSalida' => $valorFechaSalida,
                    'valorOperador' => $valorOperador,
                    'valorEstado' => null
                )
            );;
        }
    }


    public function postTravelAgents(TravelAgentFormRequest $request)
    {
        if (Auth::check()) {

            // $request->input('fechaEntrada')

            $operadores = Operador::all();
            $valorEstado = $request->input('status');
            
            if($valorEstado == 'null'){
                $reservas = Reserva::all()
                ->where('FechaEntrada', '>=', $request->input('fechaEntrada'))
                ->where('FechaSalida', '<=', $request->input('fechaSalida'))
                ->where('IdOperador', $request->input('operador'));
            }else{
                $reservas = Reserva::all()
                ->where('FechaEntrada', '>=', $request->input('fechaEntrada'))
                ->where('FechaSalida', '<=', $request->input('fechaSalida'))
                ->where('IdOperador', $request->input('operador'))
                ->where('Estado',$valorEstado);
            }

          

            $contadorReservasActivadas = 0;
            $contadorReservasDesactivadas = 0;
            $contadorReservasPagadas = 0;

            $valorFechaEntrada = $request->input('fechaEntrada');
            $valorFechaSalida = $request->input('fechaSalida');
            $valorOperador = $request->input('operador');
            


            foreach ($reservas as $reserva) {

                if ($reserva->Precio_Total == $reserva->Prepago) {
                    $contadorReservasPagadas = $contadorReservasPagadas + 1;
                }

                if ($reserva->Estado == 1) {
                    $contadorReservasActivadas = $contadorReservasActivadas + 1;
                } else {
                    $contadorReservasDesactivadas = $contadorReservasDesactivadas + 1;
                }
                
            }


            

            foreach ($reservas as $reserva) { // Obtenemos el Numero de Habitacion asignado a la reserva

                if ($reserva->IdHabitacion == NULL) {
                    $reserva->NumeroHabitacion = "Not Asiggned";
                } else {
                    $room = Habitacion::where("Id", "=", $reserva->IdHabitacion)->select("NHabitacion")->first();
                    $reserva->NumeroHabitacion = $room['NHabitacion'];
                }
            }

            $condicionesActivadas = TRUE;



            return view('travelAgents')->with(
                array(
                    'operadores' => $operadores,
                    'reservas' => $reservas,
                    'condicionesActivadas' => $condicionesActivadas,
                    'contadorReservasActivadas' => $contadorReservasActivadas,
                    'contadorReservasDesactivadas' => $contadorReservasDesactivadas,
                    'contadorReservasPagadas' => $contadorReservasPagadas,
                    'valorFechaEntrada' => $valorFechaEntrada,
                    'valorFechaSalida' => $valorFechaSalida,
                    'valorOperador' => $valorOperador,
                    'valorEstado' => $valorEstado
                )
            );;
        }
    }
}
