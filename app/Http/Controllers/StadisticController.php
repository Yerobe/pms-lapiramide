<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Models\Pais;
use App\Models\Reserva;
use App\Models\Operador;
use App\Exports\StadisticsExport;
use App\Models\ComunidadAutonoma;
use App\Models\IslasCanaria;
use DateTime;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Arr;

class StadisticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getStadistics(Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $firstDay = $request->input('diaInicial');
            $lastDay = $request->input('diaFinal');
        } else {
            $firstDay = date("Y-m-01"); // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = date("Y-m-t");  // Obtenemos la Fecha del Domingo de esta semana
        }


        $fecha1 = new DateTime($firstDay); //Formateamos la Primera Fecha
        $fecha2 = new DateTime($lastDay); // Formateamos la Ultima fecha

        $diff = $fecha1->diff($fecha2); // Contamos la cantidad de dias totales entre ambas fechas

        $firstDayTwo = $firstDay; // resguardamos la variable, para asi no modificar $firstDay
        $idReserves = array();

        for ($i = 0; $i <= $diff->days; $i++) {

            $reservasTotales = Reserva::where('Estado', 1)
                ->where('FechaEntrada', '<=', $firstDayTwo)
                ->where('FechaSalida', '>=', $firstDayTwo)
                ->get();

            foreach ($reservasTotales as $reserva) {

                if (array_search($reserva->Id, $idReserves) === false) {
                    array_push($idReserves, $reserva->Id);
                }
            }

            $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango
        }

        $totalReservation = $idReserves;

        $reserves = array();

        foreach ($idReserves as $reserve) {
            $getReserve = Reserva::where('Id', $reserve)->get();
            array_push($reserves, $getReserve);
        }


        $idOperators = array();
        $halfPrice = 0;

        foreach ($reserves as $reserva) {

            if (array_search($reserva[0]->IdOperador, $idOperators) === false) {
                array_push($idOperators, $reserva[0]->IdOperador);
            }

            $halfPrice = $halfPrice + $reserva[0]->Precio_Total;
        }


        if ($halfPrice == 0) {
        } else {
            $halfPrice = round($halfPrice / count($totalReservation));
        }


        $operatorCount = array();
        $totalLaPiramide = null;

        for ($i = 0; $i < count($idOperators); $i++) { // Obtendremos el Mejor Operador


            $firstDayTwo = $firstDay; // resguardamos la variable, para asi no modificar $firstDay

            $countByOperator = array();

            for ($j = 0; $j <= $diff->days; $j++) {

                $reservationGetByDay = Reserva::where('FechaEntrada', '<=', $firstDayTwo)->where('FechaSalida', '>=', $firstDayTwo)->where('Estado', 1)->where('IdOperador', $idOperators[$i])->get(); // Obtenemos las reservas totales mediante su Id

                foreach ($reservationGetByDay as $reserve) {
                    if (array_search($reserve->Id, $countByOperator) === false) {
                        array_push($countByOperator, $reserve->Id);
                    }
                }
                $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango
            }

            $nameOperator = Operador::where('Id', $idOperators[$i])->get(); // Obtenemos el Nombre del Operador actual


            $operator = array("idOperador" => $idOperators[$i], "CantidadReservas" => count($countByOperator), "Nombre" => $nameOperator[0]["Nombre"]);

            if ($nameOperator[0]["Nombre"] == 'La Piramide') {
                $totalLaPiramide = count($countByOperator);
            }

            array_push($operatorCount, $operator);
        }



        $bestOperatorId = null;
        $bestOperatorCount = 0;

    

        foreach ($operatorCount as $oneOperator) {
            if ($bestOperatorId == null) {
                $bestOperatorId = $oneOperator["idOperador"];
                $bestOperatorCount = $oneOperator["CantidadReservas"];
            } else {
                if ($oneOperator["CantidadReservas"] > $bestOperatorCount) {
                    $bestOperatorId = $oneOperator["idOperador"];
                    $bestOperatorCount = $oneOperator["CantidadReservas"];
                }
            }
        }

       
        $getBestOperator = Operador::where('Id', $bestOperatorId)->get();

       


        return view('stadistic')->with(
            array(
                'firstDay' => $firstDay,
                'lastDay' => $lastDay,
                'totalReservation' => $totalReservation,
                'getBestOperator' => $getBestOperator,
                'totalLaPiramide' => $totalLaPiramide,
                'halfPrice' => $halfPrice,
                'operatorCount' => $operatorCount
            )
        );;
    }

    public function getCountriesStadistics($firstDay, $lastDay)
    {
        // if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        //     $firstDay = $request->input('diaInicial'); // Obtenemos la Primera Fecha Introducida por el Usuario
        //     $lastDay = $request->input('diaFinal'); // Obtenemos la Segunda Fecha Introducida por el Usuario
        // } else {
        //     $firstDay = date("Y-m-01"); // Obtenemos la Fecha del Primer dia del mes
        //     $lastDay = date("Y-m-t");  // Obtenemos la Fecha del Ultimo dia del mes
        // }

        $fecha1 = new DateTime($firstDay); //Formateamos la Primera Fecha
        $fecha2 = new DateTime($lastDay); // Formateamos la Ultima fecha

        $diff = $fecha1->diff($fecha2); // Contamos la cantidad de dias totales entre ambas fechas

        $firstDayTwo = $firstDay; // resguardamos la variable, para asi no modificar $firstDay

        $idPaises = array(); // registrara los ID de los Paises Comprometidos
        $dates = array(); // Registrara los valores de las fechas usadas

    

        for ($i = 0; $i <= $diff->days; $i++) { // Con este Bucle, obtendremos todos los Id de Todos los Paises comprometidos en las reservas de fechas seleccionadas

            $reservasTotales = Reserva::where('Estado', 1)
                ->where('FechaEntrada', '<=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('FechaSalida', '>=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('IdPais', '!=', null)
                ->get();

            foreach ($reservasTotales as $reserva) {

                if (array_search($reserva->IdPais, $idPaises) === false) {
                    array_push($idPaises, $reserva->IdPais);
                }
            }

            array_push($dates, $firstDayTwo); // Resguardamos todos los dias Registrados

            $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango
        }


        $firstDayTwo = $firstDay; // Recuperamos el Primer dia Seleccionado

        $paises = array(); // Creamos array donde resguardaremos todos los paises

        foreach ($idPaises as $pais) { // Identificamos las propiedades de todos los Paises comprometidos
            $getPais = Pais::where('Id', $pais)->get();
            array_push($paises, $getPais);
        }

       

        $contador = count($paises); // Cantidasd de Paises, asi ahorraremos el metodo count()


        for ($i = 0; $i < $contador; $i++) { // Recorremos los Paises para asi introducir sus datos

            $mes = array();

            for ($j = 0; $j <= $diff->days; $j++) { // Introducimos los datos a cada Pais por su Dia

                $getEntradas = StadisticController::calcPersonsByDayAndByCountry($firstDayTwo, $paises[$i][0]['Id'], 1);
                $getSalidas = StadisticController::calcPersonsByDayAndByCountry($firstDayTwo, $paises[$i][0]['Id'], 2);
                $getPernoctaciones = StadisticController::calcPersonsByDayAndByCountry($firstDayTwo, $paises[$i][0]['Id'], 3);

                $day = array('Dia' => $firstDayTwo, 'Entradas' => $getEntradas, 'Salidas' => $getSalidas, 'Pernoctaciones' => $getPernoctaciones);

                array_push($mes, $day);

                $paises[$i][0]['Fechas'] = $mes;

                $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango

            }

            $firstDayTwo = $firstDay;
        }

        

        return view('exports.stadistics.countries', [
            'paises' => $paises,
            'dates' => $dates,
            'contador' => $contador
        ]);
    }


    public function getCommunitiesStadistics($firstDay, $lastDay){
        
        $fecha1 = new DateTime($firstDay); //Formateamos la Primera Fecha
        $fecha2 = new DateTime($lastDay); // Formateamos la Ultima fecha

        $diff = $fecha1->diff($fecha2); // Contamos la cantidad de dias totales entre ambas fechas

        $firstDayTwo = $firstDay; // resguardamos la variable, para asi no modificar $firstDay

        $idPaises = array(); // registrara los ID de los Paises Comprometidos
        $dates = array(); // Registrara los valores de las fechas usadas

    

        for ($i = 0; $i <= $diff->days; $i++) { // Con este Bucle, obtendremos todos los Id de Todos los Paises comprometidos en las reservas de fechas seleccionadas

            $reservasTotales = Reserva::where('Estado', 1)
                ->where('FechaEntrada', '<=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('FechaSalida', '>=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('IdComunidadAutonoma', '!=', null)
                ->get();

            foreach ($reservasTotales as $reserva) {

                if (array_search($reserva->IdComunidadAutonoma, $idPaises) === false) {
                    array_push($idPaises, $reserva->IdComunidadAutonoma);
                }
            }

            array_push($dates, $firstDayTwo); // Resguardamos todos los dias Registrados

            $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango
        }


        $firstDayTwo = $firstDay; // Recuperamos el Primer dia Seleccionado

        $paises = array(); // Creamos array donde resguardaremos todos los paises

        foreach ($idPaises as $pais) { // Identificamos las propiedades de todos los Paises comprometidos
            $getPais = ComunidadAutonoma::where('Id', $pais)->get();
            array_push($paises, $getPais);
        }

       

        $contador = count($paises); // Cantidasd de Paises, asi ahorraremos el metodo count()


        for ($i = 0; $i < $contador; $i++) { // Recorremos los Paises para asi introducir sus datos

            $mes = array();

            for ($j = 0; $j <= $diff->days; $j++) { // Introducimos los datos a cada Pais por su Dia

                $getEntradas = StadisticController::calcPersonsByDayAndByCommunity($firstDayTwo, $paises[$i][0]['Id'], 1);
                $getSalidas = StadisticController::calcPersonsByDayAndByCommunity($firstDayTwo, $paises[$i][0]['Id'], 2);
                $getPernoctaciones = StadisticController::calcPersonsByDayAndByCommunity($firstDayTwo, $paises[$i][0]['Id'], 3);

                $day = array('Dia' => $firstDayTwo, 'Entradas' => $getEntradas, 'Salidas' => $getSalidas, 'Pernoctaciones' => $getPernoctaciones);

                array_push($mes, $day);

                $paises[$i][0]['Fechas'] = $mes;

                $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango

            }

            $firstDayTwo = $firstDay;
        }

        

        return view('exports.stadistics.countries', [
            'paises' => $paises,
            'dates' => $dates,
            'contador' => $contador
        ]);
    }



    public function getCanaryIslandStadistics($firstDay, $lastDay){
        
        $fecha1 = new DateTime($firstDay); //Formateamos la Primera Fecha
        $fecha2 = new DateTime($lastDay); // Formateamos la Ultima fecha

        $diff = $fecha1->diff($fecha2); // Contamos la cantidad de dias totales entre ambas fechas

        $firstDayTwo = $firstDay; // resguardamos la variable, para asi no modificar $firstDay

        $idPaises = array(); // registrara los ID de los Paises Comprometidos
        $dates = array(); // Registrara los valores de las fechas usadas

    

        for ($i = 0; $i <= $diff->days; $i++) { // Con este Bucle, obtendremos todos los Id de Todos los Paises comprometidos en las reservas de fechas seleccionadas

            $reservasTotales = Reserva::where('Estado', 1)
                ->where('FechaEntrada', '<=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('FechaSalida', '>=', date("Y-m-d", strtotime($firstDayTwo)))
                ->where('IdCanarias', '!=', null)
                ->get();

            foreach ($reservasTotales as $reserva) {

                if (array_search($reserva->IdCanarias, $idPaises) === false) {
                    array_push($idPaises, $reserva->IdCanarias);
                }
            }

            array_push($dates, $firstDayTwo); // Resguardamos todos los dias Registrados

            $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango
        }

        $firstDayTwo = $firstDay; // Recuperamos el Primer dia Seleccionado

        $paises = array(); // Creamos array donde resguardaremos todos los paises

        foreach ($idPaises as $pais) { // Identificamos las propiedades de todos los Paises comprometidos
            $getPais = IslasCanaria::where('Id', $pais)->get();
            array_push($paises, $getPais);
        }

       

        $contador = count($paises); // Cantidasd de Paises, asi ahorraremos el metodo count()


        for ($i = 0; $i < $contador; $i++) { // Recorremos los Paises para asi introducir sus datos

            $mes = array();

            for ($j = 0; $j <= $diff->days; $j++) { // Introducimos los datos a cada Pais por su Dia

                $getEntradas = StadisticController::calcPersonsByDayAndByCanaryIsland($firstDayTwo, $paises[$i][0]['Id'], 1);
                $getSalidas = StadisticController::calcPersonsByDayAndByCanaryIsland($firstDayTwo, $paises[$i][0]['Id'], 2);
                $getPernoctaciones = StadisticController::calcPersonsByDayAndByCanaryIsland($firstDayTwo, $paises[$i][0]['Id'], 3);

                $day = array('Dia' => $firstDayTwo, 'Entradas' => $getEntradas, 'Salidas' => $getSalidas, 'Pernoctaciones' => $getPernoctaciones);

                array_push($mes, $day);

                $paises[$i][0]['Fechas'] = $mes;

                $firstDayTwo = date("Y-m-d", strtotime($firstDayTwo . "+ 1 days")); // Aumentamos +1 en Fecha y recorremos el rango

            }

            $firstDayTwo = $firstDay;
        }

        

        return view('exports.stadistics.countries', [
            'paises' => $paises,
            'dates' => $dates,
            'contador' => $contador
        ]);
    }


    // public function exportINE(Request $request)
    // {
    //     return Excel::download(new StadisticsExport, 'ine.xlsx');
    // }


    public function calcPersonsByDayAndByCountry($day, $idCountry, $mode)
    {

        switch ($mode) {
            case 1: // Caso 1 = Entradas Today
                $getReservasAdults = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 2: // Caso 2 = Salidas Today
                $getReservasAdults = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 3: // Caso 3 = Pernoctaciones Today
                $getReservasAdults = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdPais', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;
        }
    }


    public function calcPersonsByDayAndByCommunity($day, $idCountry, $mode)
    {

        switch ($mode) {
            case 1: // Caso 1 = Entradas Today
                $getReservasAdults = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 2: // Caso 2 = Salidas Today
                $getReservasAdults = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 3: // Caso 3 = Pernoctaciones Today
                $getReservasAdults = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdComunidadAutonoma', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;
        }
    }


    public function calcPersonsByDayAndByCanaryIsland($day, $idCountry, $mode)
    {

        switch ($mode) {
            case 1: // Caso 1 = Entradas Today
                $getReservasAdults = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 2: // Caso 2 = Salidas Today
                $getReservasAdults = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaSalida', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;

            case 3: // Caso 3 = Pernoctaciones Today
                $getReservasAdults = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroAdultos');

                $getReservasNinios = Reserva::where('FechaEntrada', '<=', $day)
                    ->where('FechaSalida', '>', $day)
                    ->where('Estado', 1)
                    ->where('IdCanarias', $idCountry)
                    ->sum('NumeroNinios');

                return $getReservasAdults + $getReservasNinios;
                break;
        }
    }

}
