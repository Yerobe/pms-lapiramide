<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Models\Habitacion;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getRoom(Request $request, $idHabitacion = null)
    {
        if (Auth::check()) {


            if ($request->method() == "POST") {


                // Crearemos las consultas, dependiendo de la informacion recogida

                if ($request->input('tipoApartamento') == "All") {
                    $condicionApartamento = "!=";
                    $valorApartamento = null;
                } else {
                    $condicionApartamento = "=";
                    $valorApartamento = $request->input('tipoApartamento');
                }


                if ($request->input('tipoBanos') == "All") {
                    $condicionBanos = "!=";
                    $valorBanos = null;
                } else {
                    $condicionBanos = "=";
                    $valorBanos = $request->input('tipoBanos');
                }

                if ($request->input('tipoCamas') == "All") {
                    $condicionCamas = "!=";
                    $valorCamas = null;
                } else {
                    $condicionCamas = "=";
                    $valorCamas = $request->input('tipoCamas');
                }


                if ($request->input('mascotas') == "All") {
                    $condicionMascotas = "!=";
                    $valorMascotas = null;
                } else {
                    $condicionMascotas = "=";
                    $valorMascotas = $request->input('mascotas');
                }

                if ($request->input('discapacitados') == "All") {
                    $condicionDiscapacitados = "!=";
                    $valorDiscapacitados = null;
                } else {
                    $condicionDiscapacitados = "=";
                    $valorDiscapacitados = $request->input('discapacitados');
                }


                $formularioDesactivado = TRUE;
                $habitaciones =  Habitacion::where('NHabitacion', '!=', null)
                    ->where('Tipo', $condicionApartamento, $valorApartamento)
                    ->where('TipoBano', $condicionBanos, $valorBanos)
                    ->where('TipoCama', $condicionCamas, $valorCamas)
                    ->where('Mascotas', $condicionMascotas, $valorMascotas)
                    ->where('Discapacitados', $condicionDiscapacitados, $valorDiscapacitados)
                    ->get();


                $valoresSeleccionados = array(
                    'valorApartamento' => $valorApartamento,
                    'valorBanos' => $valorBanos,
                    'valorCamas' => $valorCamas,
                    'valorMascotas' => $valorMascotas,
                    'valorDiscapacitados' => $valorDiscapacitados,
                );

                $ventanaModal = FALSE;
            } else {
                $formularioDesactivado = NULL;
                $valoresSeleccionados = NULL;
                $habitaciones =  Habitacion::paginate(15);
                $ventanaModal = TRUE;
            }


            if ($request->input('page') != null) {
                $paginaActual = $request->input('page');
            } else {
                $paginaActual = 1;
            }



            $apartamentosLibres =  Habitacion::where('Estado', '=', 'Disponible')
                ->count();

            $apartamentosOcupados =  Habitacion::where('Estado', '=', 'Ocupado')
                ->count();

            $apartamentosBloqueados =  Habitacion::where('Estado', '=', 'Bloqueado')
                ->count();

            $apartamentosSucios =  Habitacion::where('Estado', '=', 'No Limpio')
                ->count();

            if ($idHabitacion != null) {
                $habitacionSeleccionada = Habitacion::find($idHabitacion);
            } else {
                $habitacionSeleccionada = null;
            }



            return view('room')->with(
                array(
                    'habitaciones' => $habitaciones,
                    'apartamentosLibres' => $apartamentosLibres,
                    'apartamentosOcupados' => $apartamentosOcupados,
                    'apartamentosBloqueados' => $apartamentosBloqueados,
                    'apartamentosSucios' => $apartamentosSucios,
                    'habitacionSeleccionada' => $habitacionSeleccionada,
                    'paginaActual' =>  $paginaActual,
                    'formularioDesactivado' => $formularioDesactivado,
                    'valoresSeleccionados' => $valoresSeleccionados,
                    'ventanaModal' => $ventanaModal
                )
            );;
        }
    }


    public function putEdit(Request $request, $id)
    {

        if (Auth::check()) {

            if ($request->input('page') != null) {
                $paginaActual = $request->input('page');
            } else {
                $paginaActual = 1;
            }


            DB::table('habitaciones')
                ->where('Id', $id)
                ->update([
                    'Tipo' => $request->input('tipoApartamento'),
                    'TipoBano' => $request->input('tipoBano'),
                    'TipoCama' => $request->input('tipoCama'),
                    'Zona' => $request->input('zona'),
                    'Mascotas' => $request->input('mascotas'),
                    'Discapacitados' => $request->input('discapacitados'),
                    'Estado' => $request->input('estado'),

                ]);

            Session::flash('RoomUpdate', 'The Room has been update successfully');
            return redirect('/rooms?page=' . $paginaActual);
        }
    }
}
