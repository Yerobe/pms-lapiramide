<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Models\Operador;
use App\Models\TelefonoOperador;
use App\Models\EmailOperador;

use App\Http\Requests\OperadorFormRequest;
use App\Http\Requests\OperadorPhoneFormRequest;
use App\Http\Requests\OperadorEmailFormRequest;

class OperatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getOperators()
    {
        if (Auth::check()) {


            $operators = Operador::all();
            $operatorsEnabled = Operador::where('Estado', 1)->paginate(10);
            $operatorsDisabled = Operador::where('Estado', 0)->paginate(10);



            return view('operator')->with(
                array(
                    'operators' => $operators,
                    'operatorsEnabled' => $operatorsEnabled,
                    'operatorsDisabled' => $operatorsDisabled
                )
            );;
        }
    }



    public function getOperator($idOperator, $opcion = null, $datoOperator = null)
    {
        if (Auth::check()) {


            if($datoOperator != null && $opcion == 'phone'){
                $modalDeletePhone = TRUE;
                $modalDeleteEmail = FALSE;
            }elseif($datoOperator != null && $opcion == 'email'){
                $modalDeleteEmail = TRUE;
                $modalDeletePhone = FALSE;
            }else{
                $modalDeletePhone = FALSE;
                $modalDeleteEmail = FALSE;
            }

            $operator = Operador::where('Id', $idOperator)->get();

            $telefonosOperator = TelefonoOperador::where('IdOperador',$idOperator)->get();
            $emailsOperator = EmailOperador::where('IdOperador',$idOperator)->get();

            return view('operadores.edit')->with(
                array(
                    'operator' => $operator,
                    'telefonosOperator' => $telefonosOperator,
                    'emailsOperator' => $emailsOperator,
                    'idOperator' => $idOperator,
                    'datoOperator' => $datoOperator,
                    'modalDeletePhone' => $modalDeletePhone,
                    'modalDeleteEmail' => $modalDeleteEmail
                )
            );;
        }
    }



    public function postCreate(OperadorFormRequest $request)
    {

        if (Auth::check()) {


            $newOperator = array(
                'Nombre' => ucwords(strtolower($request->input('name'))),
                'Estado' => 1
            );


            DB::table('operadores')->insert($newOperator);

            Session::flash('InsertOperator', 'The Operator has been created successfully');
            return redirect('/operator');
        }
    }



    public function putEdit($idOperator, OperadorFormRequest $request)
    {

        if (Auth::check()) {


            if (($request->input('estado')) == null) {
                $estado = 1;
            } else {
                $estado = 0;
            }

            DB::table('operadores')
                ->where('Id', $idOperator)
                ->update([
                    'Nombre' => ucwords(strtolower($request->input('name'))),
                    'Estado' => $estado
                ]);


            Session::flash('ModifyOperator', 'The Operator has been update successfully');
            return redirect('/operator/information/'.$idOperator);
        }
    }



    public function postCreatePhone($idOperator, OperadorPhoneFormRequest $request){

        if (Auth::check()) {


            $newPhoneOperator = array(
                'IdOperador' => $idOperator,
                'Telefono' => $request->input('phone'),
                'Nombre' => ucwords(strtolower($request->input('name')))
            );


            DB::table('telefono_operadores')->insert($newPhoneOperator);

            Session::flash('InsertPhoneOperator', 'The mobile phone has been created successfully');
            return redirect('/operator/information/'.$idOperator);

        }

    }


    public function postCreateEmail($idOperator, OperadorEmailFormRequest $request){

        if (Auth::check()) {


            $newEmailOperator = array(
                'IdOperador' => $idOperator,
                'Email' => $request->input('email'),
                'Nombre' => ucwords(strtolower($request->input('name')))
            );


            DB::table('email_operadores')->insert($newEmailOperator);

            Session::flash('InsertEmailOperator', 'The email has been created successfully');
            return redirect('/operator/information/'.$idOperator);

        }

    }





    public function postDeletePhone($idOperator, $telefono){

        if (Auth::check()) {


            TelefonoOperador::where('IdOperador',$idOperator)
            ->where('Telefono',$telefono)
            ->delete();

            Session::flash('DeletePhoneOperator', 'The mobile phone has been delete successfully');
            return redirect('/operator/information/'.$idOperator);

        }

    }



    public function postDeleteEmail($idOperator, $email){

        if (Auth::check()) {


            EmailOperador::where('IdOperador',$idOperator)
            ->where('Email',$email)
            ->delete();

            Session::flash('DeleteEmailOperator', 'The email has been delete successfully');
            return redirect('/operator/information/'.$idOperator);

        }

    }


}
