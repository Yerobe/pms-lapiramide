<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Reserva;
use App\Models\Habitacion;

class TodayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getToday(Request $request)
    {

        if (Auth::check()) {


            if($request->method() == "POST"){ // Condicional, para establecer el día de búsqueda
                $fechaSeleccionada = date($request->input('diaSeleccionado'));
            }else{
                $fechaSeleccionada = date("Y-m-d");
            }

            // Dividimosos los Apartamentos según sus Condiciones

            $apartamentosLibres =  Habitacion::where('Estado', '=', 'Disponible')
                ->count();

            $apartamentosOcupados =  Habitacion::where('Estado', '=', 'Ocupado')
                ->count();

            $apartamentosOcupadosNormales =  Habitacion::where('Estado', '=', 'Ocupado')
            ->where('Tipo','=','Normal')    
            ->count();

            $apartamentosOcupadosSuperiores =  Habitacion::where('Estado', '=', 'Ocupado')
            ->where('Tipo','=','Superior')    
            ->count();

            $reservasTotales = Reserva::where('Estado',1) // Recogemos todas las Reservas que Actualmente se encuentran hospedadas
            ->where('FechaEntrada','<=',$fechaSeleccionada)
            ->where('FechaSalida','>=',$fechaSeleccionada)
            ->get();

         
            for ($i = 0; $i <= count($reservasTotales) - 1; $i++) { // Obtenemos el Numero de Habitacion asignado a la reserva

                if ($reservasTotales[$i]->IdHabitacion == NULL) {
                    $reservasTotales[$i]['NumeroHabitacion'] = "Not Asiggned";
                } else {
                    $room = Habitacion::where("Id", "=", $reservasTotales[$i]['IdHabitacion'])->select("NHabitacion")->first();
                    $reservasTotales[$i]['NumeroHabitacion'] = $room['NHabitacion'];
                }
            }



            return view('today')->with(
                array(
                    'apartamentosLibres' => $apartamentosLibres,
                    'apartamentosOcupados' => $apartamentosOcupados,
                    'apartamentosOcupadosNormales' => $apartamentosOcupadosNormales,
                    'apartamentosOcupadosSuperiores' => $apartamentosOcupadosSuperiores,
                    'reservasTotales' => $reservasTotales,
                    'fechaSeleccionada' => $fechaSeleccionada
                )
            );;
        }
    }
}
