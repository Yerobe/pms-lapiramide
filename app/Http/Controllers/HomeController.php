<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models

use App\Models\Reserva;
use App\Models\Habitacion;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


     /*
    public function index()
    {
        return redirect()->action([HomeController::class, 'getHome']);
    }*/


    public function getHome($fecha = NULL){


        if (Auth::check()) {


            $fechaHoy = date("Y-m-d");

            if($fecha==NULL){ // Identificamos cada día de manera independiente
                $fechaactual = $fechaHoy;
            }elseif($fecha == "yesterday"){
                $fechaactual=  date("Y-m-d",strtotime($fechaHoy."- 1 days")); 
            }elseif($fecha == "tomorrow"){
                $fechaactual=  date("Y-m-d",strtotime($fechaHoy."+ 1 days")); 
            }


                $totalBookings =  Reserva::where('FechaEntrada', '<=', $fechaactual) // Reservas Hospedadas actualmente
                ->where('FechaSalida','>',$fechaactual)
                ->where('Estado', 1)
                ->count();


                $freeRooms =  Habitacion::where('Estado', '=', 'Disponible') // Numero de Habitaciones Disponibles en el Hotel
                ->count();


                $totalEntradas =  Reserva::where('FechaEntrada', '=', $fechaactual) // Reservas con Día de Entrada
                ->where('Estado', 1)
                ->count();

                $totalSalidas =  Reserva::where('FechaSalida', '=', $fechaactual) // Reservas con Día de Salida
                ->where('Estado', 1)
                ->count();


                $entradas = Reserva::where('Estado', 1) // Reservas con Día de Entrada, en el día seleccionado
                ->where('FechaEntrada','=',$fechaactual)
                ->get();


                $salidas = Reserva::where('Estado', 1) // Reservas con Día de Salida, en el día seleccionado
                ->where('FechaSalida','=',$fechaactual)
                ->get();


               

                for($i = 0 ; $i <= count($entradas)-1; $i++){ // Identificamos el Numero de Habitacion mediante el ID

                    if($entradas[$i]->IdHabitacion == NULL){
                        $entradas[$i]['NumeroHabitacion'] = "Not Asiggned";
                    }else{
                        $room = Habitacion::where("Id","=",$entradas[$i]['IdHabitacion'])->select("NHabitacion")->first();
                        $entradas[$i]['NumeroHabitacion'] = $room['NHabitacion'];
                    }

                }

                for($i = 0 ; $i <= count($salidas)-1; $i++){ // Identificamos el Numero de Habitacion mediante el ID

                    if($salidas[$i]->IdHabitacion == NULL){
                        $salidas[$i]['NumeroHabitacion'] = "Not Asiggned";
                    }else{
                        $room = Habitacion::where("Id","=",$salidas[$i]['IdHabitacion'])->select("NHabitacion")->first();
                        $salidas[$i]['NumeroHabitacion'] = $room['NHabitacion'];
                    }

                }


            return view('home')->with(
                array(
                    'totalBookings' => $totalBookings,
                    'freeRooms' => $freeRooms,
                    'totalEntradas' => $totalEntradas,
                    'totalSalidas' => $totalSalidas,
                    'entradas' => $entradas,
                    'salidas' => $salidas,
                    'fecha' => $fecha,
                    'fechaactual' => $fechaactual
                )
            );;

        }



        

    }



    
}
