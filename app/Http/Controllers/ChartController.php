<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Reserva;

class ChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getOccupation(Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $firstday = date("Y-m-d", strtotime('monday this week')); // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = date("Y-m-d", strtotime('sunday this week'));  // Obtenemos la Fecha del Domingo de esta semana
        } else {
            $firstday = $request['fechaEntrada']; // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = $request['fechaSalida'];  // Obtenemos la Fecha del Domingo de esta semana
        }



        $firstdayRecovery = $firstday;


        $calculosGrafica = array();

        for ($i = 0; $firstday <= $lastDay; $i++) {

            $apartamentsForDay =  Reserva::where('FechaEntrada', '<=', $firstday)
                ->where('FechaSalida', '>', $firstday)
                ->where('Estado', 1)
                ->count();

            $newArray = array("Day" => $firstday, "NumberApartaments" => $apartamentsForDay);

            array_push($calculosGrafica, $newArray);

            $firstday =  date("Y-m-d", strtotime($firstday . "+ 1 days"));
        }

        $totalReservation =  Reserva::where('FechaEntrada', '>=', $firstdayRecovery)
            ->orWhere('FechaEntrada', $lastDay)
            ->where('FechaSalida', '<=', $lastDay)
            ->where('Estado', 1)
            ->count();

        $totalAdults = Reserva::where('FechaEntrada', '>=', $firstdayRecovery)
            ->orWhere('FechaEntrada', $lastDay)
            ->where('FechaSalida', '<=', $lastDay)
            ->where('Estado', 1)
            ->sum('NumeroAdultos');

        $totalChildrens = Reserva::where('FechaEntrada', '>=', $firstdayRecovery)
            ->orWhere('FechaEntrada', $lastDay)
            ->where('FechaSalida', '<=', $lastDay)
            ->where('Estado', 1)
            ->sum('NumeroNinios');

        $contador = date_diff(date_create($firstdayRecovery), date_create($lastDay));
        $days = $contador->format('%a') + 1;

        $totalmedium = floor($totalReservation / $days);

        $daysSeleccionate = array(
            'FechaEntrada' => $firstdayRecovery,
            'FechaSalida' => $lastDay
        );



        return view('charts.chart_occupation')->with(
            array(
                'totalReservation' => $totalReservation,
                'totalAdults' => $totalAdults,
                'totalChildrens' => $totalChildrens,
                'totalmedium' => $totalmedium,
                'daysSeleccionate' => $daysSeleccionate,
                'calculosGrafica' => $calculosGrafica
            )
        );;
    }



    public function getCheckInOut(Request $request)
    {


        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $firstday = date("Y-m-d", strtotime('monday this week')); // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = date("Y-m-d", strtotime('sunday this week'));  // Obtenemos la Fecha del Domingo de esta semana
        } else {
            $firstday = $request['fechaEntrada']; // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = $request['fechaSalida'];  // Obtenemos la Fecha del Domingo de esta semana
        }

        $firstdayRecovery = $firstday;

        $arrayCheckIn = array();
        $arrayCheckOut = array();

        $totalCheckIn = 0;
        $totalCheckOut = 0;
        $totalDays = 0;

        for ($i = 0; $firstday <= $lastDay; $i++) {

            $checkIn =  Reserva::where('FechaEntrada', $firstday)
                ->where('Estado', 1)
                ->count();

            $checkOut =  Reserva::where('FechaSalida', $firstday)
                ->where('Estado', 1)
                ->count();

            $totalCheckIn = $totalCheckIn + $checkIn;
            $totalCheckOut = $totalCheckOut + $checkOut;

            $newArrayCheckIn = array("Day" => $firstday, "Number" => $checkIn);
            $newArrayCheckOut = array("Day" => $firstday, "Number" => $checkOut);

            array_push($arrayCheckIn, $newArrayCheckIn);
            array_push($arrayCheckOut, $newArrayCheckOut);

            $firstday =  date("Y-m-d", strtotime($firstday . "+ 1 days"));

            $totalDays++;
        }



        $daysSeleccionate = array(
            'FechaEntrada' => $firstdayRecovery,
            'FechaSalida' => $lastDay
        );

        $averageCheckIn = floor($totalCheckIn / $totalDays);
        $averageCheckOut = floor($totalCheckOut / $totalDays);




        return view('charts.chart_checkinout')->with(
            array(
                'daysSeleccionate' => $daysSeleccionate,
                'arrayCheckIn' => $arrayCheckIn,
                'arrayCheckOut' => $arrayCheckOut,
                'totalCheckIn' => $totalCheckIn,
                'totalCheckOut' => $totalCheckOut,
                'averageCheckIn' => $averageCheckIn,
                'averageCheckOut' => $averageCheckOut

            )
        );;
    }


    public function getBookings(Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $firstday = date("Y-m-d", strtotime('monday this week')); // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = date("Y-m-d", strtotime('sunday this week'));  // Obtenemos la Fecha del Domingo de esta semana
        } else {
            $firstday = $request['fechaEntrada']; // Obtenemos la Fecha del Lunes de esta semana
            $lastDay = $request['fechaSalida'];  // Obtenemos la Fecha del Domingo de esta semana
        }

        $firstdayRecovery = $firstday;


        $calculosGrafica = array();

        $totalBookings = 0;
        $totalDays = 0;

        for ($i = 0; $firstday <= $lastDay; $i++) {

            $bookingforDay =  Reserva::where('FechaCreacion', $firstday)
                ->where('Estado', 1)
                ->count();

            $totalBookings = $totalBookings + $bookingforDay;

            $newArray = array("Day" => $firstday, "NumberBookings" => $bookingforDay);

            array_push($calculosGrafica, $newArray);

            $firstday =  date("Y-m-d", strtotime($firstday . "+ 1 days"));

            $totalDays++;
        }


        $daysSeleccionate = array(
            'FechaEntrada' => $firstdayRecovery,
            'FechaSalida' => $lastDay
        );


        
        $averageBookings = floor($totalBookings / $totalDays);


        return view('charts.chart_bookings')->with(
            array(
                'daysSeleccionate' => $daysSeleccionate,
                'calculosGrafica' => $calculosGrafica,
                'totalBookings' => $totalBookings,
                'averageBookings' => $averageBookings
            )
        );;
    }
}
