<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Reserva;
use App\Models\Operador;
use App\Models\Habitacion;

class SeekerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSeeker(Request $request)
    {
        if (Auth::check()) {

            $conficionesActivadas = FALSE;



            if ($request->method() == "POST") {

                $conficionesActivadas = TRUE;


                // Crearemos las consultas, dependiendo de la informacion recogida

                if ($request->input('numeroReserva') != NULL) {
                    $valorReserva = $request->input('numeroReserva');
                    $condicionReserva = "AND NumeroReserva = '".$request->input('numeroReserva') . "' ";
                } else {
                    $condicionReserva = "";
                    $valorReserva = NULL;
                }



                if ($request->input('bookDay') != NULL) {
                    $valorBookDay = $request->input('bookDay');
                    $condicionBookDay = "AND BookDay = " . $request->input('bookDay')  . " ";
                } else {
                    $condicionBookDay = "";
                    $valorBookDay = NULL;
                }


                if ($request->input('fechaEntrada') != NULL) {
                    $valorFechaEntrada = $request->input('fechaEntrada');
                    $condicionFechaEntrada = "AND FechaEntrada = '" . $request->input('fechaEntrada')  . "' ";
                } else {
                    $condicionFechaEntrada = "";
                    $valorFechaEntrada = NULL;
                }

                if ($request->input('fechaSalida') != NULL) {
                    $valorFechaSalida = $request->input('fechaSalida');
                    $condicionFechaSalida = "AND FechaSalida = '" . $request->input('fechaSalida')  . "' ";
                } else {
                    $condicionFechaSalida = "";
                    $valorFechaSalida = NULL;
                }

                if ($request->input('operador') != "Everyone") {
                    $valorOperador = $request->input('operador');
                    $condicionOperador = "AND IdOperador = " . $request->input('operador')  . " ";
                } else {
                    $condicionOperador = "";
                    $valorOperador = NULL;
                }


                if ($request->input('habitacion') != "Everyone") {
                    $valorHabitacion = $request->input('habitacion');
                    $condicionHabitacion = "AND IdHabitacion = " . $request->input('habitacion')  . " ";
                } else {
                    $condicionHabitacion = "";
                    $valorHabitacion = NULL;
                }



                if ($request->input('pagado') != "Everyone") {
                    $valorPagado = $request->input('pagado');
                    if ($valorPagado == 2) {
                        $condicionPagado = "AND Precio_Total = Prepago ";
                    } else {
                        $condicionPagado = "AND Precio_Total != Prepago ";
                    }
                } else {
                    $condicionPagado = "";
                    $valorPagado = NULL;
                }
                
         


                if ($request->input('estado') != "Everyone") {
                    $valorEstado = $request->input('estado');
                    if($request->input('estado') == 2){
                        $condicionEstado = "AND Estado = 1 ";
                    }else{
                        $condicionEstado = "AND Estado = 0 ";
                    }
                   
                } else {
                    $condicionEstado = "";
                    $valorEstado = NULL;
                }




                if ($request->input('fechaSalida') != NULL && $request->input('fechaEntrada') != NULL) { // Si existe FechaEntrada y FechaSalida, se aplica un rango de Fechas
                    $condicionFechaSalida = "";
                    $condicionFechaEntrada = "";
                    $condicionRangoFechas = "AND FechaEntrada >= '" . $request->input('fechaEntrada') . "' AND FechaSalida <= '" . $request->input('fechaSalida') . "' ";
                }else{
                    $condicionRangoFechas = "";
                }
      




                $reservas = DB::select('SELECT * FROM reservas WHERE IdHotel = 1 ' . $condicionReserva . $condicionBookDay .
                    $condicionFechaEntrada . $condicionFechaSalida . $condicionOperador . $condicionHabitacion .$condicionPagado .$condicionEstado . $condicionRangoFechas);

                   
                for ($i = 0; $i <= count($reservas) - 1; $i++) { // Obtenemos el Numero de Habitacion asignado a la reserva

                    if ($reservas[$i]->IdHabitacion == NULL) {
                        $reservas[$i]->NumeroHabitacion = "Not Asiggned";
                    } else {
                        $room = Habitacion::where("Id", "=", $reservas[$i]->IdHabitacion)->select("NHabitacion")->first();
                        $reservas[$i]->NumeroHabitacion = $room['NHabitacion'];
                    }
                }

                $valoresSeleccionados = array(
                    'valorReserva' => $valorReserva,
                    'valorBookDay' => $valorBookDay,
                    'valorFechaEntrada' => $valorFechaEntrada,
                    'valorFechaSalida' => $valorFechaSalida,
                    'valorOperador' => $valorOperador,
                    'valorHabitacion' => $valorHabitacion,
                    'valorPagado' => $valorPagado,
                    'valorEstado' => $valorEstado
                );

                $contadorReservasActivas = 0;
                $contadorReservasCanceladas = 0;
                $contadorReservasPagadas = 0;

                foreach($reservas as $reserva){ // Contador para la Informacion Superior
                    if($reserva->Estado == 1){
                        $contadorReservasActivas = $contadorReservasActivas + 1;
                    }else{
                        $contadorReservasCanceladas = $contadorReservasCanceladas + 1;
                    }

                    if($reserva->Precio_Total == $reserva->Prepago){
                        $contadorReservasPagadas = $contadorReservasPagadas + 1;
                    }
                }



            } else {
                $reservas = array();
                $valoresSeleccionados = NULL;
            }



            $operadores = Operador::all();
            $habitaciones = Habitacion::all();




            $buscadorDesactivado = TRUE;
            $formularioDesactivado = TRUE;

            return view('seeker')->with(
                array(
                    'buscadorActivado' => $buscadorDesactivado,
                    'formularioDesactivado' => $formularioDesactivado,
                    'operadores' => $operadores,
                    'habitaciones' => $habitaciones,
                    'reservas' => $reservas,
                    'valoresSeleccionados' => $valoresSeleccionados,
                    'conficionesActivadas' => $conficionesActivadas,
                    'contadorReservasActivas' => $contadorReservasActivas ?? 0,
                    'contadorReservasCanceladas' => $contadorReservasCanceladas ?? 0,
                    'contadorReservasPagadas' => $contadorReservasPagadas ?? 0

                )
            );;
        }
    }
}
