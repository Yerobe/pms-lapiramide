<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Reserva;
use App\Models\Habitacion;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getBooking()
    {
        if (Auth::check()) {

            $fechaHoy = date("Y-m-d"); // Obtenemos la fecha Actual


            $bookings =  Reserva::where('FechaCreacion', '=', $fechaHoy) // Reservas con Fecha Registro = CURRENT_DATE()
                ->where('Estado', 1)
                ->get();


            $bookingsYesterday =  Reserva::where('FechaCreacion', '=', date("Y-m-d", strtotime($fechaHoy . "- 1 days")))
                ->where('Estado', 1)
                ->get();

            $bookingTwoDays = Reserva::where('FechaCreacion', '>=', date("Y-m-d", strtotime($fechaHoy . "- 2 days")))
                ->where('Estado', 1)
                ->count();

            $bookingSevenDays = Reserva::where('FechaCreacion', '>=', date("Y-m-d", strtotime($fechaHoy . "- 7 days")))
                ->where('Estado', 1)
                ->count();

            $bookingThirtyDays = Reserva::where('FechaCreacion', '>=', date("Y-m-d", strtotime($fechaHoy . "- 30 days")))
                ->where('Estado', 1)
                ->count();


            for ($i = 0; $i <= count($bookings) - 1; $i++) { // Localizamos el Numero de Habitacion mediante su ID y lo incluimos al mismo Array

                if ($bookings[$i]->IdHabitacion == NULL) {
                    $bookings[$i]['NumeroHabitacion'] = "Not Asiggned";
                } else {
                    $room = Habitacion::where("Id", "=", $bookings[$i]['IdHabitacion'])->select("NHabitacion")->first();
                    $bookings[$i]['NumeroHabitacion'] = $room['NHabitacion'];
                }
            }


            for ($i = 0; $i <= count($bookingsYesterday) - 1; $i++) { // Localizamos el Numero de Habitacion mediante su ID y lo incluimos al mismo Array

                if ($bookingsYesterday[$i]->IdHabitacion == NULL) {
                    $bookingsYesterday[$i]['NumeroHabitacion'] = "Not Asiggned";
                } else {
                    $room = Habitacion::where("Id", "=", $bookingsYesterday[$i]['IdHabitacion'])->select("NHabitacion")->first();
                    $bookingsYesterday[$i]['NumeroHabitacion'] = $room['NHabitacion'];
                }
            }



            return view('booking')->with(
                array(
                    'allBookings' => $bookings,
                    'allBookingsYesterday' => $bookingsYesterday,
                    'cantidadBookingToday' => count($bookings),
                    'cantidadBookingTwoDays' => $bookingTwoDays,
                    'cantidadBookingSevenDays' => $bookingSevenDays,
                    'cantidadBookingThirtyDays' => $bookingThirtyDays
                )
            );;
        }
    }
}
