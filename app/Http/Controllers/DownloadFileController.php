<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

class DownloadFileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($nombre)
    {

      

    	$filePath = public_path().'/download/'.$nombre.'.pdf';

    	$headers = ['Content-Type: application/pdf'];
        $fileName = pathinfo($filePath, PATHINFO_FILENAME) . '.pdf';

    	return FacadeResponse::download($filePath, $fileName, $headers);

    }
}
