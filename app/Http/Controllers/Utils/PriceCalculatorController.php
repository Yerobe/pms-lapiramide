<?php

namespace App\Http\Controllers\Utils;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DateTime;

class PriceCalculatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCalcPrice(){

        return view('calculator');

    }


    public function postCalcPrice(Request $request){

        $fechaEntrada = new DateTime($request->input('FechaEntrada')); //Formateamos la Primera Fecha
        $fechaSalida = new DateTime($request->input('FechaSalida')); // Formateamos la Ultima fecha

        $diasReserva = $fechaEntrada->diff($fechaSalida); // Contamos la cantidad de dias totales entre ambas fechas

        $priceDay = $request->input('Price');
        $suplemento = $request->input('Suplemento');
        $descuento = $request->input('Descuento');


        $reservePrice = (($priceDay * $diasReserva->days) + $suplemento);

        $reservePrice = number_format($reservePrice - (($reservePrice * $descuento) / 100),2);

        return view('calculator')->with(
            array(
                'fechaEntrada' => $request->input('FechaEntrada'),
                'fechaSalida' => $request->input('FechaSalida'),
                'diasReserva' => $diasReserva->days,
                'priceDay' => $priceDay,
                'suplemento' => $suplemento,
                'descuento' => $descuento,
                'reservePrice' => $reservePrice
            )
        );

    }
}
