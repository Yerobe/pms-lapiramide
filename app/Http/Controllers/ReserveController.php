<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Http\Requests\ReservaFormRequest;
use App\Models\Cliente;
use Dompdf\Options;
use Dompdf\DOMPDF;




use App\Models\Pais;
use App\Models\ComunidadAutonoma;
use App\Models\DocumentosReserva;
use App\Models\Empleado;
use App\Models\Habitacion;
use App\Models\IslasCanaria;
use App\Models\Miembro;
use App\Models\Operador;
use App\Models\Reserva;
use App\Models\User;
use App\Models\MiembroReserva;

class ReserveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getReserve()
    {

        if (Auth::check()) {


            $fecha_actual = date("Y-m-d");

            // Recogemos los datos para los Input del formulario

            $paises = Pais::all(['Id', 'Nombre']);
            $comunidadesautonomas = ComunidadAutonoma::all(['Id', 'Nombre']);
            $canaryIsland = IslasCanaria::all(['Id', 'Nombre']);
            $travelagents = Operador::all()->where('Estado', 1);
            $habitaciones = Habitacion::all();

            $fechaCheckOut = date("Y-m-d", strtotime($fecha_actual . "+ 1 days"));


            return view('reservas.create')->with(
                array(
                    'paises' => $paises,
                    'comunidadesautonomas' => $comunidadesautonomas,
                    'travelagents' => $travelagents,
                    'habitaciones' => $habitaciones,
                    'fecha_actual' => $fecha_actual,
                    'fechaCheckOut' => $fechaCheckOut,
                    'canaryIsland' => $canaryIsland
                )
            );;
        }
    }



    public function insertReserve(ReservaFormRequest $request)
    {

        if (Auth::check()) {

            $paymentMade = str_replace(",", ".", $request->input('paymentmade')); // Sustituiremos las 'Comas', por Puntos, ya que SQL no tolera 'Comas'
            $totalPrice = str_replace(",", ".", $request->input('totalprice'));  // Sustituiremos las 'Comas', por Puntos, ya que SQL no tolera 'Comas'

            if ($request->input('room') == 'null') {
                $idHabitacion = null;
            } else {
                $idHabitacion = $request->input('room');
            }

            if ($request->input('country') == 'null') {
                $pais = null;
            } else {
                $pais = $request->input('country');
            }

            if ($request->input('country') == 1) { // Si Pais == Spain

                if ($request->input('community') == 'null') {
                    $comunidadAutonoma = null;
                } else {
                    $comunidadAutonoma = $request->input('community');
                }


                if ($request->input('community') == 5) { // Si Comunidad == Canarias


                    if ($request->input('canaryIsland') == 'null') {
                        $islaCanaria = null;
                    } else {
                        $islaCanaria = $request->input('canaryIsland');
                    }
                } else {
                    $islaCanaria = null;
                }
            } else {
                $comunidadAutonoma = null;
                $islaCanaria = null;
            }

            if ($request->input('methodpayment') == 'null') {
                $methodPayment = null;
            } else {
                $methodPayment = $request->input('methodpayment');
            }





            $newReserva = array(
                'NumeroReserva' => $request->input('numero_reserva'),
                'FechaCreacion' => date("Y-m-d"),
                'BookDay' => $request->input('book_day'),
                'FechaEntrada' => $request->input('fecha_entrada'),
                'FechaSalida' => $request->input('fecha_salida'),
                'NumeroAdultos' => $request->input('adults'),
                'NumeroNinios' => $request->input('children'),
                'NumeroMascotas' => $request->input('pets'),
                'NombreCliente' => ucwords(strtolower($request->input('name'))),
                'ApellidosCliente' => ucwords(strtolower($request->input('surname'))),
                'Email' => $request->input('email'),
                'Telefono' => $request->input('phone'),
                'Prepago' => $paymentMade,
                'Precio_Total' => $totalPrice,
                'Comentario' => $request->input('comentario'),
                'Estado' => 1,
                'Puntos' => 0,
                'Regimen' => $request->input('regimen'),
                'EstadoPago' => $methodPayment,
                'IdCanarias' => $islaCanaria,
                'IdPais' => $pais,
                'IdComunidadAutonoma' => $comunidadAutonoma,
                'IdOperador' => $request->input('travelagent'),
                'IdHabitacion' => $idHabitacion,
                'IdEmpleadoCreado' => auth()->user()->id,
                'IdHotel' => 1
            );

            DB::table('reservas')->insert($newReserva);


            Session::flash('InsertReserve', 'The Reserve has been created successfully');
            return redirect('/');
        }
    }


    public function getEdit($id, $idMiembro = null)
    {
        if (Auth::check()) {


            if ($idMiembro != null) {
                $modalDeleteMember = true;
                $miembroDelete = Miembro::all()->where('Id', $idMiembro);
            } else {
                $modalDeleteMember = false;
                $miembroDelete = array();
            }

            $miembros = MiembroReserva::all()->where('IdReserva', $id);

            $allMembers = array();


            if (count($miembros) != 0) {


                $i = 0;

                foreach ($miembros as $miembro) {

                    $i++;
                    $miembroIdentified = Miembro::first()->where('id', $miembro->IdMiembro)->get();

                    $allMembers[$i]['Id'] = $miembro->IdMiembro;
                    $allMembers[$i]['NIF'] = $miembroIdentified[0]->NIF;
                    $allMembers[$i]['Nombre'] = $miembroIdentified[0]->Nombre;
                    $allMembers[$i]['Apellidos'] = $miembroIdentified[0]->Apellidos;
                }
            }

            $paises = Pais::all(['Id', 'Nombre']);
            $clientes = Cliente::all(['Id', 'Nombre']);
            $comunidadesautonomas = ComunidadAutonoma::all(['Id', 'Nombre']);
            $canaryIsland = IslasCanaria::all(['Id', 'Nombre']);
            $travelagents = Operador::all()->where('Estado', 1);
            $habitaciones = Habitacion::all();
            $reserva = Reserva::findOrFail($id);
            $documents = DocumentosReserva::all()->where('IdReserva', $id); // Obtenemos todos los documentos asociados a la reserva

            $empleadoCreado = User::select('name')->find($reserva->IdEmpleadoCreado); // Obtenemos el Nombre del Usuario que ha creado la Reserva

            $empleadosModificados = DB::table('empleados_has_reservas')->where('IdReserva', $id)->get(); //Obtenemos los Id de los Empleados que han modificado dicha Reserva



            for ($i = 0; $i <= count($empleadosModificados) - 1; $i++) {
                $nombreEmpleado = User::select('name')->find($empleadosModificados[$i]->IdEmpleado); // Obtenemos el nombre del Empleado
                $nombre = $nombreEmpleado->name;
                $empleadosModificados[$i]->NombreEmpleado = $nombre; // Introducimos el Nombre del Empleado en el Array
            }





            return view('reservas.edit')->with(
                array(
                    'reserva' => $reserva,
                    'paises' => $paises,
                    'comunidadesautonomas' => $comunidadesautonomas,
                    'travelagents' => $travelagents,
                    'habitaciones' => $habitaciones,
                    'empleadoCreado' => $empleadoCreado->name,
                    'empleadosModificados' => $empleadosModificados,
                    'miembros' => $allMembers,
                    'idReserve' => $id,
                    'modalDeleteMember' => $modalDeleteMember,
                    'miembroDelete' => $miembroDelete,
                    'canaryIsland' => $canaryIsland,
                    'documents' => $documents,
                    'clientes' => $clientes
                )
            );;
        }
    }


    public function putEdit(ReservaFormRequest $request, $id)
    {

        if (Auth::check()) {



            $paymentMade = str_replace(",", ".", $request->input('paymentmade')); // Sustituiremos las 'Comas', por Puntos, ya que SQL no tolera 'Comas'
            $totalPrice = str_replace(",", ".", $request->input('totalprice')); // Sustituiremos las 'Comas', por Puntos, ya que SQL no tolera 'Comas'

            if ($request->input('room') == 'null') {
                $idHabitacion = null;
            } else {
                $idHabitacion = $request->input('room');
            }

            if (($request->input('estado')) == null) {
                $estado = 1;
            } else {
                $estado = 0;
            }


            if ($request->input('country') == 'null') {
                $pais = null;
            } else {
                $pais = $request->input('country');
            }

            if ($request->input('country') == 1) { // Si Pais == Spain

                if ($request->input('community') == 'null') {
                    $comunidadAutonoma = null;
                } else {
                    $comunidadAutonoma = $request->input('community');
                }


                if ($request->input('community') == 5) { // Si Comunidad == Canarias


                    if ($request->input('canaryIsland') == 'null') {
                        $islaCanaria = null;
                    } else {
                        $islaCanaria = $request->input('canaryIsland');
                    }
                } else {
                    $islaCanaria = null;
                }
            } else {
                $comunidadAutonoma = null;
                $islaCanaria = null;
            }


            if ($request->input('methodpayment') == 'null') {
                $methodPayment = null;
            } else {
                $methodPayment = $request->input('methodpayment');
            }




            DB::table('reservas')
                ->where('Id', $id)
                ->update([
                    'NumeroReserva' => $request->input('numero_reserva'),
                    'BookDay' => $request->input('book_day'),
                    'FechaEntrada' => $request->input('fecha_entrada'),
                    'FechaSalida' => $request->input('fecha_salida'),
                    'NumeroAdultos' => $request->input('adults'),
                    'NumeroNinios' => $request->input('children'),
                    'NumeroMascotas' => $request->input('pets'),
                    'NombreCliente' => ucwords(strtolower($request->input('name'))), // Formateamos el nombre. YEROBE MARRERO = Yerobe Marrero
                    'ApellidosCliente' => ucwords(strtolower($request->input('surname'))),
                    'Email' => $request->input('email'),
                    'Telefono' => $request->input('phone'),
                    'Prepago' => $paymentMade,
                    'Precio_Total' => $totalPrice,
                    'Comentario' => $request->input('comentario'),
                    'Estado' => $estado,
                    'Regimen' => $request->input('regimen'),
                    'EstadoPago' => $methodPayment,
                    'IdCanarias' => $islaCanaria,
                    'IdPais' => $pais,
                    'IdComunidadAutonoma' => $comunidadAutonoma,
                    'IdOperador' => $request->input('travelagent'),
                    'IdHabitacion' => $idHabitacion,

                ]);


            $empleadosModificados = DB::table('empleados_has_reservas')->where('IdReserva', $id)->where('IdEmpleado', auth()->user()->id)->where('FechaModificacion', date("Y-m-d"))->get(); //Obtenemos los Id de los Empleados que han modificado dicha Reserva


            if (count($empleadosModificados) == 0) { // Evitaremos Redundancia de Datos
                DB::table('empleados_has_reservas')->insert(
                    ['IdEmpleado' => auth()->user()->id, 'IdReserva' => $id, 'FechaModificacion' => date("Y-m-d")]
                );
            }


            Session::flash('InsertReserve', 'The Reserve has been update successfully');
            return redirect('/');
        }
    }

    public function getVoucher($id)
    {
        if (Auth::check()) {

            $reserva = Reserva::where('Id', $id)->get();

            $path = base_path('public/images/voucher/logo.png');
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $logo = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $path = base_path('public/images/voucher/informationHotel.png');
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $information = 'data:image/' . $type . ';base64,' . base64_encode($data);


            $path = base_path('public/images/voucher/mapHotel.png');
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $map = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $pdf = \PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadview('reservas.voucher', compact('logo', 'information', 'map', 'reserva'));

            return $pdf->stream();
        }
    }


    public function savePDF(Request $request, $id)
    {

        if ($request->hasFile('urlpdf') === TRUE) {

            $reserve = DB::table('reservas')
                ->where('id', $id)
                ->first();


            $file = $request->file('urlpdf');
            $name = $reserve->NumeroReserva . '-' . time() . "." . $file->guessExtension();


            $ruta = public_path('documents/PDF/' . $name);


            if ($file->guessExtension() == 'pdf') {
                copy($file, $ruta);

                $newDocument = array( // Registramos la informacion en un mismo array
                    'Nombre' => $name,
                    'IdReserva' => $id
                );

                DB::table('documentosreservas')->insert($newDocument); // Insertamos la informacion del documento asociada a la reserva


                Session::flash('InserDNI', 'The Document has been created successfully');
                return redirect('/edit/reservation/' . $id);
            } else {
                Session::flash('ErrorInsertDNI', 'The document that has been uploaded does not have a PDF extension');
                return redirect('/edit/reservation/' . $id);
            }
        } else { // Se ha generado un error ajeno
            Session::flash('ErrorInsertDNI', 'An unknown error has been generated, please contact Jerobel Jose Marrero Moreno [Administrator]');
            return redirect('/edit/reservation/' . $id);
        }
    }


    public function viewPDF($nombre)
    {
        return response()->file('documents/PDF/' . $nombre);
    }

    public function createInvoice(Request $request, $id)
    {


        $reserveInvoie = DB::table('facturas') // Determinamos si la Reserva ya tiene Factura
            ->where('IdReserva', $id)
            ->count();

        if ($reserveInvoie == 0) { // Comprobamos que Facturas = 0 y Generamos Factura

            $facturas = DB::table('facturas') // Esto nos permite identificar si se trata de la 1 Factura a crear
                ->count();

            $number = $facturas + 1; // Generamos el Numero de Factura Correspondiente
            $numberInvocie = str_pad($number, 4, "000", STR_PAD_LEFT); // Establecemos la limitacion 0000
            $numeroFactura = "F-LP-" . $numberInvocie; // Concatenamos el Numero de Factura con String

            $reserve = DB::table('reservas') // Identificamos a la reserva
                ->where('id', $id)
                ->first();

            $cliente = DB::table('clientes') // Obtenemos los datos del cliente seleccionado
                ->where('Id', $request->input('client'))
                ->get();

            $IGIC = number_format(($reserve->Precio_Total * 7) / 107, 2); // Precio * 7 /100 = Precio a Declarar
            $finalPrice = $reserve->Precio_Total - $IGIC; // Precio el cual seran totales para el hotel


            $datosEmpresa = array(
                'Nombre' => 'Turisfuer S.L.',
                'CIF' => 'B-35345479',
                'Direccion' => 'Calle Berlina 7',
                'CP' => '35610 Costa de Antigua',
                'Provincia' => 'Las Palmas',
                'Telefono' => '+34 928 163 008',
                'IBAN' => 'ES04  2080 1203 1530 4001 0294'
            );


            $informacion = array(
                'NumeroFactura' => $numeroFactura,
                'cliente' => $cliente,
                'NumeroReserva' => $reserve->NumeroReserva,
                'NombreCliente' => $reserve->NombreCliente . ' ' . $reserve->ApellidosCliente,
                'FechaEntrada' => date("d-m-Y", strtotime($reserve->FechaEntrada)),
                'FechaSalida' => date("d-m-Y", strtotime($reserve->FechaSalida)),
                'NumeroAdultos' => $reserve->NumeroAdultos,
                'NumeroNinios' => $reserve->NumeroNinios,
                'PrecioTotal' => number_format($reserve->Precio_Total, 2),
                'IGIC' => '7%',
                'CalculatedIGIG' => $IGIC,
                'PrecioFinal' => number_format($finalPrice, 2),
                'Empresa' => $datosEmpresa,
                'IdEmpleado' => auth()->user()->id
            );




            $path = base_path('public/images/voucher/Logo-Texto.png'); // Datos de carga de imagen
            $type = pathinfo($path, PATHINFO_EXTENSION); // Datos de carga de Imagen
            $data = file_get_contents($path); // Datos de carga sde Imagen
            $logo = 'data:image/' . $type . ';base64,' . base64_encode($data); // Datos de Carga de Imagen


            $pdf = \PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadview('reservas.invoice', compact('logo', 'informacion')); // Establecemos la Vista pasando las variables necesarias

            // return $pdf->stream();
            

            $rutaGuardado = '/documentos/facturas/reservas/';
            $nombreArchivo = $reserve->NumeroReserva.'-'.$numeroFactura . '.pdf';
           // $pdf->render();

            $output = $pdf->output();
            file_put_contents( $rutaGuardado.$nombreArchivo, $output);



            




        } else { // YaExiste una Factura en relacion a este Reserva
            // Caso de error, mensaje de que ya existe una factura
            dd('Esto es un aviso de error, por favor, contacte con el administrador lo antesss posible');
        }
    }
}
