<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Models\Proveedor;
use App\Models\TelefonoProveedor;
use App\Models\EmailProveedor;

use App\Http\Requests\ProveedorFormRequest;
use App\Http\Requests\ProveedorPhoneFormRequest;
use App\Http\Requests\ProveedorEmailFormRequest;


class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProviders()
    {
        if (Auth::check()) {


            $providers = Proveedor::all();
            $providersEnabled = Proveedor::where('Estado', 1)->paginate(10);
            $providersDisabled = Proveedor::where('Estado', 0)->paginate(10);



            return view('provider')->with(
                array(
                    'providers' => $providers,
                    'providersEnabled' => $providersEnabled,
                    'providersDisabled' => $providersDisabled
                )
            );;
        }
    }

    public function getProvider($idProveedor, $opcion = null, $datoProveedor = null)
    {
        if (Auth::check()) {


            if($datoProveedor != null && $opcion == 'phone'){
                $modalDeletePhone = TRUE;
                $modalDeleteEmail = FALSE;
            }elseif($datoProveedor != null && $opcion == 'email'){
                $modalDeleteEmail = TRUE;
                $modalDeletePhone = FALSE;
            }else{
                $modalDeletePhone = FALSE;
                $modalDeleteEmail = FALSE;
            }

            $provider = Proveedor::where('Id', $idProveedor)->get();

            $telefonosProvider = TelefonoProveedor::where('IdProveedor',$idProveedor)->get();
            $emailsProvider = EmailProveedor::where('IdProveedor',$idProveedor)->get();

            return view('proveedores.edit')->with(
                array(
                    'provider' => $provider,
                    'telefonosProvider' => $telefonosProvider,
                    'emailsProvider' => $emailsProvider,
                    'idProveedor' => $idProveedor,
                    'datoProveedor' => $datoProveedor,
                    'modalDeletePhone' => $modalDeletePhone,
                    'modalDeleteEmail' => $modalDeleteEmail
                )
            );;
        }
    }


    public function postCreate(ProveedorFormRequest $request)
    {

        if (Auth::check()) {


            $newProvider = array(
                'CIF' => strtoupper($request->input('cif')),
                'Nombre' => ucwords(strtolower($request->input('name'))),
                'Direccion' => ucwords(strtolower($request->input('address'))),
                'Estado' => 1
            );


            DB::table('proveedores')->insert($newProvider);

            Session::flash('InsertProvider', 'The Provider has been created successfully');
            return redirect('/provider');
        }
    }


    public function putEdit($idProveedor, ProveedorFormRequest $request)
    {

        if (Auth::check()) {


            if (($request->input('estado')) == null) {
                $estado = 1;
            } else {
                $estado = 0;
            }

            DB::table('proveedores')
                ->where('Id', $idProveedor)
                ->update([
                    'CIF' => strtoupper($request->input('cif')),
                    'Nombre' => ucwords(strtolower($request->input('name'))),
                    'Direccion' => ucwords(strtolower($request->input('address'))),
                    'Estado' => $estado

                ]);


            Session::flash('ModifyProvider', 'The Provider has been update successfully');
            return redirect('/provider/information/'.$idProveedor);
        }
    }


    public function postCreatePhone($idProveedor, ProveedorPhoneFormRequest $request){

        if (Auth::check()) {


            $newPhoneProvider = array(
                'IdProveedor' => $idProveedor,
                'Telefono' => $request->input('phone'),
                'Nombre' => ucwords(strtolower($request->input('name')))
            );


            DB::table('telefono_proveedores')->insert($newPhoneProvider);

            Session::flash('InsertPhoneProvider', 'The mobile phone has been created successfully');
            return redirect('/provider/information/'.$idProveedor);

        }

    }



    public function postCreateEmail($idProveedor, ProveedorEmailFormRequest $request){

        if (Auth::check()) {


            $newEmailProvider = array(
                'IdProveedor' => $idProveedor,
                'Email' => $request->input('email'),
                'Nombre' => ucwords(strtolower($request->input('name')))
            );


            DB::table('email_proveedores')->insert($newEmailProvider);

            Session::flash('InsertEmailProvider', 'The email has been created successfully');
            return redirect('/provider/information/'.$idProveedor);

        }

    }


    public function postDeletePhone($idProveedor, $telefono){

        if (Auth::check()) {


            TelefonoProveedor::where('IdProveedor',$idProveedor)
            ->where('Telefono',$telefono)
            ->delete();

            Session::flash('DeletePhoneProvider', 'The mobile phone has been delete successfully');
            return redirect('/provider/information/'.$idProveedor);

        }

    }


    public function postDeleteEmail($idProveedor, $email){

        if (Auth::check()) {


            EmailProveedor::where('IdProveedor',$idProveedor)
            ->where('Email',$email)
            ->delete();

            Session::flash('DeleteEmailProvider', 'The email has been delete successfully');
            return redirect('/provider/information/'.$idProveedor);

        }

    }
}
