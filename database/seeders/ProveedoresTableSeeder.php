<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proveedores')->delete();


        $proveedores = array(
            array('Nombre' => 'Azulejos Insulares S.A', 'CIF' => 'A35014075', 'Direccion' => 'Heneken 13. 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Industrias de la Pesca Canaria S.A.', 'CIF' => 'A35028208', 'Direccion' => 'Carretera Pto. del Rosario - 35612 - Corralejo','Estado' => 1),
            array('Nombre' => 'Congelados Herbania S.A.', 'CIF' => 'A35107838', 'Direccion' => 'C/ Las Mimosas, 7, POL.IND.ARINAGA - 35118','Estado' => 1),
            array('Nombre' => 'Las Salinas', 'CIF' => 'A35121912', 'Direccion' => 'Pol.Ind.El Goro - 35219 - Telde','Estado' => 1),
            array('Nombre' => 'Aperitivos Snack S.A.', 'CIF' => 'A35125426', 'Direccion' => 'C/ Las Adelfas, 51 - POL.IND.ARINAGA','Estado' => 1),
            array('Nombre' => 'Electron', 'CIF' => 'A35421395', 'Direccion' => 'URB.Risco Prieto - 35600','Estado' => 1),
            array('Nombre' => 'Kalise', 'CIF' => 'A35561554', 'Direccion' => 'Luis Correa Media 11 - 35013 - Gran Canaria','Estado' => 1),
            array('Nombre' => 'Mercadona S.A', 'CIF' => 'A46103834', 'Direccion' => 'Parcelas 15.. 35610 Antigua','Estado' => 1),
            array('Nombre' => 'LIDL', 'CIF' => 'A60195278', 'Direccion' => 'C/ Tacha Blanca, Pol.Ind - El Matorral','Estado' => 1),

            array('Nombre' => 'Dielectro Canarias', 'CIF' => 'A96933510', 'Direccion' => 'C/ La Nasa 26-28 - 35600 - La Hondura','Estado' => 1),
            array('Nombre' => 'Meprolim', 'CIF' => 'B35011675', 'Direccion' => 'C/ Juan Rejón, 118 - 35600 - Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Químicas Morales S.L.', 'CIF' => 'B35056720	', 'Direccion' => 'Urb.ind.Risco Prieto - Parcela IV','Estado' => 1),
            array('Nombre' => 'Cosco S.L.', 'CIF' => 'B35098250', 'Direccion' => 'Isla Graciosa 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Gas y Agua S.L.', 'CIF' => 'B35112127', 'Direccion' => 'Leon y Castillo 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Peña Saavedra', 'CIF' => 'B35113703', 'Direccion' => 'C/ Profesora M Luz Saavedra 6 - 35600','Estado' => 1),
            array('Nombre' => 'AlCruz', 'CIF' => 'B35125582', 'Direccion' => 'POL.IND.Risco Prieto - 35600','Estado' => 1),
            array('Nombre' => 'Comercial Puerto Lajas S.L.', 'CIF' => 'B35135284', 'Direccion' => 'C/Libertad 17-19 - 35600','Estado' => 1),
            array('Nombre' => 'Dimerca Canarial S.L.', 'CIF' => 'B35221373', 'Direccion' => 'C/ Atalaya 26 - Urb.Ind.Lomo Blanco - Las Torres','Estado' => 1),
            array('Nombre' => 'Andromeda', 'CIF' => 'B35296029', 'Direccion' => 'C/ Pico de la Pila, 26 - 35628 - Pájara','Estado' => 1),

            array('Nombre' => 'Antartida Hielo', 'CIF' => 'B35301258', 'Direccion' => 'Avenida de la Constitucion, Nº30 - 1ºA9 - 35620 - Gran Tarajal','Estado' => 1),
            array('Nombre' => 'Bricolaje Marcial Gonzalez S.L.', 'CIF' => 'B35324953', 'Direccion' => 'Risco Prieto 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Jose Padilla Frances S.L.', 'CIF' => 'B35325422', 'Direccion' => 'C/ Jandia Km 11 - 35610','Estado' => 1),
            array('Nombre' => 'MPH', 'CIF' => 'B35405869', 'Direccion' => 'C/ Valle Largo 4 - 35610','Estado' => 1),
            array('Nombre' => 'Gruas Miguel S.L.', 'CIF' => 'B35445758', 'Direccion' => 'C/ Leon y Castillo 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => '	Saavedra Brito S.L.', 'CIF' => 'B35564160', 'Direccion' => 'C/ Gran Canaria 176 - 35600','Estado' => 1),
            array('Nombre' => 'Ferreteria Las Naves S.L.', 'CIF' => 'B35583699', 'Direccion' => 'C/ Henequen, 17 - Risco Prieto - 35600','Estado' => 1),
            array('Nombre' => 'Quimiventura Manipulados S.L.', 'CIF' => 'B35621440', 'Direccion' => 'C/ Los Pajeros 25, Pol. Ind. El Matorral, 35600','Estado' => 1),
            array('Nombre' => 'Aguas de Antigua', 'CIF' => 'B35642198', 'Direccion' => 'AV. Juán Ramón Soto Morales 7, C.C. Castillo Plaza, Local 3 - 35610 - Caleta de Fuste','Estado' => 1),
            array('Nombre' => 'Eurosanex', 'CIF' => 'B35653948', 'Direccion' => 'POL.IND. El Cuchillete, Nave 4 - 35620 - Gran Tarajal','Estado' => 1),

            array('Nombre' => 'Hermanos Paniagua Machín S.L.', 'CIF' => 'B35736883', 'Direccion' => 'C/ Cosco, 31-33 - 35600','Estado' => 1),
            array('Nombre' => 'Ferreteria La Hondura', 'CIF' => 'B35783554', 'Direccion' => 'C/ Nasa 18','Estado' => 1),
            array('Nombre' => 'Repuestos Morales y Cabrera', 'CIF' => 'B35785807', 'Direccion' => 'C/ Velazquez, 1 - 35600','Estado' => 1),
            array('Nombre' => 'Fran y Chemi S.L.', 'CIF' => 'B35885151', 'Direccion' => 'POL.IND. El Matorral - Naves 9-10-11 35610','Estado' => 1),
            array('Nombre' => 'Ideal Bricolaje', 'CIF' => 'B35888163', 'Direccion' => 'Risco Prieto 35600 Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Net Service', 'CIF' => 'B35893114', 'Direccion' => 'C/ El Veril 14 35600- Puerto Rosario','Estado' => 1),
            array('Nombre' => 'Surpan', 'CIF' => 'B38056925', 'Direccion' => 'C/ Taro 33 - 35610','Estado' => 1),
            array('Nombre' => 'Seranca', 'CIF' => 'B38479242', 'Direccion' => 'Nave 5 - POL-IND.Salinetas - 35200 - Telde','Estado' => 1),
            array('Nombre' => 'Comesa', 'CIF' => 'B38504510', 'Direccion' => 'C/ Mimosas nº49 - 35119 - Aguimes','Estado' => 1),
            array('Nombre' => 'Foncal', 'CIF' => 'B38874525', 'Direccion' => 'POL.IND.El Matorral - C/Noria 36','Estado' => 1),

            array('Nombre' => 'Montó', 'CIF' => 'B73284457', 'Direccion' => 'POL.IND.Valle de Guimar 1 - 38509','Estado' => 1),
            array('Nombre' => 'Mamma Fiore', 'CIF' => 'B76215250', 'Direccion' => 'C/ Pinto - 35219 - Telde','Estado' => 1),
            array('Nombre' => 'Cofalanz S.L.', 'CIF' => 'B76241256', 'Direccion' => 'C/ Bilbao, 58 - 35500 - Lanzarote','Estado' => 1),
            array('Nombre' => 'Ahembo S.L.', 'CIF' => 'B76271139', 'Direccion' => 'C/ Janichon - 35600 - Costa Antigua','Estado' => 1),
            array('Nombre' => 'Carnicerias El Argentino', 'CIF' => 'B76338870', 'Direccion' => 'C/ Cataluña 14 - 35600 - Puerto del Rosario','Estado' => 1),
            array('Nombre' => 'Dycten S.L.', 'CIF' => 'B76785484', 'Direccion' => 'C/ Camino Tornero, 96 - 38206 - La Laguna','Estado' => 1),
            array('Nombre' => 'Expedia Group', 'CIF' => 'B83356071', 'Direccion' => 'Pza Carlos Trias Betran 28020 Madrid','Estado' => 1),
            array('Nombre' => 'Cash Unide', 'CIF' => 'F28154946', 'Direccion' => 'POL.IND.El Matorral - Parcela 37 - 35630','Estado' => 1),
            array('Nombre' => 'Valsequillo', 'CIF' => 'F35209469', 'Direccion' => 'C/Trillo 45 - El Matorral','Estado' => 1),


            
        );



        DB::table('proveedores')->insert($proveedores);
    }
}
