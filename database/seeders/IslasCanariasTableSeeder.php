<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\IslasCanaria;
use Illuminate\Support\Facades\DB;

class IslasCanariasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('islascanarias')->delete();

        $islasCanarias = array(
            array('Nombre' => 'Lanzarote'),
            array('Nombre' =>'Fuerteventura'),
            array('Nombre' =>'Gran Canaria'),
            array('Nombre' =>'Tenerife'),
            array('Nombre' =>'La Gomera'),
            array('Nombre' =>'La Palma'),
            array('Nombre' =>'El Hierro'),
            array('Nombre' =>'La Graciosa'));

        DB::table('islascanarias')->insert($islasCanarias);
    }
}
