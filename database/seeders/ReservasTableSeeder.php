<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReservasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservas')->delete();


        $reservas = array(


            array('NumeroReserva' => '1805202101','FechaCreacion' => '2021/05/18', 'BookDay' => '2021/05/18', 'FechaEntrada' => '2021/05/19', 'FechaSalida' => '2021/05/20', 'NumeroAdultos' => 2, 'NumeroNinios' => 0, 'NumeroMascotas' => 0, 'NombreCliente' => 'CoryCocina', 'ApellidosCliente' => 'Fragiel Morales', 'Prepago' => 0, 'Precio_Total' => 150, 'Comentario' => NULL, 'Estado' => 1, 'Puntos' => 0, 'Regimen' => 'Breakfast', 'IdOperador' => 1, 'IdHabitacion' => NULL, 'IdEmpleadoCreado' => 1, 'IdHotel' => 1, 'IdCliente' => NULL),
            array('NumeroReserva' => '1805202102','FechaCreacion' => '2021/05/18', 'BookDay' => '2021/05/18', 'FechaEntrada' => '2021/05/19', 'FechaSalida' => '2021/05/20', 'NumeroAdultos' => 2, 'NumeroNinios' => 3, 'NumeroMascotas' => 1, 'NombreCliente' => 'Yerobe', 'ApellidosCliente' => 'Marrero Moreno', 'Prepago' => 150, 'Precio_Total' => 150, 'Comentario' => NULL, 'Estado' => 1, 'Puntos' => 0, 'Regimen' => 'Breakfast', 'IdOperador' => 1, 'IdHabitacion' => 50, 'IdEmpleadoCreado' => 1, 'IdHotel' => 1, 'IdCliente' => NULL),

            array('NumeroReserva' => '1805202103','FechaCreacion' => '2021/05/18', 'BookDay' => '2021/05/18', 'FechaEntrada' => '2021/05/18', 'FechaSalida' => '2021/05/19', 'NumeroAdultos' => 4, 'NumeroNinios' => 2, 'NumeroMascotas' => 0, 'NombreCliente' => 'Luis', 'ApellidosCliente' => 'Cabrera', 'Prepago' => 0, 'Precio_Total' => 150, 'Comentario' => NULL, 'Estado' => 1, 'Puntos' => 0, 'Regimen' => 'Breakfast', 'IdOperador' => 1, 'IdHabitacion' => NULL, 'IdEmpleadoCreado' => 1, 'IdHotel' => 1, 'IdCliente' => NULL),
            array('NumeroReserva' => '1805202104','FechaCreacion' => '2021/05/18', 'BookDay' => '2021/05/18', 'FechaEntrada' => '2021/05/18', 'FechaSalida' => '2021/05/19', 'NumeroAdultos' => 1, 'NumeroNinios' => 1, 'NumeroMascotas' => 1, 'NombreCliente' => 'Thomas', 'ApellidosCliente' => 'Tealdi', 'Prepago' => 150, 'Precio_Total' => 150, 'Comentario' => NULL, 'Estado' => 1, 'Puntos' => 0, 'Regimen' => 'Breakfast', 'IdOperador' => 1, 'IdHabitacion' => 49, 'IdEmpleadoCreado' => 1, 'IdHotel' => 1, 'IdCliente' => NULL),
           
           
        );


        DB::table('reservas')->insert($reservas);



    }
}
