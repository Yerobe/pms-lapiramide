<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventos')->delete();

        $eventos = array(


            array('Titulo' => 'Asadero La Piramide', 'Descripcion' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur, laborum. Repellendus eos eaque perferendis magni ea aspernatur voluptatibus reiciendis facilis dicta fuga quibusdam non eligendi debitis, magnam, animi accusamus iste eius iure cum dignissimos. Error delectus quisquam dolorem sunt et laborum voluptates facere maxime possimus. Iure, explicabo eum? Quas, laudantium?', 'Fecha' => '2021/05/07', 'ImagenURL' => 'Evento1.jpg'),
            array('Titulo' => 'Paella La Piramide', 'Descripcion' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur, laborum. Repellendus eos eaque perferendis magni ea aspernatur voluptatibus reiciendis facilis dicta fuga quibusdam non eligendi debitis, magnam, animi accusamus iste eius iure cum dignissimos. Error delectus quisquam dolorem sunt et laborum voluptates facere maxime possimus. Iure, explicabo eum? Quas, laudantium?', 'Fecha' => '2021/05/08', 'ImagenURL' => 'Evento2.jpg'),
            array('Titulo' => 'Zumba La Piramide', 'Descripcion' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur, laborum. Repellendus eos eaque perferendis magni ea aspernatur voluptatibus reiciendis facilis dicta fuga quibusdam non eligendi debitis, magnam, animi accusamus iste eius iure cum dignissimos. Error delectus quisquam dolorem sunt et laborum voluptates facere maxime possimus. Iure, explicabo eum? Quas, laudantium?', 'Fecha' => '2021/05/09', 'ImagenURL' => 'Evento3.jpg'),

        );

        DB::table('eventos')->insert($eventos);
    }
}
