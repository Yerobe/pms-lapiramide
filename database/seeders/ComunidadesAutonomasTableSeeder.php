<?php

namespace Database\Seeders;

use App\Models\ComunidadAutonoma;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ComunidadesAutonomasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('comunidadesautonomas')->delete();


        $comunidades = array(
			array('Nombre' => 'Andalucia'),
            array('Nombre' => 'Aragon'),
            array('Nombre' => 'Asturias'),
            array('Nombre' => 'Islas Baleares'),
            array('Nombre' => 'Canarias'),
            array('Nombre' => 'Cantabria'),
            array('Nombre' => 'Castilla y Leon'),
            array('Nombre' => 'Castilla la Mancha'),
            array('Nombre' => 'Cataluna'),
            array('Nombre' => 'Comunidad Valenciana'),
            array('Nombre' => 'Extremadura'),
            array('Nombre' => 'Galicia'),
            array('Nombre' => 'Comunidad de Madrid'),
            array('Nombre' => 'Region de Murcia'),
            array('Nombre' => 'Pais Vasco'),
            array('Nombre' => 'Navarra'),
            array('Nombre' => 'La Rioja')
        );


        DB::table('comunidadesautonomas')->insert($comunidades);
    }
}
