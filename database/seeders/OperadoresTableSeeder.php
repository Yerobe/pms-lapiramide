<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Operador;
use Illuminate\Support\Facades\DB;

class OperadoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operadores')->delete();


        $operadores = array(
            array('Nombre' => 'La Piramide', 'Estado' => 1),
			array('Nombre' => 'Booking', 'Estado' => 1),
            array('Nombre' => 'Expedia', 'Estado' => 1),
            array('Nombre' => 'Hotel Beds', 'Estado' => 1),
            array('Nombre' => 'Meeting Point', 'Estado' => 1),

        );


        DB::table('operadores')->insert($operadores);
    }
}
