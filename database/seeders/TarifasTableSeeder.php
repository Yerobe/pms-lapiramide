<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TarifasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tarifas')->delete();


        $tarifas = array(

            // ============ Tarifas 1 Persona =============




            // -- 01 de Mayo al 18 de Junio = Temporada Baja

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "34", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/05/01', 'FechaFin' => "2021/06/18", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "47", 'Estado' => 1),

            // -- 13 de Abril al 30 de Abril = Temporada Baja

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "34", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "39", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/04/13', 'FechaFin' => "2021/04/30", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Baja", 'PrecioNoche' => "47", 'Estado' => 1),





            // -- 13 de Septiembre al 31 de Octubre = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/09/13', 'FechaFin' => "2021/10/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),

            // -- 01 de Noviembre al 19 de Diciembre = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/11/01', 'FechaFin' => "2021/12/19", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),


            // -- 06 de Enero al 07 de Abril = Temporada Media

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/01/06', 'FechaFin' => "2021/04/07", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Media", 'PrecioNoche' => "53", 'Estado' => 1),






            // -- 19 de Junio al 12 de Septiembre = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/06/19', 'FechaFin' => "2021/09/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "53", 'Estado' => 1),

            // -- 20 de Diciembre al 31 de Diciembre = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/12/20', 'FechaFin' => "2021/12/31", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "53", 'Estado' => 1),

            
            // -- 01 de Enero al 05 de Enero = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/01/01', 'FechaFin' => "2021/01/05", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "53", 'Estado' => 1),


            // -- 08 de Abril al 12 de Abril = Temporada Alta

            array('Nombre' => '1 Persona - Solo Alojamiento', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "40", 'Estado' => 1),
            array('Nombre' => '1 Persona - Con Desayuno', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "45", 'Estado' => 1),
            array('Nombre' => '1 Persona - Media Pension', 'FechaInicio' => '2021/04/08', 'FechaFin' => "2021/04/12", 'NumeroAdultos' => 1, 'NumeroNinos' => 0, 'Temporada' => "Alta", 'PrecioNoche' => "53", 'Estado' => 1),


        );



        DB::table('tarifas')->insert($tarifas);
    }
}
