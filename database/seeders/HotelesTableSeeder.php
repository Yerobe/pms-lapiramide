<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Hotel;
use Illuminate\Support\Facades\DB;

class HotelesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hoteles')->delete();

        $hoteles = array(


            array(
                'Nif' => 'B35345479', 'Nombre' => 'Apartamentos la Piramide', 'NombreFiscal' => 'Turisfuer S.L.', 'Direccion' => 'C/ Berlina 7 - Costa de Antigua',
                'CodigoPostal' => '35610', 'Ciudad' => 'Antigua', 'Banco' => 'Abanca', 'IBAN' => 'ES04 2080 1203 1530 4001 0294', 'IGIC' => '7'
            )
        );

        DB::table('hoteles')->insert($hoteles);
    }
}
