<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_proveedores', function (Blueprint $table) {
            $table->integer('IdProveedor');
            $table->string('Email');
            $table->string('Nombre');
            $table->primary(['IdProveedor', 'Email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_proveedores');
    }
}
