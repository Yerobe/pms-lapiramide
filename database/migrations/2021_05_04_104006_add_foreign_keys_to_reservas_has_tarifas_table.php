<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReservasHasTarifasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas_has_tarifas', function (Blueprint $table) {
            $table->foreign('IdReserva', 'FK25')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdTarifa', 'FK26')->references('Id')->on('tarifas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas_has_tarifas', function (Blueprint $table) {
            $table->dropForeign('FK25');
            $table->dropForeign('FK26');
        });
    }
}
