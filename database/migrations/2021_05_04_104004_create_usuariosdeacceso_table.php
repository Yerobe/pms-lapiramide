<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosdeaccesoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariosdeacceso', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('Email');
            $table->string('Contrasena');
            $table->boolean('Estado');
            $table->integer('idCliente')->nullable()->index('FK19');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuariosdeacceso');
    }
}
