<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFacturasProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas_proveedores', function (Blueprint $table) {
            $table->foreign('IdProveedor', 'FK16')->references('Id')->on('proveedores')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdEmpleado', 'FK17')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturas_proveedores', function (Blueprint $table) {
            $table->dropForeign('FK16');
            $table->dropForeign('FK17');
        });
    }
}
