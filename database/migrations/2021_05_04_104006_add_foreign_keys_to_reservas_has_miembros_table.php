<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReservasHasMiembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas_has_miembros', function (Blueprint $table) {
            $table->foreign('IdReserva', 'FK28')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdMiembro', 'FK29')->references('Id')->on('miembros')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas_has_miembros', function (Blueprint $table) {
            $table->dropForeign('FK28');
            $table->dropForeign('FK29');
        });
    }
}
