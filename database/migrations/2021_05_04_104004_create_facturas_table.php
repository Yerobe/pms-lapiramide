<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('NumeroFactura')->unique('NumeroFactura');
            $table->string('Nombre');
            $table->integer('IdCliente')->nullable()->index('FK2IdCiente');
            $table->integer('IdReserva')->index('FK2IdReserva');
            $table->integer('IdEmpleado')->index('FK2IdEmpleado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
