<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosHasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos_has_productos', function (Blueprint $table) {
            $table->integer('IdPedido');
            $table->integer('IdProducto')->index('FK22');
            $table->smallInteger('Cantidad');
            $table->primary(['IdPedido', 'IdProducto']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos_has_productos');
    }
}
