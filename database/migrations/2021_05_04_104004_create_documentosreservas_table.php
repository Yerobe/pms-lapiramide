<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosreservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentosreservas', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('Nombre', 80)->unique('Nombre');
            $table->integer('IdReserva')->nullable()->index('FK-IdReserva');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentosreservas');
    }
}
