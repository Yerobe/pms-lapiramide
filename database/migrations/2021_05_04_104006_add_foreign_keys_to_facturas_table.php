<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas', function (Blueprint $table) {
            $table->foreign('IdCliente', 'FK2IdCiente')->references('Id')->on('clientes')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdReserva', 'FK2IdReserva')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdEmpleado', 'FK2IdEmpleado')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturas', function (Blueprint $table) {
            $table->dropForeign('FK2IdCiente');
            $table->dropForeign('FK2IdReserva');
            $table->dropForeign('FK2IdEmpleado');
        });
    }
}
