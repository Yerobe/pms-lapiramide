<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocumentosreservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentosreservas', function (Blueprint $table) {
            $table->foreign('IdReserva', 'FKIdReservaDocumentos')->references('Id')->on('reservas')->onUpdate('NO ACTION')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentosreservas', function (Blueprint $table) {
            $table->dropForeign('FKIdReservaDocumentos');
        });
    }
}
