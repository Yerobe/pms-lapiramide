<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoteles', function (Blueprint $table) {
            $table->integer('Id', true);
            $table->string('Nif', 15);
            $table->string('Nombre');
            $table->string('NombreFiscal');
            $table->string('Direccion');
            $table->string('CodigoPostal', 10);
            $table->string('Ciudad');
            $table->string('Banco');
            $table->string('IBAN');
            $table->float('IGIC', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoteles');
    }
}
