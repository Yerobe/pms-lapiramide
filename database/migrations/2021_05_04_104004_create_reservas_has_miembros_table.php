<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasHasMiembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas_has_miembros', function (Blueprint $table) {
            $table->integer('IdReserva')->index('FK28');
            $table->integer('IdMiembro')->index('FK29');
            $table->primary(['IdReserva', 'IdMiembro']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas_has_miembros');
    }
}
