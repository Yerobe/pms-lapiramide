<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPedidosHasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos_has_productos', function (Blueprint $table) {
            $table->foreign('IdPedido', 'FK21')->references('Id')->on('pedidos')->onUpdate('NO ACTION')->onDelete('RESTRICT');
            $table->foreign('IdProducto', 'FK22')->references('Id')->on('productos')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos_has_productos', function (Blueprint $table) {
            $table->dropForeign('FK21');
            $table->dropForeign('FK22');
        });
    }
}
